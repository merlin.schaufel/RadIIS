#include <Arduino.h>
#include <WiFi.h>
#include <ESP32WebServer.h>
#include <ESPmDNS.h>
#include <FS.h>
#include <SPIFFS.h>
#include <ArduinoJson.h>
#include <Timer.h>


// acces point
const char *ssid = "RadIIS rev_1.0";
const char *password = "thereisnospoon";
const char *hostname = "RadIIS";

ESP32WebServer server (80);

// globals
uint32_t counts = 0;
int countrate;
int n_bins = 4096;
int spectrum [4096];
float offset = 0;
float scale = 1.0;

// timer
Timer t;

void handleRoot() {
  server.send(200, "text/plain", "Webserver On.");
}

void calculate_countrate(){
    countrate = int (counts/1000);
}

uint8_t find(double f)
{
	uint8_t i = 0;
	while(i < n_bins && f > i) i++;
	return i;
}

void send_spectrum(){
    String spec = "";
    for (int i = 0; i < n_bins; i++)
    {
        spec += String(spectrum[i]) + ", ";
    }

    String json = "{\"spectrum_00\": [" + spec + "]}";
    server.send(200, "application/json", json);
}

void send_countrate(){
    String json = "{\"count_rate\":";
    json += "{\"channel_0\":" + String(countrate) + ",";
    json += "}";
    json += "}";

    server.send(200, "application/json", json);
}

void reset_measurement(){
    for (uint8_t i = 0; i < n_bins; i++)
    {
        spectrum[i] = 0;
    }
}

void handle_dynamic_content(){
    server.on("/D/Me/spec.json", send_spectrum);
    server.on("/D/Me/cr.json", send_countrate);
    server.on("/reset", reset_measurement);
}

void setup(void){
    // start serial port, sets data rate to 9600 bps
    Serial.begin(9600);
    Serial.println("Serial communication up - setup started");

    if (SPIFFS.begin(true)) {Serial.println("SPIFFS started");};

    WiFi.mode(WIFI_AP);
    WiFi.softAP(ssid, password);
    Serial.print("IP address:\t");
    Serial.println(WiFi.softAPIP());

    if (MDNS.begin(hostname)) {Serial.println("MDNS started");};
    MDNS.addService("http", "tcp", 80);

    // server.serveStatic("/", SPIFFS, "/index.html");
    server.begin();
    server.on("/", handleRoot);


    // Serial.println("HTTP server started");
    //
    handle_dynamic_content();
}

void loop(void){
    server.handleClient();
    if(Serial.available()>0)    //Checks is there any data in buffer
    {
        int input = int(Serial.parseInt());
        if (input > 0) {
            Serial.println(input);
            spectrum[find(input)]+=1;
            counts+=1;
        }
    }
    t.update();
}

// follows https://randomnerdtutorials.com/esp32-esp8266-input-data-html-form/

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>

AsyncWebServer server(80);

// REPLACE WITH YOUR NETWORK CREDENTIALS
const char* ssid = "SSID";
const char* password = "PWD";
const char *hostname = "RadIIS";

const char* SERIAL_INPUT = "serial";

// HTML web page to handle 3 input fields (input1, input2, input3)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>RadIIS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
  <form action="/send">
    send over serial: <input type="text" name="serial">
    <input type="submit" value="Send">
  </form><br>
</body></html>)rawliteral";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi Failed!");
    return;
  }
  Serial.println();
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  // Send web page with input fields to client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html);
  });

  // Send a GET request to <ESP_IP>/send?input1=<serial_message>
  server.on("/send", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String serial_message;
    String input_param;
    // GET SERIAL_INPUT value on <ESP_IP>/send?serial_input=<serial_message>
    if (request->hasParam(SERIAL_INPUT)) {
      serial_message = request->getParam(SERIAL_INPUT)->value();
      input_param = SERIAL_INPUT;
    }
    else {
      serial_message = "";
      input_param = "none";
    }
    Serial.println(serial_message);
    request->send(200, "text/html", "HTTP GET request sent to your ESP on input_param field ("
                                     + input_param + ") with value: " + serial_message +
                                     "<br><a href=\"/\">Return to Home Page</a>");
  });
  server.onNotFound(notFound);
  server.begin();
}

void loop() {

}

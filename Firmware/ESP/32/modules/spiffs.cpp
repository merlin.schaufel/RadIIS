#include "ArduinoJson.h"
#include "FS.h"

void setup() {

  bool result = SPIFFS.begin();
  Serial.println("SPIFFS opened: " + result);

  File f = SPIFFS.open("/try.txt", "r");
  if (!f) {
    Serial.println("file open failed");
  } else {
    while(f.available()) {
      String line = f.readStringUntil('n');
      Serial.println(line);
    }

  }

  f.close();

  Serial.print("Shutting down SPIFFS: ");
  SPIFFS.end();
  Serial.print("done");
}

void loop() {
  // put your main code here, to run repeatedly:

}

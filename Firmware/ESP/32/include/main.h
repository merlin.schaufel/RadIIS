// Baudrate for the STM<>ESP communication:
#define STM_ESP_SERIAL_BAUD 115200

// Pin definitions:
// Outputs:

#define GPIO_Buzzer 27
#define GPIO_LEDR 14
#define GPIO_LEDG 15
#define GPIO_LEDB 12
#define GPIO_SDCS 5
#define GPIO_SDPON 21

// Inputs:
#define GPIO_Button 13
#define GPIO_Button_NUM GPIO_NUM_13

// LED PWM channels:
#define LEDChR 0
#define LEDChG 1
#define LEDChB 2
#define LED_PWM_FREQ 5000 //5kHz PWM
#define LED_PWM_RES 8 //8bit Resolution

// PWM channel for buzzer:
#define BuzzerCh 3

// Error Types:
#define SD_INIT_ERROR 1
#define SD_TYPE_ERROR 2
#define HTML_FILE_ERROR 3

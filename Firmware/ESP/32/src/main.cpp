// follows https://randomnerdtutorials.com/esp32-esp8266-input-data-html-form/
// This piece of code opens an AP from the ESP, subsequently a web page can be opened and serial commands written in the web interface can be sent

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>

// Libraries for SD card
#include "FS.h"
#include "SD.h"
#include <SPI.h>

#include "main.h"

AsyncWebServer server(80);

// REPLACE WITH YOUR NETWORK CREDENTIALS
const char* ssid = "RadIIS rev 1.0";
const char* password = "password";
const char *hostname = "RadIIS";

const char* SERIAL_INPUT = "serial";

// HTML web page to handle 3 input fields (input1, input2, input3)
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>RadIIS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
  <form action="/send">
    send over serial: <input type="text" name="serial">
    <input type="submit" value="Send">
  </form><br>
</body></html>)rawliteral";

char* html;

RTC_DATA_ATTR int RTC_SramTest = 0;
//RadIIS_Config.json

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

void set_button_color(int Red, int Green, int Blue)
{
  ledcWrite(LEDChR,Red);
  ledcWrite(LEDChG,Green);
  ledcWrite(LEDChB,Blue);
}

void enter_deep_sleep(void)
{
  // ToDo:
  // 1) Send Shutdown STM command

  // Shutdown external peripherals (SD):
  digitalWrite(GPIO_SDPON,LOW);
  // Init ext0 wakeup (Button):
  esp_sleep_enable_ext0_wakeup(GPIO_Button_NUM,1); //1 = High, 0 = Low
  // Goto deep sleep:
  esp_deep_sleep_start();
}

void play_tone(int freq, int dur) {
  ledcWriteTone(0, freq); // play tone
  delay(dur);
  ledcWriteTone(0, 0); // Stop tone
}

void Error_Handler(int Error_Type)
{
  play_tone(4400,1000);
  while(true)
  {
    for(int i = 0; i<Error_Type;i++)
    {
      set_button_color(255,0,0);
      delay(400);
      set_button_color(0,0,0);
      delay(400);
    }
    delay(5000);
  }
}

void setup() {
    // Setup serial for STM-ESP Com:
    Serial.begin(STM_ESP_SERIAL_BAUD);

    // Setup input and output pins:
    // Button input:
    pinMode(GPIO_Button,INPUT_PULLDOWN);

    // Configure LED PWM functionalitites
    ledcSetup(LEDChR, LED_PWM_FREQ, LED_PWM_RES);
    ledcSetup(LEDChG, LED_PWM_FREQ, LED_PWM_RES);
    ledcSetup(LEDChB, LED_PWM_FREQ, LED_PWM_RES);

    // attach the channel to the GPIO to be controlled
    ledcAttachPin(GPIO_LEDR, LEDChR);
    ledcAttachPin(GPIO_LEDG, LEDChG);
    ledcAttachPin(GPIO_LEDB, LEDChB);

    // Init Buzzer:
    ledcSetup(BuzzerCh, 2000, 8);
    ledcAttachPin(GPIO_Buzzer, 0);
    ledcWriteTone(0, 0);

    // Sd card and FS init:
    pinMode(GPIO_SDPON,OUTPUT);
    digitalWrite(GPIO_SDPON,HIGH);

    //ToDo: Read in list with saved Networks to check before creating own AP

    // Init SD card
    SD.begin(GPIO_SDCS);
    if (!SD.begin(GPIO_SDCS)) {
      //Serial.println("Attach SD card and restart");
      Error_Handler(SD_INIT_ERROR);
    }

    uint8_t cardType = SD.cardType();
    switch (cardType) {
      case CARD_NONE:
      case CARD_UNKNOWN:
        Error_Handler(SD_TYPE_ERROR);
      case CARD_SD:
        set_button_color(0,255,0);
        break;
      case CARD_SDHC:
        set_button_color(0,255,255);
        break;
    }

    //read html file from SD card
    File file = SD.open("/index.html");
    if (!file) {
      Error_Handler(HTML_FILE_ERROR);
    }
    //Serial.printf("index.htm file size: %d\r\n", file.size());
    html = new char [file.size() + 1];
    file.read((uint8_t*)html, file.size());
    html[file.size()] = 0;  //make a CString
    file.close();

    //   Use existing Wifi:
    //   WiFi.mode(WIFI_STA);
    //   WiFi.begin(ssid, password);
    //   if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    //     Serial.println("WiFi Failed!");
    //     return;
    //   }
    //   Serial.println();
    //   Serial.print("IP Address: ");
    //   Serial.println(WiFi.localIP());

    //  Make Access Point:
    WiFi.softAP(ssid, password);

    // Send web page with input fields to client
    server.on("/", HTTP_GET, [] (AsyncWebServerRequest *request){
        request->send_P(200, "text/html", html);
    });

    //   const char* soft_ap_ip = WiFi.softAPIP();
    //   server.on("/ip", HTTP_GET, [] (AsyncWebServerRequest *request){
    //     request->send_P(200, "text/plain", soft_ap_ip);
    //   });

    // Send a GET request to <ESP_IP>/send?input1=<serial_message>
    server.on("/send", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String serial_message;
    String input_param;
    // GET SERIAL_INPUT value on <ESP_IP>/send?serial_input=<serial_message>
    if (request->hasParam(SERIAL_INPUT)) {
        serial_message = request->getParam(SERIAL_INPUT)->value();
        input_param = SERIAL_INPUT;
    }
    else {
        serial_message = "";
        input_param = "none";
    }
    Serial.println(serial_message);
    request->send(200, "text/html",
        "HTTP GET request sent to your ESP on input_param field ("
        + input_param + ") with value: " + serial_message +
        "<br><a href=\"/\">Return to Home Page</a>");
    });

    server.onNotFound(notFound);
    server.begin();
}


void loop()
{


}

#include <Arduino.h>
#include <histogram.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <Timer.h>

// acces point
const char *ssid = "RadIIS rev_1.0";
const char *password = "thereisnospoon";
const char *hostname = "RadIIS";

ESP8266WebServer server (80);

// file system
// you can upload the contents of a folder if you CD in that folder and run the following command:
// for file in `ls -A1`; do curl -F "file=@$PWD/$file" esp8266fs.local/edit; done
// File fsUploadFile;
// File fsStreamFile;
// File fsFileFile;
const char *PATH_SPECTRUM = "spectrum.json";
const char *PATH_COUNTS = "counts.json";

// json
StaticJsonBuffer<44000> jsonBuffer;

// timer
Timer t;

// globals
uint32_t counts = 0;
int countrate;
int n_bins = 6;
long *spectrum = (long*) malloc((4096) * sizeof(long));
float offset = 0;
float scale = 1.0;


void calculate_countrate(){
    countrate = int (counts/1000);
}

// void read_file(const char* file_path){
//     File file = SPIFFS.open(file_path, "r");
//     if (!file){
//         Serial.println("File can't be found.");
//     } else {
//         size_t size = file.size();
//         if ( size == 0 ) {
//             Serial.println("File is empty.");
//         } else {
//             std::unique_ptr<char[]> buf (new char[size]);
//             file.readBytes(buf.get(), size);
//             JsonObject& root = jsonBuffer.parseObject(buf.get());
//             if (!root.success()) {
//                 Serial.println("File can't be read.");
//             } else {
//                 Serial.println("File loaded");
//                 root.printTo(Serial);
//             }
//         }
//         file.close();
//     }
// }

void spectrum_to_json(JsonObject& root){
    // write spectrum and countrate to json or
    JsonArray& spectrum_buffer = root.createNestedArray("spectrum");
    for (uint8_t i = 0; i < 4096; i++)
    {
        int indx = int (offset + i * scale);
        spectrum_buffer.add(spectrum[indx]);
    }
}

void spectrum_to_file(void){
    JsonObject& root = jsonBuffer.createObject();
    spectrum_to_json(root);

    // write json to file in SPIFFS
    File json_file = SPIFFS.open(PATH_SPECTRUM, "w");
    root.printTo(json_file); // Exporte et enregsitre le JSON dans la zone SPIFFS - Export and save JSON object to SPIFFS area
    json_file.close();
}

uint8_t find(double f)
{
	uint8_t i = 0;
	while(i < 4096 && f > i) i++;
	return i;
}

void send_spectrum(){
    String spec = "";
    for (uint8_t i = 0; i < 4096; i++)
    {
        spec += String(spectrum[i]) + ", ";
    }

    String json = "{\"spectrum_00\": " + spec + "}";
    server.send(200, "application/json", json);
}

void send_countrate(){
    String json = "{\"count_rate\":";
    json += "{\"channel_0\":" + String(countrate) + ",";
    json += "}";
    json += "}";

    server.send(200, "application/json", json);
}

void reset_measurement(){
    for (uint8_t i = 0; i < 4096; i++)
    {
        spectrum[i] = 0;
    }
}

// void handle_static_content(){
//     server.on("/data/spectrum.json", send_spectrum);
//     server.on("/data/count_rate.json", send_countrate);
//     server.on("/reset", reset_measurement);
// }

void handle_dynamic_content(){
    server.on("/data/spectrum.json", send_spectrum);
    server.on("/data/count_rate.json", send_countrate);
    server.on("/reset", reset_measurement);
}

void setup(void){
    // InitializeESP: Tell STM: READY :)
    pinMode(D2, OUTPUT);
    digitalWrite(D2, HIGH);

    // start serial port, sets data rate to 9600 bps
    Serial.begin(9600);
    Serial.print("Board Started");
    SPIFFS.begin();

    WiFi.softAP(ssid, password);
    MDNS.begin(hostname);
    MDNS.addService("http", "tcp", 80);

    // read_file(PATH_SPECTRUM);
    // read_file(PATH_COUNTS);

    t.every(1000, calculate_countrate);

    t.every(50000, spectrum_to_file);

    for (uint8_t i = 0; i < 4096; i++)
	{
		spectrum[i] = 0;
	}

    server.serveStatic("/", SPIFFS, "/index.html");
    server.begin();
    // handle_static_content()
    handle_dynamic_content();
}

void loop(void){
    if(Serial.available()>0)    //Checks is there any data in buffer
    {
        spectrum[find(int(Serial.read()))]++;
        counts++;
    }
     t.update();
}

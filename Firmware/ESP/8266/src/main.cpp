#include <Arduino.h>
#include <histogram.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <Timer.h>

// acces point
const char *ssid = "RadIIS rev_1.0";
const char *password = "thereisnospoon";
const char *hostname = "RadIIS";

ESP8266WebServer server (80);

// globals
uint32_t counts = 0;
int countrate;
int n_bins = 40;
int spectrum [40];
float offset = 0;
float scale = 1.0;

// timer
Timer t;

void calculate_countrate(){
    countrate = int (counts/1000);
}

uint8_t find(double f)
{
	uint8_t i = 0;
	while(i < n_bins && f > i) i++;
	return i;
}

void send_spectrum(){
    String spec = "";
    for (int i = 0; i < n_bins; i++)
    {
        spec += String(spectrum[i]) + ", ";
    }

    String json = "{\"spectrum_00\": [" + spec + "]}";
    server.send(200, "application/json", json);
}

void send_countrate(){
    String json = "{\"count_rate\":";
    json += "{\"channel_0\":" + String(countrate) + ",";
    json += "}";
    json += "}";

    server.send(200, "application/json", json);
}

void reset_measurement(){
    for (uint8_t i = 0; i < n_bins; i++)
    {
        spectrum[i] = 0;
    }
}

void handle_dynamic_content(){
    server.on("/data/spectrum.json", send_spectrum);
    server.on("/reset", reset_measurement);
}

void setup(void){
    // start serial port, sets data rate to 9600 bps
    Serial.begin(9600);
    Serial.println("Serial communication up - setup started");

    if (SPIFFS.begin()) {Serial.println("SPIFFS started");};

    WiFi.softAP(ssid, password);
    if (MDNS.begin(hostname)) {Serial.println("MDNS started");};
    MDNS.addService("http", "tcp", 80);

    for (int i = 0; i < n_bins; i++)
    {
        spectrum[i] = 0;
    }

    server.serveStatic("/", SPIFFS, "/index.html");
    server.begin();
    Serial.println("HTTP server started");

    handle_dynamic_content();
}

void loop(void){
    server.handleClient();
    if(Serial.available()>0)    //Checks is there any data in buffer
    {
        int input = int(Serial.read());
        Serial.println(input);
        spectrum[find(input)]+=1;
        counts+=1;
    }
     t.update();
}

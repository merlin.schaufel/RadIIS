/*
 * radiis_sys.h
 *
 *  Created on: Apr 9, 2017
 *      Author: Merlin Schaufel
 */

#ifndef APPLICATION_USER_RADIIS_SYS_H_
#define APPLICATION_USER_RADIIS_SYS_H_

#include "main.h"

//STM firmware version:
#define FW_VERSION 0x000a
//STM firmware version:
#define SERIAL_NUMBER 0x0001
//Define the default resolution
#define RESOLUTION 512
//Changed parameters during meas.
#define PCHANGE -1

typedef struct{
	uint32_t spectrum[RESOLUTION];
	uint32_t channel_pointer;
	uint32_t Tlive;
	uint32_t Date;
	char Comment[65];
} Background;


typedef struct{
	uint32_t current_spectrum[RESOLUTION];
	uint32_t Count;
	uint32_t MaxCount;
	uint32_t Tstart;
	uint32_t Tstop;
	uint32_t Tlive;
	int32_t Bias;
	int8_t ATC;
	int32_t Thres;
	uint8_t	 Active;
	uint8_t Channel;
} Measurement;


typedef struct{
	int32_t Temperature;
	uint32_t Bias;
	uint16_t Threshold;
	uint32_t BiasNom;
	uint32_t BatLevel;
	uint32_t GateTime;
	uint32_t Count;
	uint32_t ExCount;
	uint32_t Rate;
	uint32_t RateBGC;
	uint32_t ExRate;
	uint32_t ButtonTs;
	uint32_t Background[RESOLUTION];
	uint8_t	 Status;
	uint32_t Flags;
} HWSystem;

//Hardware defines:
#define NomTemp_SiPM 			21000 	// mC
#define TempCoeff_SiPM			21.5  // mV/C
#define BiasOffset				24032 	// mV
#define MAX_BIAS				29950 //mV
#define MIN_BIAS				24304 //mV
#define BGlivetime			100000 		// s

//GPIO Port Conncections:
//defines:							 Pin:	HW_Connection:	Init:	ADC Channel:
//**************PORT A**************//
#define Pin_PH2 		GPIO_PIN_0 	// PA0	PulseHold2		InitADC	Ch. 5
#define Pin_PH1 		GPIO_PIN_1 	// PA1	PulseHold1		InitADC Ch. 6
#define Pin_TempIn		GPIO_PIN_2 	// PA2	TempIn			InitADC Ch. 7
#define Pin_BiasIn		GPIO_PIN_3 	// PA3	BiasIn			InitADC Ch. 8
#define Pin_BiasOut		GPIO_PIN_4 	// PA4	BiasOut			InitDAC
#define Pin_BatIn		GPIO_PIN_5	// PA5	BatSenseIn		InitADC Ch. 9
#define Pin_PulsC		GPIO_PIN_6	// PA6	PulserCharge	InitADC Ch. 10
 	 	 	 	 	 	 	 	 	// PA7	ExIO			-TODO-
 	 	 	 	 	 	 	 	 	// PA8	NC				--NC--
 	 	 	 	 	 	 	 	 	// PA9	USBVBUS			USBlib
 	 	 	 	 	 	 	 	 	// PA10	USBID			USBlib
 	 	 	 	 	 	 	 	 	// PA11	USBDM			USBlib
 	 	 	 	 	 	 	 	 	// PA12	USBDP			USBlib
 	 	 	 	 	 	 	 	 	// PA13	TMS_D			--No--
 	 	 	 	 	 	 	 	 	// PA14	TCK_D			--No--
 	 	 	 	 	 	 	 	 	// PA15	NC				--NC--
//**********************************//

//**************PORT B**************//
 	 	 	 	 	 	 	 	 	// PB0	NC				--NC--
 	 	 	 	 	 	 	 	 	// PB1	NC				--NC--
#define Pin_TrgIn1		GPIO_PIN_2 	// PB2 	TriggerIn1		InitComp
 	 	 	 	 	 	 	 	 	// PB3	SWO_D			--No--
#define Pin_TrgIn2		GPIO_PIN_4	// PB4	TriggerIn2		InitComp
 	 	 	 	 	 	 	 	 	// PB5	NC				--NC--
 	 	 	 	 	 	 	 	 	// PB6	NC				--NC--
 	 	 	 	 	 	 	 	 	// PB7	NC				--NC--
 	 	 	 	 	 	 	 	 	// PB8	NC				--NC--
#define Pin_SHNDAn		GPIO_PIN_9 	// PB9	SHDNAnalog		InitGPIO
#define Pin_ESPtx		GPIO_PIN_10	// PB10	ESP_TX			USBlib
#define Pin_EPSrx		GPIO_PIN_11	// PB11	ESP_RX			USBlib
 	 	 	 	 	 	 	 	 	// PB12	NC				--NC--
#define Pin_SHNDEx		GPIO_PIN_13	// PB13	SHDNExVin		InitGPIO
#define Pin_Buzzer		GPIO_PIN_14	// PB14	BuzzerOut		InitGPIO //TODO ESP32
#define Pin_ChStat		GPIO_PIN_15	// PB15	ChargerStatIn	InitGPIO
//**********************************//

//**************PORT C**************//
									// PC0	NC				--NC--
#define Pin_ResetPH1	GPIO_PIN_1 	// PC1	ResetPH1		InitGPIO
#define Pin_ResetPH2	GPIO_PIN_2	// PC2	ResetPH2		InitGPIO
 	 	 	 	 	 	 	 	 	// PC3	NC				--NC--
 	 	 	 	 	 	 	 	 	// PC4	NC				--NC--
#define Pin_ESPready 	GPIO_PIN_5 // PC5	ESP_RD-
 	 	 	 	 	 	 	 	 	// PC6	NC				--NC--
 	 	 	 	 	 	 	 	 	// PC7	NC				--NC--
#define Pin_LEDStat		GPIO_PIN_8 	// PC8	StatusLED		InitGPIO //TODO ADD?
#define Pin_Button		GPIO_PIN_9	// PC9	ButtonIn		InitGPIO //TODO ESO32
#define Pin_RNtx		GPIO_PIN_10	// PC10	RN_TX			-TODO-
#define Pin_RNrx		GPIO_PIN_11	// PC11	RN_RX			-TODO-
#define Pin_EXtx		GPIO_PIN_12	// PC12	Ex_TX			-TODO-
 	 	 	 	 	 	 	 	 	// PC13	NC				--NC--
 	 	 	 	 	 	 	 	 	// PC14	LF_OSCIN		--No--
 	 	 	 	 	 	 	 	 	// PC15	LF_OSCOUT		--No--
//**********************************//

//**************PORT D**************//
#define Pin_RNreset		GPIO_PIN_0	// PD0	RN_Reset		-TODO-
 	 	 	 	 	 	 	 	 	// PD1	NC				--NC--
#define Pin_EXrx		GPIO_PIN_2	// PD2	Ex_RX			-TODO-
 	 	 	 	 	 	 	 	 	// PD3	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD4	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD5	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD6	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD7	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD8	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD9	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD10	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD11	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD12	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD13	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD14	NC				--NC--
 	 	 	 	 	 	 	 	 	// PD15	NC				--NC--
//**********************************//

//**************PORT E**************//
 	 	 	 	 	 	 	 	 	// PE0	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE1	NC				--NC--
#define Pin_LEDTrg		GPIO_PIN_2	// PE2	PulserTrigger	InitGPIO
 	 	 	 	 	 	 	 	 	// PE3	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE4	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE5	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE6	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE7	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE8	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE9	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE10	NC				--NC--
 	 	 	 	 	 	 	 	 	// PE11	NC				--NC--
//#define Pin_ESPreset	GPIO_PIN_12	// PE12	ESP_RS			InitGPIO //TODO obsolet for ESP32
#define Pin_ESPpdown 	GPIO_PIN_13	// PE13	ESP_PD			InitGPIO
#define Pin_ESPboot		GPIO_PIN_14	// PE14	ESP_BOOT		InitGPIO
//#define Pin_ESPready 	GPIO_PIN_15 // PE15	ESP_RD			InitGPIO //TODO obsolet Now PC
//**********************************//
 	 	 	 	 	 	 	 	 	// PH0	HF_OSCIN		--No--
 	 	 	 	 	 	 	 	 	// PH1	HF_OSCOUT		--No--
//**********************************//


#define DAC_BIAS DAC_CHANNEL_1
#define DAC_THOLD DAC_CHANNEL_2

//Macro for checking/setting/clearing single bits
//#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))
//#define SET_BIT(var,pos) ((var) |= (1 << (pos)))
//#define CLR_BIT(var,pos) ((var) &= ~((1) << (pos)))

//Stolen from mbed/TARGET_DISCO_L476VG/cmsis_nvic.h
#define NVIC_RAM_VECTOR_ADDRESS   (0x20000000)  // Vectors positioned at start of RAM
#define NVIC_FLASH_VECTOR_ADDRESS (0x08000000)  // Initial vector position in flash
#define BOOTLOADER_FLAG_REGISTER 	RTC->BKP30R 	//Backup address for bootloader flags
#define BOOTLOADER_MAGIC_NUMBER 	42						// Flag to start the bootlader
// CORE: 16 vectors = 64 bytes from 0x00 to 0x3F
// MCU Peripherals: 82 vectors = 328 bytes from 0x40 to 0x187
// Total: 98 vectors = 392 bytes (0x188) to be reserved in RAM
#define NVIC_NUM_VECTORS      98
#define NVIC_USER_IRQ_OFFSET  16
#ifdef __cplusplus
extern "C" {
#endif
//void NVIC_SetVector(IRQn_Type IRQn, uint32_t vector);
//uint32_t NVIC_GetVector(IRQn_Type IRQn);
#ifdef __cplusplus
}
#endif


//Status bits for the status_flag
#define FLAG_RESET 		(0<<0) // No flags set, state after wakeup
#define ESP_RX_AVAIL	(1<<0) //New data from the ESP is available
#define USB_RX_AVAIL	(1<<1) //New data from the USB is available
#define PROC_CMD_ESP	(1<<2) //Next command in the cmd queue is from ESP
#define PROC_CMD_USB	(1<<3) //Next command in the cmd queue is from USB
#define MEAS_IDLE			(1<<4) //System is idle, no measurement
#define MEAS_RUNNING	(1<<5) //System is running, measurement
#define MEAS_FINISHED	(1<<6) //System is idle, but Measurement not yet transmitted
#define ESP_TRANSCOM	(1<<7) //ESP firmware update running
#define BGCOMPMODE		(1<<8) //Background spectrum loaded
#define DEBUG_MODE		(1<<9) //More comments send to the USB (high verbosity)
#define AUTOTCOMP			(1<<10) //Automatic temperatur compensation flag
#define ESP_TRANSFER	(1<<11) // Check for ongoing data transfer from USB to ESP UART
#define ESP_PROGGING	(1<<12) // Crawl around, hungry for the new ESP firmware data

#define ESP_TRANSPARENT_TIMEOUT 5000

#define DEFAULT_THRESHOLD 120//80 // Default threshold for the comp.
#define DEFAULT_BIAS 3000//2400 // Default bias ~ <27V
#define DEFAULT_GATETIME	1000 // Seconds


/* Definition for ESP USART_ESP clock resources */
#define USART_ESP                           USART3
#define USART_ESP_CLK_ENABLE()              __HAL_RCC_USART3_CLK_ENABLE();
#define DMA_ESP_CLK_ENABLE()                __HAL_RCC_DMA1_CLK_ENABLE()
#define USART_ESP_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()
#define USART_ESP_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOB_CLK_ENABLE()

#define USART_ESP_FORCE_RESET()             __HAL_RCC_USART3_FORCE_RESET()
#define USART_ESP_RELEASE_RESET()           __HAL_RCC_USART3_RELEASE_RESET()

/* Definition for USART_ESP Pins */
#define USART_ESP_TX_PIN                    GPIO_PIN_10
#define USART_ESP_TX_GPIO_PORT              GPIOB
#define USART_ESP_TX_AF                     GPIO_AF7_USART3
#define USART_ESP_RX_PIN                    GPIO_PIN_11
#define USART_ESP_RX_GPIO_PORT              GPIOB
#define USART_ESP_RX_AF                     GPIO_AF7_USART3

/* Definition for USART_ESP's DMA */
#define USART_ESP_TX_DMA_CHANNEL             DMA1_Channel4
#define USART_ESP_RX_DMA_CHANNEL             DMA1_Channel5

/* Definition for USART_ESP's DMA Request */
#define USART_ESP_TX_DMA_REQUEST             DMA_REQUEST_2
#define USART_ESP_RX_DMA_REQUEST             DMA_REQUEST_2

/* Definition for USART_ESP's NVIC */
#define USART_ESP_DMA_TX_IRQn                DMA1_Channel4_IRQn
#define USART_ESP_DMA_RX_IRQn                DMA1_Channel5_IRQn
#define USART_ESP_DMA_TX_IRQHandler          DMA1_Channel4_IRQHandler
#define USART_ESP_DMA_RX_IRQHandler          DMA1_Channel5_IRQHandler

/* Definition for USART_ESP's NVIC */
#define USART_ESP_IRQn                      USART3_IRQn
#define USART_ESP_IRQHandler                USART3_IRQHandler

void delayUS(uint32_t us);
void Check_Wakeup(void);
void Check_Sleep(void);

void Enter_Standby(void);
void JumpToBootloader(void);


//void _cmpISR(void);
void Init_RadIIS(void);
void Init_GPIOs(void);
void Init_VREF(void);
void Init_COMP(void);
void Init_DACs(void);
void Init_ADCs(void);

int32_t ReadFastADC(uint32_t pin);
int32_t ReadSlowADC(uint32_t pin);
int32_t getSiPMTemp(void);
int32_t getBatLevel(void);
void SetBiasVoltTC(uint32_t nom_mV);


#endif /* APPLICATION_USER_RADIIS_SYS_H_ */

/**
 ******************************************************************************
 * @file    USB_Device/CDC_Standalone/Src/usbd_cdc_interface.c
 * @author  MCD Application Team
 * @version V1.7.0
 * @date    17-February-2017
 * @brief   Source file for USBD CDC interface
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright(c) 2017 STMicroelectronics International N.V.
 * All rights reserved.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

#include "main.h"
#include "radiis_sys.h"

extern volatile HWSystem RadIIS_HW;

//TODO Proper command sctruct!
char global_command_ESP[5000];
char global_command_USB[5000];

// USB buffer (RX/TX) size:
#define APP_RX_DATA_SIZE  2048
#define APP_TX_DATA_SIZE  2048

USBD_CDC_LineCodingTypeDef LineCoding = { 115200, // baud rate
0x00, // stop bits-1
0x00, // parity - none
0x08 // nb. of bits 8
};

uint8_t USBRxBuffer[APP_RX_DATA_SIZE]; // Received Data over USB are stored in this buffer
uint8_t USBTxBuffer[APP_TX_DATA_SIZE]; // Received Data over UART are stored in this buffer

uint32_t USBTxBufPtrIn = 0; // Increment this pointer or roll it back to start address when data is copied to USBTxBuffer
uint32_t USBTxBufPtrOut = 0; // Increment this pointer or roll it back to start address when data are sent over USB

uint8_t ESPRxBuffer[1]; // Received Data over Serial (ESP) are stored in this buffer (so far every char is received individually)
// ESP TX uses UART Hardware buffer

uint32_t BuffLength;

// UART handler declaration
UART_HandleTypeDef UartHandle;
// TIM handler declaration
TIM_HandleTypeDef TimHandle;
// USB handler declaration
extern USBD_HandleTypeDef USBD_Device;
extern volatile HWSystem RadIIS_HW;

/* Private function prototypes -----------------------------------------------*/
static int8_t CDC_Itf_Init(void);
static int8_t CDC_Itf_DeInit(void);
static int8_t CDC_Itf_Control(uint8_t cmd, uint8_t* pbuf, uint16_t length);
static int8_t CDC_Itf_Receive(uint8_t* pbuf, uint32_t *Len);

static void Error_Handler(void);
static void ComPort_Config(void);
static void TIM_Config(void);

USBD_CDC_ItfTypeDef USBD_CDC_fops = { CDC_Itf_Init, CDC_Itf_DeInit,
		CDC_Itf_Control, CDC_Itf_Receive };

/* Private functions ---------------------------------------------------------*/

/**
 * @brief  CDC_Itf_Init
 *         Initializes the CDC media low layer
 * @param  None
 * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
 */
static int8_t CDC_Itf_Init(void) {
	/*##-1- Configure the UART peripheral ######################################*/
	/* Put the USART peripheral in the Asynchronous mode (UART Mode) */
	/* USART configured as follow:
	 - Word Length = 8 Bits
	 - Stop Bit    = One Stop bit
	 - Parity      = No parity
	 - BaudRate    = 115200 baud
	 - Hardware flow control disabled (RTS and CTS signals) */
	UartHandle.Instance = USART_ESP;
	UartHandle.Init.BaudRate = 115200;
	UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
	UartHandle.Init.StopBits = UART_STOPBITS_1;
	UartHandle.Init.Parity = UART_PARITY_NONE;
	UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	UartHandle.Init.Mode = UART_MODE_TX_RX;

	//Init UartBuffer:
	ESPRxBuffer[0] = 0;

	if (HAL_UART_Init(&UartHandle) != HAL_OK) {
		/* Initialization Error */
		Error_Handler();
	}

	/*##-2- Put UART peripheral in IT reception process ########################*/
	/* Any data received will be stored in "UserTxBuffer" buffer  */
	if (HAL_UART_Receive_IT(&UartHandle, (uint8_t *) ESPRxBuffer, 1) != HAL_OK) {
		/* Transfer error in reception process */
		Error_Handler();
	}

	/*##-3- Configure the TIM Base generation  #################################*/
	TIM_Config();

	/*##-4- Start the TIM Base generation in interrupt mode ####################*/
	/* Start Channel1 */
	if (HAL_TIM_Base_Start_IT(&TimHandle) != HAL_OK) {
		/* Starting Error */
		Error_Handler();
	}
	// Init Buffer:
	for(int32_t i=0;i<APP_RX_DATA_SIZE;i++)
	{
		USBRxBuffer[i] = 0;
	}
	for(int32_t i=0;i<APP_TX_DATA_SIZE;i++)
	{
		USBTxBuffer[i] = 0;
	}


	/*##-5- Set Application Buffers ############################################*/
	USBD_CDC_SetTxBuffer(&USBD_Device, USBTxBuffer, 0);
	USBD_CDC_SetRxBuffer(&USBD_Device, USBRxBuffer);

	return (USBD_OK);
}

/**
 * @brief  CDC_Itf_DeInit
 *         DeInitializes the CDC media low layer
 * @param  None
 * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
 */
static int8_t CDC_Itf_DeInit(void) {
	/* DeInitialize the UART peripheral */
	if (HAL_UART_DeInit(&UartHandle) != HAL_OK) {
		/* Initialization Error */
		Error_Handler();
	}
	return (USBD_OK);
}

/**
 * @brief  CDC_Itf_Control
 *         Manage the CDC class requests
 * @param  Cmd: Command code
 * @param  Buf: Buffer containing command data (request parameters)
 * @param  Len: Number of data to be sent (in bytes)
 * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
 */
static int8_t CDC_Itf_Control(uint8_t cmd, uint8_t* pbuf, uint16_t length) {

	switch (cmd) {
	case CDC_SEND_ENCAPSULATED_COMMAND:
		/* Add your code here */
		break;

	case CDC_GET_ENCAPSULATED_RESPONSE:
		/* Add your code here */
		break;

	case CDC_SET_COMM_FEATURE:
		/* Add your code here */
		break;

	case CDC_GET_COMM_FEATURE:
		/* Add your code here */
		break;

	case CDC_CLEAR_COMM_FEATURE:
		/* Add your code here */
		break;

	case CDC_SET_LINE_CODING:
		LineCoding.bitrate = (uint32_t) (pbuf[0] | (pbuf[1] << 8)
				|\
 (pbuf[2] << 16) | (pbuf[3] << 24));
		LineCoding.format = pbuf[4];
		LineCoding.paritytype = pbuf[5];
		LineCoding.datatype = pbuf[6];

		/* Set the new configuration */
		//ComPort_Config(); Should not be configured by the PC! Maybe TODO SCPI command?
		break;

	case CDC_GET_LINE_CODING:
		pbuf[0] = (uint8_t) (LineCoding.bitrate);
		pbuf[1] = (uint8_t) (LineCoding.bitrate >> 8);
		pbuf[2] = (uint8_t) (LineCoding.bitrate >> 16);
		pbuf[3] = (uint8_t) (LineCoding.bitrate >> 24);
		pbuf[4] = LineCoding.format;
		pbuf[5] = LineCoding.paritytype;
		pbuf[6] = LineCoding.datatype;
		break;

	case CDC_SET_CONTROL_LINE_STATE:
		/* Add your code here */
		break;

	case CDC_SEND_BREAK:
		/* Add your code here */
		break;

	default:
		break;
	}

	return (USBD_OK);
}

/**
 * @brief  Send buffer over the USB interface
 * @param  None
 * @retval None
 */
uint8_t Send_CharBuffer_USB(uint8_t *buffer, uint16_t length) {
	// Do not transmit anything if in ESP programming mode
	if(READ_BIT(RadIIS_HW.Flags,ESP_TRANSCOM))
		return DEFAULT_ERROR;

	// Do not transmit if length larger than buffer size (TODO, remaining buffer size?)
	if (length > APP_TX_DATA_SIZE)
		return DEFAULT_ERROR;

	// Copy message to USB out-buffer:
  int i;
	for (i = 0 ; i<length ; i++)
	{
		// Increment Index for buffer writing
		USBTxBufPtrIn++;

		// To avoid buffer overflow
		if (USBTxBufPtrIn == APP_TX_DATA_SIZE) {
			USBTxBufPtrIn = 0;
		}
		// Copy buffer to transmission buffer
		USBTxBuffer[USBTxBufPtrIn] = buffer[i];
	}
	return NOERROR;

}

/**
 * @brief  Send buffer over the ESP serial interface (DMA)
 * @param  None
 * @retval None
 */
uint8_t Send_CharBuffer_ESP(uint8_t *buffer, uint16_t length) {

	if(READ_BIT(RadIIS_HW.Flags,ESP_TRANSCOM))
		return DEFAULT_ERROR;

	return HAL_UART_Transmit(&UartHandle, buffer, length,1000000);
	//uint8_t errorc;
	//errorc = HAL_UART_Transmit_DMA(&UartHandle, buffer, length);
	//return errorc;

	//{
	//	return DEFAULT_ERROR;
	//}
	//return NOERROR;

}

/**
 * @brief  TIM period elapsed callback //Currently TIM3 with 1ms
 * @param  htim: TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {

	uint32_t buffptr;
	uint32_t buffsize;
	if (USBTxBufPtrOut != USBTxBufPtrIn) {
		if (USBTxBufPtrOut > USBTxBufPtrIn) /* Rollback */
		{
			buffsize = APP_RX_DATA_SIZE - USBTxBufPtrOut;
		} else {
			buffsize = USBTxBufPtrIn - USBTxBufPtrOut;
		}

		buffptr = USBTxBufPtrOut;

//TODO What if USB not used??
		USBD_CDC_SetTxBuffer(&USBD_Device, (uint8_t*) &USBTxBuffer[buffptr],
				buffsize);

		if (USBD_CDC_TransmitPacket(&USBD_Device) == USBD_OK) {
			USBTxBufPtrOut += buffsize;
			if (USBTxBufPtrOut == APP_RX_DATA_SIZE) {
				USBTxBufPtrOut = 0;
			}
		}
	}

}



/**
 * @brief  Rx Transfer completed callback (single char!)
 * @param  huart: UART handle
 * @retval None
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	if(READ_BIT(RadIIS_HW.Flags,ESP_TRANSCOM))
	{
		// Increment Index for buffer writing
		USBTxBufPtrIn++;

		/* To avoid buffer overflow */
		if (USBTxBufPtrIn == APP_RX_DATA_SIZE)
			USBTxBufPtrIn = 0;

		HAL_UART_Receive_IT(huart, (uint8_t *) (USBTxBuffer + USBTxBufPtrIn), 1);
	}
	else
	{
		// Check if LineFeed (LF) is detected
		//	Yes:
		// 			Flag command received
		// 	No:
		//			Add string to current command

		if (ESPRxBuffer[0] == 10) // if LF is detected
		{
			SET_BIT(RadIIS_HW.Flags,ESP_RX_AVAIL); // Flag received command
		}
		else
		{
			strcat(global_command_ESP,(char*)ESPRxBuffer); //No LF, just copy everything to the command buffer
		}
		// Start another reception:
		HAL_UART_Receive_IT(huart, (uint8_t *) ESPRxBuffer, 1);
	}
}

/**
 * @brief  CDC_Itf_DataRx
 *         Data received over USB OUT endpoint are sent over CDC interface
 *         through this function.
 * @param  Buf: Buffer of data to be transmitted
 * @param  Len: Number of data received (in bytes)
 * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
 */
static int8_t CDC_Itf_Receive(uint8_t* Buf, uint32_t *Len) {

	//Send_CharBuffer_USB(Buf,*Len);
	if(READ_BIT(RadIIS_HW.Flags,ESP_TRANSCOM))
	{
		SET_BIT(RadIIS_HW.Flags,ESP_TRANSFER);
		//HAL_UART_Transmit_DMA(&UartHandle, Buf, *Len); //TODO Solve DMA, do real com
		HAL_UART_Transmit(&UartHandle, Buf, *Len,1000000);
	}
	else //if (!READ_BIT(RadIIS_HW.Flags,USB_RX_AVAIL))
	{

		// Check if LineFeed (LF) is detected
		//	Yes:
		// 			1) Put everything up to LF in the current command
		// 			2) Flag command received
		//			3) Discard commands until command is processed
		// 	No:
		//			1) Add string to current command

		char *PLF = strchr((char *)Buf,10); // Returns a pointer to the first occurrence of the char 10 (LF)
		if (PLF != NULL) // if LF is detected
		{
			if (PLF-(char *)Buf) // Check if there is more than a LF
			{
				strncat(global_command_USB,(char*)Buf,PLF-(char *)Buf); // Copy everything to the command buffer
			}
			SET_BIT(RadIIS_HW.Flags,USB_RX_AVAIL); // Flag received command
		}
		else
		{
			strcat(global_command_USB,(char*)Buf); //No LF, just copy everything to the command buffer
		}
	}

	USBD_CDC_ReceivePacket(&USBD_Device); // Start new USB reception
	return (USBD_OK);
}

/**
 * @brief  Tx Transfer completed callback
 * @param  huart: UART handle
 * @retval None
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
	/* Initiate next USB packet transfer once UART completes transfer (transmitting data over Tx line) */
	//USBD_CDC_ReceivePacket(&USBD_Device);
}

/**
 * @brief  ComPort_Config
 *         Configure the COM Port with the parameters received from host.
 * @param  None.
 * @retval None.
 * @note   When a configuration is not supported, a default value is used.
 */
static void ComPort_Config(void) {
	if (HAL_UART_DeInit(&UartHandle) != HAL_OK) {
		/* Initialization Error */
		Error_Handler();
	}

	/* set the Stop bit */
	switch (LineCoding.format) {
	case 0:
		UartHandle.Init.StopBits = UART_STOPBITS_1;
		break;
	case 2:
		UartHandle.Init.StopBits = UART_STOPBITS_2;
		break;
	default:
		UartHandle.Init.StopBits = UART_STOPBITS_1;
		break;
	}

	/* set the parity bit*/
	switch (LineCoding.paritytype) {
	case 0:
		UartHandle.Init.Parity = UART_PARITY_NONE;
		break;
	case 1:
		UartHandle.Init.Parity = UART_PARITY_ODD;
		break;
	case 2:
		UartHandle.Init.Parity = UART_PARITY_EVEN;
		break;
	default:
		UartHandle.Init.Parity = UART_PARITY_NONE;
		break;
	}

	/*set the data type : only 8bits and 9bits is supported */
	switch (LineCoding.datatype) {
	case 0x07:
		/* With this configuration a parity (Even or Odd) must be set */
		UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
		break;
	case 0x08:
		if (UartHandle.Init.Parity == UART_PARITY_NONE) {
			UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
		} else {
			UartHandle.Init.WordLength = UART_WORDLENGTH_9B;
		}

		break;
	default:
		UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
		break;
	}

	UartHandle.Init.BaudRate = LineCoding.bitrate;
	UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	UartHandle.Init.Mode = UART_MODE_TX_RX;
	UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;

	if (HAL_UART_Init(&UartHandle) != HAL_OK) {
		/* Initialization Error */
		Error_Handler();
	}

	/* Start reception: provide the buffer pointer with offset and the buffer size */
	HAL_UART_Receive_IT(&UartHandle, (uint8_t *) ESPRxBuffer, 1);
}

/**
 * @brief  TIM_Config: Configure TIMx timer
 * @param  None.
 * @retval None.
 */
static void TIM_Config(void) {
	/* Set TIMx instance */
	TimHandle.Instance = TIMx;

	/* Initialize TIM3 peripheral as follow:
	 + Period = 10000 - 1
	 + Prescaler = ((SystemCoreClock/2)/10000) - 1
	 + ClockDivision = 0
	 + Counter direction = Up
	 */
	TimHandle.Init.Period = (CDC_POLLING_INTERVAL * 1000) - 1;
	TimHandle.Init.Prescaler = 84 - 1;
	TimHandle.Init.ClockDivision = 0;
	TimHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
	if (HAL_TIM_Base_Init(&TimHandle) != HAL_OK) {
		/* Initialization Error */
		Error_Handler();
	}
}

/**
 * @brief  UART error callbacks
 * @param  UartHandle: UART handle
 * @retval None
 */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle) {
	/* Transfer error occured in reception and/or transmission process */
	Error_Handler();
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
static void Error_Handler(void) {
	/* Add your own code here */
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/*
 * radiis_sys.c
 *
 *  Created on: Apr 9, 2017
 *      Author: Merlin Schaufel
 */

//#include "main.h"
#include "main.h"

volatile HWSystem RadIIS_HW; //Main hardware config and flags
volatile Measurement current_measurement; //Data/status of the running measurement
volatile Measurement temp_measurement; //Data/status of the "Always-active" measurement
volatile Background current_background; //Placeholder for the background to be loaded from ESP or PC

//volatile uint32_t Triggerrate_bgfree;//TODO Add background and calculation for a background corrected trigger rate

//Global Hardware Handles:

DAC_HandleTypeDef	DacHandle; //DAC handler for threshold and bias regulation
ADC_HandleTypeDef FastAdcHandle; //Fast ADC handler for Pulsehold
ADC_HandleTypeDef SlowAdcHandle; //Slow ADC handler (Battery , Bias ...)


COMP_HandleTypeDef	CompHandle_P1; //COMP analog pulse comparator input 1
COMP_HandleTypeDef	CompHandle_P2; //COMP analog pulse comparator input 2

const char *scpi_eol = "\r\n";

//TODO ..... remove all the mess: Merge hal_msp+usbd_cdc_interf into radiis_sys.c!!!



static void Error_Handler(uint8_t error_code);
void _cmpISR(void);

//***************User functions*****************//

void delayUS(uint32_t us) {
	volatile uint32_t count = 7*us;
	while(count--);
}

 /**
  * @brief Function to perform jump to system memory boot from user application
  *
  * @param  None
  * @retval None
  */
void JumpToBootloader(void) {
	void (*SysMemBootJump)(void);

	/**
	 * Step: Set system memory address.
	 *
	 *       For STM32L4, system memory is on 0x1FFF 0000
	 *       For other families, check AN2606 document table 110 with descriptions of memory addresses
	 */
	volatile uint32_t addr = 0x1FFF0000;

	/**
	 * Step: Disable RCC, set it to default (after reset) settings
	 *       Internal clock, no PLL, etc.
	 */
	HAL_RCC_DeInit();

	/**
	 * Step: Disable systick timer and reset it to default values
	 */
	SysTick->CTRL = 0;
	SysTick->LOAD = 0;
	SysTick->VAL = 0;

	/**
	 * Step: Disable all interrupts
	 */
	//__disable_irq();
  __set_PRIMASK(1);
	/**
	 * Step: Remap system memory to address 0x0000 0000 in address space
	 *       For each family registers may be different.
	 *       Check reference manual for each family.
	 *
	 *       For STM32L4xx, MEMRMP register in SYSCFG is used (bits[1:0])
	 *       For STM32F4xx, MEMRMP register in SYSCFG is used (bits[1:0])
	 *       For STM32F0xx, CFGR1 register in SYSCFG is used (bits[1:0])
	 *       For others, check family reference manual
	 */
	//Remap by hand... {
		SYSCFG->MEMRMP = 0x01;
	//} ...or if you use HAL drivers
	//__HAL_SYSCFG_REMAPMEMORY_SYSTEMFLASH();	//Call HAL macro to do this for you

	/**
	 * Step: Set jump memory location for system memory
	 *       Use address with 4 bytes offset which specifies jump location where program starts
	 */
	SysMemBootJump = (void (*)(void)) (*((uint32_t *)(addr + 4)));

	/**
	 * Step: Set main stack pointer.
	 *       This step must be done last otherwise local variables in this function
	 *       don't have proper value since stack pointer is located on different position
	 *
	 *       Set direct address location which specifies stack pointer in SRAM location
	 */
	__set_MSP(*(uint32_t *)addr);

	/**
	 * Step: Actually call our function to jump to set location
	 *       This will start system memory execution
	 */
	SysMemBootJump();

	/**
	 * Step: Connect USB<->UART converter to dedicated USART pins and test
	 *       and test with bootloader works with STM32 Flash Loader Demonstrator software
	 */
}

/**
 * @brief Check State after wakeup
 *
 * @param  None
 * @retval None
 */
void Check_Wakeup(void)
{
	if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
	{
		char buffer[20];
		uint16_t n = sprintf(buffer,"WakeUP!\n");
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	}
	//Check if Button is still pressed
	if(!HAL_GPIO_ReadPin(GPIOC,Pin_Button))
	{
		//Wait 3s to ensure intended activation
		HAL_Delay(1500);
		//Check if Button is still pressed
		if(!HAL_GPIO_ReadPin(GPIOC,Pin_Button))
		{
			//Wait 1s for release
			HAL_Delay(1000);
			//If released, start
			if(HAL_GPIO_ReadPin(GPIOC,Pin_Button))
			{
				return;
			}
			else
			{
				//If not released go to sleep ...
				Enter_Standby();
			}
		}
		else
		{
			//If release signal is ignored got to sleep as well
			Enter_Standby();
		}
	}
	else
	{
		Enter_Standby();
	}
}

void Check_Sleep(void)
{
	//HACK Quick and dirty:
	if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
	{
		char buffer[20];
		uint16_t n = sprintf(buffer,"Button pressed!\n");
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	}

	//Check if Button is still pressed
	if(!HAL_GPIO_ReadPin(GPIOC,Pin_Button))
	{
		//Wait 3s to ensure intended activation
		HAL_Delay(1500);
		//Check if Button is still pressed
		if(!HAL_GPIO_ReadPin(GPIOC,Pin_Button))
		{
			//Wait 1s for release
			HAL_Delay(1000);
			//If released, enter standby
			if(HAL_GPIO_ReadPin(GPIOC,Pin_Button))
			{
				if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
				{
					char buffer[30];
					uint16_t n = sprintf(buffer,"Released -> Standby!\n");
					Send_CharBuffer_USB((uint8_t *)buffer,n);
				}
				Enter_Standby();
			}
			else
			{
				//If not released return
				if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
				{
					char buffer[30];
					uint16_t n = sprintf(buffer,"Button not released!\n");
					Send_CharBuffer_USB((uint8_t *)buffer,n);
				}
				return;
			}
		}
		else
		{
			//If not 3s pressed return
			if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
			{
				char buffer[30];
				uint16_t n = sprintf(buffer,"Button early released!\n");
				Send_CharBuffer_USB((uint8_t *)buffer,n);
			}
			return;
		}
	}
}

/**
 * @brief Enter stop mode 1 and disable all other hardware!
 *
 * @param  None
 * @retval None
 */
void Enter_Standby(void)
{
	//TODO Switch off everything !! (To be shifted to ESP!)
		//Shutdown analog circutiry (Bias, Amp, Shaper, PulseHold, ADC)
		//TODO Is that the proper way,ADC is enabled!??? :
		//__HAL_RCC_ADC_CLK_DISABLE();
		//__HAL_RCC_DAC1_CLK_DISABLE();
		//__HAL_RCC_SYSCFG_CLK_DISABLE();
		HAL_GPIO_WritePin(GPIOB,Pin_SHNDAn, GPIO_PIN_RESET);
		// Disable ESP
		HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown, GPIO_PIN_RESET);

		//Disable LED
		HAL_GPIO_WritePin(GPIOC,Pin_LEDStat, GPIO_PIN_RESET);
		//Disable LED

		//
		RadIIS_HW.Status = STANDBY;
		//Sleep!
		HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON,PWR_STOPENTRY_WFI);
}

/**
 * @brief Read the SiPM temperature
 *
 * @param  None
 * @retval int32_t: Temperature in mC
 */
int32_t getSiPMTemp(void)
{
	//TMP36: U = 0.5V + 0.01 V/C*T <-> T = (U-0.5V)*100C/V
	// 2.048V Vref -> 4096 ADCcounts
	int32_t temp_temp = 0;
	for(int i = 0;i<1000;i++)
	{
		//TODO subtract offset?
		temp_temp += ReadSlowADC(Pin_TempIn);
	}

// To mV:
temp_temp = ((temp_temp/10)*2048)/4096;
// To mC
temp_temp = (temp_temp-50000);

return temp_temp;
}

int32_t getBatLevel(void)
{
	//TMP36: U = 0.5V + 0.01 V/C*T <-> T = (U-0.5V)*100C/V
	// 2.048V Vref -> 4096 ADCcounts
	int32_t Bat_Level = 0;
	for(int i = 0;i<10;i++)
	{
		//TODO subtract offset?
		Bat_Level += ReadSlowADC(Pin_BatIn);
	}

	// To mV:
	Bat_Level = ((Bat_Level/10)*2048)/4096;
	// To mV at Battery (1M+2M2 Ohm Divider):
	Bat_Level = (32*Bat_Level)/22;


	return Bat_Level;
}

/**
 * @brief Sets the bias voltage without temp. compensation
 *
 * @param uint32_t bias_mV: Bias voltage in mV
 * @retval None
 */

void SetBiasVolt(uint32_t bias_mV){

	//24304mV Offset Voltage, 1.3796 DAC gain factor TODO: Change wrong resistors!
	if ((bias_mV<MIN_BIAS) || (bias_mV > MAX_BIAS))
		Error_Handler(SETBIAS_ERROR);

 	uint32_t DAC_TICKS = (bias_mV-MIN_BIAS)*10000/13796;

	if (HAL_DAC_SetValue(&DacHandle, DAC_BIAS, DAC_ALIGN_12B_R, DAC_TICKS) != HAL_OK){
		Error_Handler(SETDACB_ERROR);
	}
	//Update bias voltage (only here!)
	RadIIS_HW.Bias = bias_mV;
  return;
}

/**
 * @brief Sets the bias voltage with temp. compensation
 *           -
 * @param uint32_t nom_mV: Nominal voltage for the standard temp. of 21 C
 * @retval None
 */
void SetBiasVoltTC(uint32_t nom_mV)
{
	int32_t current_tempdiff = getSiPMTemp()-NomTemp_SiPM; //
	uint32_t comp_mV = nom_mV + (current_tempdiff*TempCoeff_SiPM)/1000;

	if (comp_mV > MAX_BIAS)
		SetBiasVolt(MAX_BIAS);
	else if(comp_mV < MIN_BIAS)
		SetBiasVolt(MIN_BIAS);
	else
		SetBiasVolt(comp_mV);
	return;
}

void SetThreshold(uint16_t threshold)
{
	RadIIS_HW.Threshold = threshold;
	if (HAL_DAC_SetValue(&DacHandle, DAC_THOLD, DAC_ALIGN_12B_R,threshold) != HAL_OK)
	{
				Error_Handler(SETDACC_ERROR);
	}
	return;
}

int32_t ReadSlowADC(uint32_t pin)
{
		ADC_ChannelConfTypeDef sConfig;

		switch(pin)
		{
			case Pin_TempIn:
				sConfig.Channel      = ADC_CHANNEL_7;
				break;
			case Pin_BiasIn:
				sConfig.Channel      = ADC_CHANNEL_8;
				break;
			case Pin_BatIn:
				sConfig.Channel      = ADC_CHANNEL_10;
				break;
			default:
			break;
		}
		sConfig.Rank         = ADC_REGULAR_RANK_1;          // Rank of sampled channel number ADCx_CHANNEL
		sConfig.SamplingTime = ADC_SAMPLETIME_6CYCLES_5;    // Sampling time (number of clock cycles unit)
		sConfig.SingleDiff   = ADC_SINGLE_ENDED;            // Single-ended input channel
		sConfig.OffsetNumber = ADC_OFFSET_NONE;             // No offset subtraction
		sConfig.Offset = 0;                                 // Parameter discarded because offset correction is disabled

		if (HAL_ADC_ConfigChannel(&SlowAdcHandle, &sConfig) != HAL_OK)
		{
			// Channel Configuration Error
			//Error_Handler(DEFAULT_ERROR);
			return -1;
		}

		if (HAL_ADC_Start(&SlowAdcHandle) != HAL_OK)
		{
			// Start Conversation Error
			//Error_Handler(DEFAULT_ERROR);
			return -1;
		}

		//##-4- Wait for the end of conversion #####################################
		//  For simplicity reasons, this example is just waiting till the end of the conversion, but application may perform other tasks while conversion operation is ongoing.
		if (HAL_ADC_PollForConversion(&SlowAdcHandle, 10) != HAL_OK)
		{
			// End Of Conversion flag not set on time
			//Error_Handler(DEFAULT_ERROR);
			return -1;
		}
		__IO uint16_t uhADCxConvertedValue;
		// Check if the continous conversion of regular channel is finished
		if ((HAL_ADC_GetState(&SlowAdcHandle) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC)
		{
			//##-5- Get the converted value of regular channel  ########################
			uhADCxConvertedValue = HAL_ADC_GetValue(&SlowAdcHandle);
		}

		return uhADCxConvertedValue;
}

int32_t ReadFastADC(uint32_t pin)
{
	//TODO Optimize this ****, touto trigger with comperator
	ADC_ChannelConfTypeDef sConfig;

	switch(pin)
	{
	case Pin_PH1:
		sConfig.Channel      = ADC_CHANNEL_6;
		break;
	case Pin_PH2:
		sConfig.Channel      = ADC_CHANNEL_5;
		break;
	default:
		break;
	}
	sConfig.Rank         = ADC_REGULAR_RANK_1;          // Rank of sampled channel number ADCx_CHANNEL
	sConfig.SamplingTime = ADC_SAMPLETIME_6CYCLES_5;    // Sampling time (number of clock cycles unit)
	sConfig.SingleDiff   = ADC_SINGLE_ENDED;            // Single-ended input channel
	sConfig.OffsetNumber = ADC_OFFSET_NONE;             // No offset subtraction
	sConfig.Offset = 0;                                 // Parameter discarded because offset correction is disabled

	if (HAL_ADC_ConfigChannel(&FastAdcHandle, &sConfig) != HAL_OK)
	{
		// Channel Configuration Error
		//Error_Handler(DEFAULT_ERROR);
		return -1;
	}

	if (HAL_ADC_Start(&FastAdcHandle) != HAL_OK)
	{
		// Start Conversation Error
		//Error_Handler(DEFAULT_ERROR);
		return -1;
	}

	//##-4- Wait for the end of conversion #####################################
	//  For simplicity reasons, this example is just waiting till the end of the conversion, but application may perform other tasks while conversion operation is ongoing.
	if (HAL_ADC_PollForConversion(&FastAdcHandle, 10) != HAL_OK)
	{
		// End Of Conversion flag not set on time
		//Error_Handler(DEFAULT_ERROR);
		return -1;
	}
	__IO uint16_t uhADCxConvertedValue;
	// Check if the continous conversion of regular channel is finished
	if ((HAL_ADC_GetState(&FastAdcHandle) & HAL_ADC_STATE_REG_EOC) == HAL_ADC_STATE_REG_EOC)
	{
		//##-5- Get the converted value of regular channel  ########################
		uhADCxConvertedValue = HAL_ADC_GetValue(&FastAdcHandle);
	}

	return uhADCxConvertedValue;
}


/**
 * @brief System initialization:
 *        This function initialize the RadIIS Hardware after reset
 *           - Init GPIOs, DACs, ADCs ..
 *           - Sets everything to default
 * @param None
 * @retval None
 */
void Init_RadIIS() {


	RadIIS_HW.Flags = FLAG_RESET;
	RadIIS_HW.Status = ONINIT;
	RadIIS_HW.GateTime = DEFAULT_GATETIME;
	RadIIS_HW.ButtonTs = 0;
	RadIIS_HW.Count = 0;
	RadIIS_HW.ExCount = 0;
	RadIIS_HW.Rate = 0;
	RadIIS_HW.ExRate = 0;
	RadIIS_HW.BiasNom = 28032;

	Init_Memory();
	Init_GPIOs();
	Init_VREF();
	Init_ADCs();
	Init_DACs();
	Init_COMP();

	return;
}


//*********************Internal functions*********************//


/**
 * @brief Custom meomory initialization
 *        This function inits memory for the measurements and background
 * @param None
 * @retval None
 */
void Init_Memory()
{
	//Reset rate measurement:
	for(int i = 0; i < RESOLUTION ; i++)
	{
		temp_measurement.current_spectrum[i] = 0;
		current_background.spectrum[i] = 0;
	}
current_background.Tlive = 1; //Needs to be > 0 as it is used per default X/0 will crash... TODO!: Check this if set!
current_background.Date = 0;

}


/**
 * @brief Custom GPIOs initialization
 *        This function configures the GPIO hardware:
 *           - Peripheral's clock enable
 * @param None
 * @retval None
 */
void Init_GPIOs()
{

	static GPIO_InitTypeDef  GPIO_InitStruct;

	//Enable all GPIO clocks A-E
	//__HAL_RCC_GPIOA_CLK_ENABLE(); //TODO Needed for ADC?
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();

	//************Init all low speed GPIOs Outputs***********//
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;

	//PortB:
	GPIO_InitStruct.Pin = Pin_SHNDAn | Pin_SHNDEx | Pin_Buzzer ;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	//PortC:
	GPIO_InitStruct.Pin = Pin_LEDStat ;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	//PortE:
	GPIO_InitStruct.Pin = Pin_LEDTrg | Pin_ESPpdown | Pin_ESPboot ; //LEDTrg maybe high-speed?
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);


	//************Init all high speed GPIOs Outputs***********//
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;//TODO Does not make any diff. //GPIO_SPEED_FREQ_VERY_HIGH;

	//PortC:
	GPIO_InitStruct.Pin = Pin_ResetPH1 | Pin_ResetPH2 ;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);


	//****************Init all GPIOs Inputs*******************//
	GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;

	//PortB:
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Pin = Pin_ChStat;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	//PortC:
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Pin = Pin_ESPready;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	//Interrupts:
	//PortC:
	GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Pin = Pin_Button;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	// Enable and set Button EXTI Interrupt to the lowest priority:
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0x0F, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);


	//********Setting Default values to the outputs**********//

	HAL_GPIO_WritePin(GPIOB,Pin_SHNDAn | Pin_SHNDEx,GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC,Pin_LEDStat,GPIO_PIN_SET);

	//*******************Enable ESP**************************//

	HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_RESET);

	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOE,Pin_ESPboot,GPIO_PIN_SET);
	HAL_Delay(100);

	HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_SET);

}


/**
 * @brief Custom VREFBUF initialization (2.048V Output for bias regulation +ADC/DAC reference)
 *        This function configures the VREFBUF hardware:
 *           - Peripheral's clock enable
 *           - Peripheral's configuration (Low impedance mode, 2.048V)
 * @param None
 * @retval None
 */
void Init_VREF(void)
{

    __HAL_RCC_SYSCFG_CLK_ENABLE();
	HAL_SYSCFG_VREFBUF_VoltageScalingConfig(SYSCFG_VREFBUF_VOLTAGE_SCALE0);
	HAL_SYSCFG_VREFBUF_HighImpedanceConfig(SYSCFG_VREFBUF_HIGH_IMPEDANCE_DISABLE);

	if (HAL_SYSCFG_EnableVREFBUF() != HAL_OK)
	{
		Error_Handler(ENVREFO_ERROR);
	}
}


/**
 * @brief Custom COMP initialization (Analog pulse input comperator (Interrupt))
 *        This function configures the COMP hardware:
 *           - Peripheral's clock enable
 *           - Peripheral's GPIO Configuration
 *           - Interrupt Configuration
 * @param None
 * @retval None
 */
void Init_COMP()
{

	GPIO_InitTypeDef GPIO_InitStructure;

	// GPIOB Peripheral clock enable
	__GPIOB_CLK_ENABLE();
	// Configure PB5 in analog closes the switch GR6-2: PB5 is connected to COMP2 non inverting input
	//Old, PB5 new PB4!
	GPIO_InitStructure.Pin = GPIO_PIN_2 | GPIO_PIN_4;
	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF12_COMP1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);


	CompHandle_P1.Instance = COMP1;
	CompHandle_P1.Init.NonInvertingInput = COMP_NONINVERTINGINPUT_IO2;
	CompHandle_P1.Init.InvertingInput = COMP_INVERTINGINPUT_DAC1_CH2;
	CompHandle_P1.Init.OutputPol = COMP_OUTPUTPOL_NONINVERTED;
	CompHandle_P1.Init.Mode = COMP_MODE_HIGHSPEED;
	CompHandle_P1.Init.TriggerMode = COMP_TRIGGERMODE_IT_RISING;
	// Start & marry GPIO w/ COMP <-- Congrats!
	HAL_COMP_Init(&CompHandle_P1);

	CompHandle_P2.Instance = COMP2;
	CompHandle_P2.Init.NonInvertingInput = COMP_NONINVERTINGINPUT_IO1;
	CompHandle_P2.Init.InvertingInput = COMP_INVERTINGINPUT_DAC1_CH2;
	CompHandle_P2.Init.OutputPol = COMP_OUTPUTPOL_NONINVERTED;
	CompHandle_P2.Init.Mode = COMP_MODE_HIGHSPEED;
	CompHandle_P2.Init.TriggerMode = COMP_TRIGGERMODE_IT_RISING;
	// Start & marry GPIO w/ COMP <-- Congrats!
	HAL_COMP_Init(&CompHandle_P2);



	HAL_COMP_Start_IT(&CompHandle_P1);
	HAL_COMP_Start_IT(&CompHandle_P2);

	NVIC_EnableIRQ(COMP_IRQn);


}

/**
 * @brief Custom DAC initialization (Bias regulation and comperator reference)
 *        This function configures the 2 DACs hardware:
 *           - Peripheral's clock enable
 *           - Peripheral's GPIO Configuration
 * @param None
 * @retval None
 */
void Init_DACs()
{
	static DAC_ChannelConfTypeDef sConfig_Bias;
	static DAC_ChannelConfTypeDef sConfig_Comp;

	GPIO_InitTypeDef          GPIO_InitStruct;

	//Enable Bias reg. GPIO clock (DAC out)
	__HAL_RCC_GPIOA_CLK_ENABLE();
	//Enable DAC clock
	__HAL_RCC_DAC1_CLK_ENABLE();

	//Configure Bias reg. GPIO (DAC out)
	GPIO_InitStruct.Pin = GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	DacHandle.Instance = DAC;
	//DacHandle_Bias.Instance = DAC;


	//Configure the DAC peripherals:
	if (HAL_DAC_Init(&DacHandle) != HAL_OK) {
		/* Initialization Error */
		Error_Handler(DACINIT_ERROR);
	}

	//Set the DAC configures:
	sConfig_Bias.DAC_SampleAndHold = DAC_SAMPLEANDHOLD_DISABLE;
	sConfig_Bias.DAC_Trigger = DAC_TRIGGER_NONE;
	sConfig_Bias.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
	sConfig_Bias.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_DISABLE;
	sConfig_Bias.DAC_UserTrimming = DAC_TRIMMING_FACTORY;

	sConfig_Comp.DAC_SampleAndHold = DAC_SAMPLEANDHOLD_DISABLE;
	sConfig_Comp.DAC_Trigger = DAC_TRIGGER_NONE;
	sConfig_Comp.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
	sConfig_Comp.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_ENABLE;
	sConfig_Comp.DAC_UserTrimming = DAC_TRIMMING_FACTORY;

	//Config DAC channels
	if (HAL_DAC_ConfigChannel(&DacHandle, &sConfig_Comp, DAC_THOLD)!= HAL_OK) {
		/* Channel configuration Error */
		Error_Handler(DACINIT_ERROR);
	}
	if (HAL_DAC_ConfigChannel(&DacHandle, &sConfig_Bias, DAC_BIAS)!= HAL_OK) {
		/* Channel configuration Error */
		Error_Handler(DACINIT_ERROR);
	}

	//Set DACs to zero
	if (HAL_DAC_SetValue(&DacHandle, DAC_BIAS, DAC_ALIGN_8B_R, 0x00)!= HAL_OK) {
		/* Setting value Error */
		Error_Handler(DACINIT_ERROR);
	}
	if (HAL_DAC_SetValue(&DacHandle, DAC_THOLD, DAC_ALIGN_8B_R, 0x00)!= HAL_OK) {
		/* Setting value Error */
		Error_Handler(DACINIT_ERROR);
	}

	//Enable DAC Channels
	if (HAL_DAC_Start(&DacHandle, DAC_THOLD) != HAL_OK) {
		/* Start Error */
		Error_Handler(DACINIT_ERROR);
	}
	if (HAL_DAC_Start(&DacHandle, DAC_BIAS) != HAL_OK) {
		/* Start Error */
		Error_Handler(DACINIT_ERROR);
	}

	if (HAL_DAC_SetValue(&DacHandle, DAC_THOLD, DAC_ALIGN_12B_R,DEFAULT_THRESHOLD) != HAL_OK)
	{
			Error_Handler(SETDACC_ERROR);
	}

	if (HAL_DAC_SetValue(&DacHandle, DAC_BIAS, DAC_ALIGN_12B_R,DEFAULT_BIAS) != HAL_OK)
	{
			Error_Handler(SETDACB_ERROR);
	}

}

/**
 * @brief Custom ADC initialization (TempMon, BiasMon, PulsHold, BatMon, ...)
 *        This function configures the ADC hardware:
 *           - Peripheral's clock enable
 *           - Peripheral's GPIO Configuration
 * @param None
 * @retval None
 */

void Init_ADCs()
{

	//TODO Change all define from example + think about smarter inits in msp (see example!)
	GPIO_InitTypeDef          GPIO_InitStruct;
	//##-1- Enable peripherals and GPIO Clocks #################################
	// ADC Periph clock enable
	__HAL_RCC_ADC_CLK_ENABLE();
	// ADC Periph interface clock configuration
	__HAL_RCC_ADC_CONFIG(RCC_ADCCLKSOURCE_SYSCLK);
	// Enable GPIO clock ***************************************
	__HAL_RCC_GPIOA_CLK_ENABLE();

	//##-2- Configure peripheral GPIO ##########################################
	// ADC Channel GPIO pin configuration
	GPIO_InitStruct.Pin = Pin_PH2|Pin_PH1|Pin_TempIn|Pin_BiasIn|Pin_BatIn|Pin_PulsC;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	ADC_ChannelConfTypeDef sConfig;


	//##-1- Configure the ADC peripheral #######################################
	FastAdcHandle.Instance = ADC1;
	SlowAdcHandle.Instance = ADC2;

	if ((HAL_ADC_DeInit(&FastAdcHandle) != HAL_OK) && (HAL_ADC_DeInit(&SlowAdcHandle) != HAL_OK))
	{
		// ADC de-initialization Error
		Error_Handler(DEFAULT_ERROR);
	}

	//Init Fast ADC:

	FastAdcHandle.Init.ClockPrescaler        = ADC_CLOCK_ASYNC_DIV1;          // Asynchronous clock mode, input ADC clock not divided
	FastAdcHandle.Init.Resolution            = ADC_RESOLUTION_12B;             // 12-bit resolution for converted data
	FastAdcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;           // Right-alignment for converted data
	FastAdcHandle.Init.ScanConvMode          = DISABLE;                       // Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1)
	FastAdcHandle.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;           // EOC flag picked-up to indicate conversion end
	FastAdcHandle.Init.LowPowerAutoWait      = DISABLE;                       // Auto-delayed conversion feature disabled
	FastAdcHandle.Init.ContinuousConvMode    = DISABLE;                       // Continuous mode disabled to have only 1 conversion at each conversion trig
	FastAdcHandle.Init.NbrOfConversion       = 1;                             // Parameter discarded because sequencer is disabled
	FastAdcHandle.Init.DiscontinuousConvMode = DISABLE;                       // Parameter discarded because sequencer is disabled
	FastAdcHandle.Init.NbrOfDiscConversion   = 1;                             // Parameter discarded because sequencer is disabled
	FastAdcHandle.Init.ExternalTrigConv      = ADC_SOFTWARE_START;            // Software start to trig the 1st conversion manually, without external event
	FastAdcHandle.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE; // Parameter discarded because software trigger chosen
	FastAdcHandle.Init.DMAContinuousRequests = DISABLE;                       // DMA one-shot mode selected (not applied to this example)
	FastAdcHandle.Init.Overrun               = ADC_OVR_DATA_OVERWRITTEN;      // DR register is overwritten with the last conversion result in case of overrun
	FastAdcHandle.Init.OversamplingMode      = DISABLE;                       // No oversampling

	if (HAL_ADC_Init(&FastAdcHandle) != HAL_OK)
	{
		// ADC initialization Error
		Error_Handler(DEFAULT_ERROR);
	}

	//##-2- Configure ADC regular channel ######################################
	sConfig.Channel      = ADC_CHANNEL_6;               // Sampled channel number PH1 default
	sConfig.Rank         = ADC_REGULAR_RANK_1;          // Rank of sampled channel number ADCx_CHANNEL
	sConfig.SamplingTime = ADC_SAMPLETIME_6CYCLES_5;    // Sampling time (number of clock cycles unit)
	sConfig.SingleDiff   = ADC_SINGLE_ENDED;            // Single-ended input channel
	sConfig.OffsetNumber = ADC_OFFSET_NONE;             // No offset subtraction
	sConfig.Offset = 0;                                 // Parameter discarded because offset correction is disabled

	if (HAL_ADC_ConfigChannel(&FastAdcHandle, &sConfig) != HAL_OK)
	{
		// Channel Configuration Error
		Error_Handler(DEFAULT_ERROR);
	}


	//##-3- Calibrate ADC then Start the conversion process ####################
	if (HAL_ADCEx_Calibration_Start(&FastAdcHandle, ADC_SINGLE_ENDED) !=  HAL_OK)
	{
		// ADC Calibration Error
		Error_Handler(DEFAULT_ERROR);
	}

	// Init Slow ADC :

	SlowAdcHandle.Init.ClockPrescaler        = ADC_CLOCK_ASYNC_DIV1;          // Asynchronous clock mode, input ADC clock not divided
	SlowAdcHandle.Init.Resolution            = ADC_RESOLUTION_12B;             // 12-bit resolution for converted data
	SlowAdcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;           // Right-alignment for converted data
	SlowAdcHandle.Init.ScanConvMode          = DISABLE;                       // Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1)
	SlowAdcHandle.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;           // EOC flag picked-up to indicate conversion end
	SlowAdcHandle.Init.LowPowerAutoWait      = DISABLE;                       // Auto-delayed conversion feature disabled
	SlowAdcHandle.Init.ContinuousConvMode    = DISABLE;                       // Continuous mode disabled to have only 1 conversion at each conversion trig
	SlowAdcHandle.Init.NbrOfConversion       = 1;                             // Parameter discarded because sequencer is disabled
	SlowAdcHandle.Init.DiscontinuousConvMode = DISABLE;                       // Parameter discarded because sequencer is disabled
	SlowAdcHandle.Init.NbrOfDiscConversion   = 1;                             // Parameter discarded because sequencer is disabled
	SlowAdcHandle.Init.ExternalTrigConv      = ADC_SOFTWARE_START;            // Software start to trig the 1st conversion manually, without external event
	SlowAdcHandle.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE; // Parameter discarded because software trigger chosen
	SlowAdcHandle.Init.DMAContinuousRequests = DISABLE;                       // DMA one-shot mode selected (not applied to this example)
	SlowAdcHandle.Init.Overrun               = ADC_OVR_DATA_OVERWRITTEN;      // DR register is overwritten with the last conversion result in case of overrun
	SlowAdcHandle.Init.OversamplingMode      = DISABLE;                       // No oversampling

	if (HAL_ADC_Init(&SlowAdcHandle) != HAL_OK)
	{
		// ADC initialization Error
		Error_Handler(DEFAULT_ERROR);
	}

	//##-2- Configure ADC regular channel ######################################
	sConfig.Channel      = ADC_CHANNEL_6;               // Sampled channel number PH1 default //TODO Change this, slow adc should not sample PH1
	sConfig.Rank         = ADC_REGULAR_RANK_1;          // Rank of sampled channel number ADCx_CHANNEL
	sConfig.SamplingTime = ADC_SAMPLETIME_6CYCLES_5;    // Sampling time (number of clock cycles unit)
	sConfig.SingleDiff   = ADC_SINGLE_ENDED;            // Single-ended input channel
	sConfig.OffsetNumber = ADC_OFFSET_NONE;             // No offset subtraction
	sConfig.Offset = 0;                                 // Parameter discarded because offset correction is disabled

	if (HAL_ADC_ConfigChannel(&SlowAdcHandle, &sConfig) != HAL_OK)
	{
		// Channel Configuration Error
		Error_Handler(DEFAULT_ERROR);
	}


	//##-3- Calibrate ADC then Start the conversion process ####################
	if (HAL_ADCEx_Calibration_Start(&SlowAdcHandle, ADC_SINGLE_ENDED) !=  HAL_OK)
	{
		// ADC Calibration Error
		Error_Handler(DEFAULT_ERROR);
	}

}


//**********************SCPI Command handling**********************//

const SCPI_error_desc scpi_user_errors[] = {
	{10, "Custom error"},
	{0} // terminator
};


void scpi_send_byte_impl(uint8_t b)
{
	//If com. from USB answer over USB
	if(READ_BIT(RadIIS_HW.Flags,USB_RX_AVAIL))
	{
		Send_CharBuffer_USB(&b,1);
	}
	//If com. from ESP answer to ESP
	if(READ_BIT(RadIIS_HW.Flags,ESP_RX_AVAIL))
	{
		Send_CharBuffer_ESP(&b,1);
	}
}


const char *scpi_user_IDN(void)
{
	//TODO Add defines for automatic revision and serial number
	char buffer[200];
	int16_t n = sprintf(buffer,"RadIIS,GammaSpectrometer,%d,%#x\n", SERIAL_NUMBER, FW_VERSION);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return NULL; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
	return NULL;
}


/** Error callback */
void scpi_user_error(int16_t errno, const char * msg)
{
	//TODO!! Deal with large error messages?
	char buffer[200];
	int16_t n = sprintf(buffer,"### SCPI ERROR ADDED: %d, %s ###\n", errno, msg);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}


/** Service request impl */
void scpi_user_SRQ(void)
{
	// NOTE: Actual instrument should send SRQ event somehow
	char buffer[20];
	int16_t n = sprintf(buffer,"[Service Request]\n");
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

// ---- INSTRUMENT COMMANDS ----

void cmd_SET_BIAS_cb(const SCPI_argval_t *args)
{
	if ((args[0].INT<MIN_BIAS) || (args[0].INT > MAX_BIAS))
	{
		if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
		{
			char err_buffer[20];
			int16_t err_n = sprintf(err_buffer,"Bias to low/high!\n");
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
				return; //TODO Error Handling?
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
				Send_CharBuffer_USB((uint8_t *)err_buffer,err_n);
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
				Send_CharBuffer_ESP((uint8_t *)err_buffer,err_n);
		}
		return;
	}
	RadIIS_HW.BiasNom = args[0].INT;
	if(current_measurement.Active)
	{
		current_measurement.Bias = PCHANGE;
	}
	if (READ_BIT(RadIIS_HW.Flags,AUTOTCOMP))
	{
		SetBiasVoltTC(RadIIS_HW.BiasNom);
	}
	else
	{
		SetBiasVolt(RadIIS_HW.BiasNom);
	}
}

void cmd_GET_BIAS_cb(const SCPI_argval_t *args)
{
	char buffer[50];
	int16_t n = sprintf(buffer,"%lu,%lu\n",RadIIS_HW.Bias,RadIIS_HW.BiasNom);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);}

void cmd_GET_ATC_cb(const SCPI_argval_t *args)
{
	char buffer[10];
	int n;
	if(READ_BIT(RadIIS_HW.Flags, AUTOTCOMP))
	{
		n = sprintf(buffer,"ON\n");
	}
	else
	{
		n = sprintf(buffer,"OFF\n");
	}
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
	}

void cmd_SET_ATC_cb(const SCPI_argval_t *args)
{
	if(args[0].BOOL)
	{
		SET_BIT(RadIIS_HW.Flags, AUTOTCOMP);
	}
	else
	{
		CLEAR_BIT(RadIIS_HW.Flags, AUTOTCOMP);
	}
	if(current_measurement.Active)
	{
		current_measurement.ATC = PCHANGE;
	}
}

void cmd_GET_TEMP_cb(const SCPI_argval_t *args)
{
	char buffer[50];
	int16_t n = sprintf(buffer,"%lu\n",RadIIS_HW.Temperature);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
	}

	void cmd_GET_BAT_LEVEL_cb(const SCPI_argval_t *args)
	{
		char buffer[50];
		int16_t n = sprintf(buffer,"%lu\n",RadIIS_HW.BatLevel);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			return; //TODO Error Handling?
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
			Send_CharBuffer_USB((uint8_t *)buffer,n);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			Send_CharBuffer_ESP((uint8_t *)buffer,n);
		}

void cmd_SET_COMP_cb(const SCPI_argval_t *args)
{
	SetThreshold(args[0].INT);
	if(current_measurement.Active)
	{
		current_measurement.Thres = PCHANGE;
	}
}

void cmd_GET_COMP_cb(const SCPI_argval_t *args)
{
	char buffer[50];
	int16_t n = sprintf(buffer,"%d\n",RadIIS_HW.Threshold);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_SET_COMP_ST_cb(const SCPI_argval_t *args)
{
	if(args[0].BOOL)
	{
		NVIC_EnableIRQ(COMP_IRQn);
	}
	else
	{
		NVIC_DisableIRQ(COMP_IRQn);
	}

}

void cmd_GET_COMP_ST_cb(const SCPI_argval_t *args)
{
	char buffer[10];
	int n;
	//Crazy stuff from CMSIS 5.0.1 to check whether COMP_IRQn is enabled
	if((uint32_t)(((NVIC->ISER[(((uint32_t)(int32_t)COMP_IRQn) >> 5UL)] & (1UL << (((uint32_t)(int32_t)COMP_IRQn) & 0x1FUL))) != 0UL)))
	{
		n = sprintf(buffer,"ON\n");
	}
	else
	{
		n = sprintf(buffer,"OFF\n");
	}
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_SET_GATETIME_cb(const SCPI_argval_t *args)
{
	RadIIS_HW.GateTime = args[0].INT;
}

void cmd_GET_GATETIME_cb(const SCPI_argval_t *args)
{
	char buffer[50];
	int16_t n = sprintf(buffer,"%d\n",RadIIS_HW.GateTime);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_GET_RATE_cb(const SCPI_argval_t *args)
{
	char buffer[20];
	int16_t n = sprintf(buffer,"%lu\n",RadIIS_HW.Rate);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_GET_RATE_BACKG_cb(const SCPI_argval_t *args)
{
	char buffer[20];
	int16_t n = sprintf(buffer,"%lu\n",	RadIIS_HW.RateBGC);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_GET_EXRATE_cb(const SCPI_argval_t *args)
{
	char buffer[20];
	int16_t n = sprintf(buffer,"%lu\n",RadIIS_HW.ExRate);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_ACK_cb(const SCPI_argval_t *args)
{
	char buffer[5];
	int16_t n = sprintf(buffer,"ACK\n");
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_MEAS_START_cb(const SCPI_argval_t *args)
{
	if(READ_BIT(RadIIS_HW.Flags, MEAS_RUNNING))
	{
		if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
		{
			char err_buffer[20];
			int16_t err_n = sprintf(err_buffer,"Already running...\n");
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
				return; //TODO Error Handling?
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
				Send_CharBuffer_USB((uint8_t *)err_buffer,err_n);
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
				Send_CharBuffer_ESP((uint8_t *)err_buffer,err_n);
		}
		return;
	}

	if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
	{
		char buffer[10];
		int16_t n = sprintf(buffer,"START\n");
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			return; //TODO Error Handling?
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
			Send_CharBuffer_USB((uint8_t *)buffer,n);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			Send_CharBuffer_ESP((uint8_t *)buffer,n);
		}
	//Reset Measurement:
	for(int i = 0; i < RESOLUTION ; i++)
	{
		current_measurement.current_spectrum[i] = 0;
	}
	current_measurement.Tstart = HAL_GetTick();
	current_measurement.Tlive = 0;
	current_measurement.Count = 0;
	current_measurement.Bias = RadIIS_HW.BiasNom;
	current_measurement.ATC = READ_BIT(RadIIS_HW.Flags, AUTOTCOMP);
	current_measurement.Thres = RadIIS_HW.Threshold;
	current_measurement.Active = 1;

	current_measurement.Tstop = args[0].INT;
	current_measurement.MaxCount = args[1].INT;
	current_measurement.Channel = args[2].INT;

	SET_BIT(RadIIS_HW.Flags, MEAS_RUNNING);
	RadIIS_HW.Status = RUNNING;
}

void cmd_MEAS_STOP_cb(const SCPI_argval_t *args)
{
	if(!READ_BIT(RadIIS_HW.Flags, MEAS_RUNNING))
	{
		if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
		{
			char err_buffer[20];
			int16_t err_n = sprintf(err_buffer,"Not running...\n");
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
				return; //TODO Error Handling?
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
				Send_CharBuffer_USB((uint8_t *)err_buffer,err_n);
			if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
				Send_CharBuffer_ESP((uint8_t *)err_buffer,err_n);
		}
		return;
	}

	if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
	{
		char buffer[10];
		int16_t n = sprintf(buffer,"STOP\n");
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			return; //TODO Error Handling?
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
			Send_CharBuffer_USB((uint8_t *)buffer,n);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			Send_CharBuffer_ESP((uint8_t *)buffer,n);
	}
	current_measurement.Tstop = HAL_GetTick();
	current_measurement.Active = 0;
	CLEAR_BIT(RadIIS_HW.Flags, MEAS_RUNNING);
	RadIIS_HW.Status = IDLE;
}

void cmd_MEAS_TRANSM_cb(const SCPI_argval_t *args)
{
	char buffer[100];
	int16_t n = sprintf(buffer,"#Spectrum: Start:%lu, Stop:%lu TBusy:%lu\n#Bias:%d mV, Threshold:%d, ATC: %d\n#Channel:Counts\n",current_measurement.Tstart,current_measurement.Tstop,current_measurement.Tlive,current_measurement.Bias,current_measurement.Thres,current_measurement.ATC);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
	for(int i = 0; i < RESOLUTION ; i++)
	{
		n = sprintf(buffer,"%d,%lu\n",i,current_measurement.current_spectrum[i]);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			return; //TODO Error Handling?
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
			Send_CharBuffer_USB((uint8_t *)buffer,n);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			Send_CharBuffer_ESP((uint8_t *)buffer,n);
	}
}

void cmd_MEAS_STATE_cb(const SCPI_argval_t *args)
{
	char buffer[10];
	int n;
	if (current_measurement.Active)
	{
		n = sprintf(buffer,"ON\n");
	}
	else
	{
		n = sprintf(buffer,"OFF\n");
	}
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
	}

void cmd_MEAS_BACKG_TRANSM_cb(const SCPI_argval_t *args)
{
	char buffer[100];
	int16_t n = sprintf(buffer,"#Spectrum-BG: Start:%lu, Stop:%lu TBusy:%lu\n#Bias:%d mV, Threshold:%d, ATC: %d\n#Channel:Rate[mHz]\n",current_measurement.Tstart,current_measurement.Tstop,current_measurement.Tlive,current_measurement.Bias,current_measurement.Thres,current_measurement.ATC);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
	for(int i = 0; i < RESOLUTION ; i++)
	{
		n = sprintf(buffer,"%d,%lu\n",i,((current_measurement.current_spectrum[i]*1000)/current_measurement.Tlive)-((current_background.spectrum[i]*1000)/current_background.Tlive));
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			return; //TODO Error Handling?
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
			Send_CharBuffer_USB((uint8_t *)buffer,n);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			Send_CharBuffer_ESP((uint8_t *)buffer,n);
	}
}

void cmd_BACKG_TRANSM_cb(const SCPI_argval_t *args)
{
	char buffer[100];
	int16_t n = sprintf(buffer,"#Background:\n#Channel:Counts\n");
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);

	for(int i = 0; i < RESOLUTION ; i++)
	{
		n = sprintf(buffer,"%d,%lu\n",i,current_background.spectrum[i]);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			return; //TODO Error Handling?
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
			Send_CharBuffer_USB((uint8_t *)buffer,n);
		if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
			Send_CharBuffer_ESP((uint8_t *)buffer,n);
	}

}

void cmd_BACKG_SET_cb(const SCPI_argval_t *args)
{
	char buffer[100];
	int16_t n = sprintf(buffer,"cb DATA:BLOB <%d>\n", args[0].BLOB_LEN);
	Send_CharBuffer_USB((uint8_t *)buffer,n);
	//TODO implement and maybe response
	current_background.channel_pointer = 0;

}

void cmd_BACKG_SET_blob_cb(const uint8_t *bytes)
{
	char buffer[100];
	int16_t n = sprintf(buffer,"binary data: %d and %d\n", bytes[0],bytes[1]);
	Send_CharBuffer_USB((uint8_t *)buffer,n);
	current_background.spectrum[current_background.channel_pointer] = bytes[0]+(bytes[1]<<8);
	current_background.channel_pointer++;
}

void cmd_BACKG_INFO_SET_cb(const SCPI_argval_t *args)
{
	current_background.Tlive = args[0].INT;
	current_background.Date = args[1].INT;
	sprintf(current_background.Comment,"%s",args[2].STRING);
}

void cmd_BACKG_INFO_GET_cb(const SCPI_argval_t *args)
{
	char buffer[200];
	int16_t n = sprintf(buffer,"%d,%d,%s\n",current_background.Tlive,current_background.Date,current_background.Comment);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_ESP_FLASH_MODE_cb(const SCPI_argval_t *args)
{

	//Reset ESP
	HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_RESET);
	//Boot to FLASH-Mode
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOE,Pin_ESPboot,GPIO_PIN_RESET);
	HAL_Delay(100);
	//Disable RESET -> Start
	HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_SET);
	HAL_Delay(100);

	SET_BIT(RadIIS_HW.Flags,ESP_PROGGING);
	//Enable USB<->UART Bridge
	SET_BIT(RadIIS_HW.Flags,ESP_TRANSCOM);
}

void cmd_ESP_TRANSPARENT_MODE_cb(const SCPI_argval_t *args)
{
	//Enable USB<->UART Bridge
	SET_BIT(RadIIS_HW.Flags,ESP_TRANSCOM);
}

void cmd_ENTER_BOOTLOADER_cb(const SCPI_argval_t *args)
{
	char buffer[20];
	int n;
	n = sprintf(buffer,"Jump to BL now!\n");
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);

	WRITE_REG(BOOTLOADER_FLAG_REGISTER, BOOTLOADER_MAGIC_NUMBER);

	HAL_Delay(500);
	NVIC_SystemReset();
}

void cmd_GET_DEBUG_MODE_cb(const SCPI_argval_t *args)
{
	char buffer[10];
	int n;
	if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
	{
		n = sprintf(buffer,"ON\n");
	}
	else
	{
		n = sprintf(buffer,"OFF\n");
	}
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB) && READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		return; //TODO Error Handling?
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_USB))
		Send_CharBuffer_USB((uint8_t *)buffer,n);
	if(READ_BIT(RadIIS_HW.Flags,PROC_CMD_ESP))
		Send_CharBuffer_ESP((uint8_t *)buffer,n);
}

void cmd_SET_DEBUG_MODE_cb(const SCPI_argval_t *args)
{
	if(args[0].BOOL)
	{
		SET_BIT(RadIIS_HW.Flags,DEBUG_MODE);
	}
	else
	{
		CLEAR_BIT(RadIIS_HW.Flags, DEBUG_MODE);
	}
}

void cmd_ESP_RESET_cb(const SCPI_argval_t *args)
{
	//Reset ESP
	HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_RESET);
	//Set ESP to normal operation
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOE,Pin_ESPboot,GPIO_PIN_SET);
	HAL_Delay(100);
	//Start ESP
	HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_SET);
}

void cmd_ESP_RESET_FM_cb(const SCPI_argval_t *args)
{
	//Reset ESP
	HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_RESET);
	//Boot to FLASH-Mode
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOE,Pin_ESPboot,GPIO_PIN_RESET);
	HAL_Delay(100);
	//Disable RESET -> Start
	HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_SET);
}

// Command definition (mandatory commands are built-in)
const SCPI_command_t scpi_commands[] = {
	{
		.levels = {"SYStem", "BIAS"},
		.params = {SCPI_DT_INT},
		.callback = cmd_SET_BIAS_cb
	},
	{
		.levels = {"SYStem", "BIAS?"},
		.callback = cmd_GET_BIAS_cb
	},
	{
		.levels = {"SYStem","ATC?"},
		.callback = cmd_GET_ATC_cb
	},
	{
		.levels = {"SYStem","ATC"},
		.params = {SCPI_DT_BOOL},
		.callback = cmd_SET_ATC_cb
	},
	{
		.levels = {"SYStem","TEMPerature?"},
		.callback = cmd_GET_TEMP_cb
	},
	{
		.levels = {"SYStem","BATtery","LEVEL?"},
		.callback = cmd_GET_BAT_LEVEL_cb
	},
	{
		.levels = {"SYStem", "COMParator","THReshold"},
		.params = {SCPI_DT_INT},
		.callback = cmd_SET_COMP_cb
	},
	{
		.levels = {"SYStem", "COMParator","THReshold?"},
		.callback = cmd_GET_COMP_cb
	},
	{
		.levels = {"SYStem", "COMParator","STate"},
		.params = {SCPI_DT_BOOL},
		.callback = cmd_SET_COMP_ST_cb
	},
	{
		.levels = {"SYStem", "COMParator","STate?"},
		.callback = cmd_GET_COMP_ST_cb
	},
	{
		.levels = {"SYStem", "GATEtime"},
		.params = {SCPI_DT_INT},
		.callback = cmd_SET_GATETIME_cb
	},
	{
		.levels = {"SYStem", "GATEtime?"},
		.callback = cmd_GET_GATETIME_cb
	},
	{
		.levels = {"SYStem", "Rate?"},
		.callback = cmd_GET_RATE_cb
	},
	{
		.levels = {"SYStem", "EXRate?"},
		.callback = cmd_GET_EXRATE_cb
	},
	{
		.levels = {"SYStem", "BGRate?"},
		.callback = cmd_GET_RATE_BACKG_cb
	},
	{
		.levels = {"MEASurement", "START"},
		.params = {SCPI_DT_INT,SCPI_DT_INT,SCPI_DT_INT},
		.callback = cmd_MEAS_START_cb
	},
	{
		.levels = {"MEASurement", "STOP"},
		.callback = cmd_MEAS_STOP_cb
	},
	{
		.levels = {"MEASurement", "STate?"},
		.callback = cmd_MEAS_STATE_cb
	},
	{
		.levels = {"MEASurement", "GET?"},
		.callback = cmd_MEAS_TRANSM_cb
	},
	{
		.levels = {"BACKGround","DATA","SET"},
		.params = {SCPI_DT_BLOB},
		.callback = cmd_BACKG_SET_cb,
		.blob_chunk = 2,
		.blob_callback = cmd_BACKG_SET_blob_cb
	},
	{
		.levels = {"BACKGround","DATA","GET?"},
		.callback = cmd_BACKG_TRANSM_cb
	},
	{
		.levels = {"BACKGround", "INFO","SET"},
		.params = {SCPI_DT_INT,SCPI_DT_INT,SCPI_DT_STRING},
		.callback = cmd_BACKG_INFO_SET_cb
	},
	{
		.levels = {"BACKGround", "INFO","GET?"},
		.callback = cmd_BACKG_INFO_GET_cb
	},
	{
		.levels = {"SYStem","BOOTLoader"},
		.callback = cmd_ENTER_BOOTLOADER_cb
	},
	{
		.levels = {"SYStem","ESP","FLASHMode"},
		.callback = cmd_ESP_FLASH_MODE_cb
	},
	{
		.levels = {"SYStem","ESP","TRANSMode"},
		.callback = cmd_ESP_TRANSPARENT_MODE_cb
	},
	{
		.levels = {"SYStem", "ESP", "Reset"},
		.callback = cmd_ESP_RESET_cb
	},
	{
		.levels = {"SYStem", "ESP", "RTOFlashmode"},
		.callback = cmd_ESP_RESET_FM_cb
	},
	{
		.levels = {"SYStem", "DEBUGmode"},
		.params = {SCPI_DT_BOOL},
		.callback = cmd_SET_DEBUG_MODE_cb
	},
	{
		.levels = {"SYStem", "DEBUGmode?"},
		.callback = cmd_GET_DEBUG_MODE_cb
	},
	{
		.levels = {"SYStem", "ACKnowledge"},
		.callback = cmd_ACK_cb
	},
	{/*END*/}
};



//********************SCPI Command handling END********************//


/**
 * @brief Jump to internal system bootloader
 * @param None
 * @retval None
 */
/*
void JumpToBootloader(void)
{
	//Declare jump function:
	void (*SysMemBootJump)(void);
	//STM32 L476 has 0x1FFF0000 as System Memory base address:
	volatile uint32_t addr = 0x1FFF0000;
	//Stop the Clocks:
	HAL_RCC_DeInit();
	//Reset the SysTick:
	SysTick->CTRL = 0;
	SysTick->LOAD = 0;
	SysTick->VAL = 0;
	//Disable all interrupts:
	__disable_irq();
	//Remap the System Memory to 0x00000000:
	//__HAL_SYSCFG_REMAPMEMORY_SYSTEMFLASH();	//Call HAL macro to do this for you
	//Set jump adress to System Memory + 4:
	SysMemBootJump = (void (*)(void)) (*((uint32_t *)(addr + 4)));
	//Set main stack pointer, 0x200030d0 = default address?:
	__set_MSP(0x200030d0);
	//Perform jump:
	SysMemBootJump();
}
*/
/*
//Stolen from mbed :TARGET_DISCO_L476VG/cmsis_nvic.c
void NVIC_SetVector(IRQn_Type IRQn, uint32_t vector) {
	uint32_t *vectors = (uint32_t *)SCB->VTOR;
	uint32_t i;

	// Copy and switch to dynamic vectors if the first time called
	if (SCB->VTOR == NVIC_FLASH_VECTDACsOR_ADDRESS) {
		uint32_t *old_vectors = vectors;
		vectors = (uint32_t*)NVIC_RAM_VECTOR_ADDRESS;
		for (i=0; i<NVIC_NUM_VECTORS; i++) {
			vectors[i] = old_vectors[i];
		}
		SCB->VTOR = (uint32_t)NVIC_RAM_VECTOR_ADDRESS;
	}
	vectors[IRQn + NVIC_USER_IRQ_OFFSET] = vector;
}

uint32_t NVIC_GetVector(IRQn_Type IRQn) {
	uint32_t *vectors = (uint32_t*)SCB->VTOR;
	return vectors[IRQn + NVIC_USER_IRQ_OFFSET];
}
*/

//****************************** ISRs *****************************//

/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(RadIIS_HW.Status == STANDBY)
	{
		NVIC_SystemReset();
	}
	RadIIS_HW.ButtonTs = HAL_GetTick();
	/*
if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
{
	char buffer[20];
	uint16_t n = sprintf(buffer,"Button pressed!\n");
	Send_CharBuffer_USB((uint8_t *)buffer,n);
}
	//TODO Button etc handling by ESP!!

  if (GPIO_Pin == Pin_Button)
  {

		if (RadIIS_HW.Status == STANDBY)
		{
			Check_Wakeup();
		}
		else
		{
			Check_Sleep();
		}
	}
	*/
}

/**
  * @brief  Comparator interrupt callback.
  * @param  hcomp: COMP handle
  * @retval None
  */
void HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp)
{
	__disable_irq();
	if(hcomp->Instance==COMP1)
	{
		int32_t energy;
		delayUS(1); //Let RC filter settle
		//TODO: That needs 30us->Need to be MUCH faster!
		energy = ReadFastADC(Pin_PH1);
		HAL_GPIO_WritePin(GPIOC,Pin_ResetPH1,GPIO_PIN_SET);

		// Fill spectrum for BG corrected rate measurement
		temp_measurement.current_spectrum[energy/(4096/RESOLUTION)]++;

		// If a measurement is running fill spectrum
		if(READ_BIT(RadIIS_HW.Flags, MEAS_RUNNING))
		{
			if((current_measurement.Active)&&(current_measurement.Channel == 0))
			{
				current_measurement.current_spectrum[energy/(4096/RESOLUTION)]++;
				current_measurement.Count++;
			}
		}

		/*
		if(!READ_BIT(RadIIS_HW.Flags,ESP_TRANSCOM))
		{
			char buffer[10];
			uint16_t n = sprintf(buffer,"%ld\n",energy);
			Send_CharBuffer_ESP((uint8_t *)buffer,n);
			if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
			{
				Send_CharBuffer_USB((uint8_t *)buffer,n);
			}
		}
		*/


		RadIIS_HW.Count++;
		HAL_GPIO_WritePin(GPIOC,Pin_ResetPH1,GPIO_PIN_RESET);
	}
	else
	{
		int32_t energy;
		delayUS(1);  //Let RC filter settle
		energy = ReadFastADC(Pin_PH2);
		HAL_GPIO_WritePin(GPIOC,Pin_ResetPH2,GPIO_PIN_SET);

		if(READ_BIT(RadIIS_HW.Flags, MEAS_RUNNING))
		{
			if((current_measurement.Active)&&(current_measurement.Channel == 1))
			{
				current_measurement.current_spectrum[energy/(4096/RESOLUTION)]++;
				current_measurement.Count++;
			}
		}
		/*
		if(!READ_BIT(RadIIS_HW.Flags,ESP_TRANSCOM))
		{
			char buffer[10];
			uint16_t n = sprintf(buffer,"%ld\n",energy);
			Send_CharBuffer_ESP((uint8_t*)buffer,n);
			if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
				Send_CharBuffer_USB((uint8_t*)buffer,n);
		}
		*/
		RadIIS_HW.ExCount++;
		HAL_GPIO_WritePin(GPIOC,Pin_ResetPH2,GPIO_PIN_RESET);

	}
	__enable_irq();
	//delayUS(6); // TODO Workaround for re-triggering, a bit hacky
	WRITE_REG(EXTI->PR1,COMP_GET_EXTI_LINE(hcomp->Instance));
}
//**************************** ISRs END ***************************//


/**
 * @brief  This function is executed in case of error occurrence. Error codes defines in main.h.
 * @param  None
 * @retval None
 */
static void Error_Handler(uint8_t error_code)
{
	while (1)
	{
        int i;
		for (i = 0; i<error_code; i++)
		{
			HAL_GPIO_WritePin(GPIOC,Pin_LEDStat,GPIO_PIN_RESET);
			HAL_Delay(251);
			HAL_GPIO_WritePin(GPIOC,Pin_LEDStat,GPIO_PIN_SET);
			HAL_Delay(250);
		}
		HAL_GPIO_WritePin(GPIOC,Pin_LEDStat,GPIO_PIN_RESET);
		HAL_Delay(2000);
	}
}

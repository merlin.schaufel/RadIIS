/**
 ******************************************************************************
 * @file    RadIIS_STM/src/main.c
 * @author  Merlin Schaufel
 * @version V1.0.1
 * @date    09-January-2018
 * @brief   RadIIS main
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright(c) 2017 STMicroelectronics International N.V.
 * All rights reserved.</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

// Includes ------------------------------------------------------------------
#include "main.h"

// Private typedef -----------------------------------------------------------
// Private define ------------------------------------------------------------
// Private macro -------------------------------------------------------------
// Private variables ---------------------------------------------------------
USBD_HandleTypeDef USBD_Device;
extern PCD_HandleTypeDef hpcd;

// Private function prototypes -----------------------------------------------
static void SystemClock_Config(void);
static void Error_Handler(uint8_t error_code);

extern char global_command_USB[5000];
extern char global_command_ESP[5000];

extern volatile HWSystem RadIIS_HW;
extern volatile Measurement current_measurement;
extern volatile Measurement temp_measurement;
extern volatile Background current_background;
// Private functions ---------------------------------------------------------

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void) {

	// Enable clock
	__HAL_RCC_PWR_CLK_ENABLE();

   // Enable RTC back-up registers access
  HAL_PWR_EnableBkUpAccess();

	if((RCC->CSR & RCC_CSR_SFTRSTF) && (READ_REG(BOOTLOADER_FLAG_REGISTER) == BOOTLOADER_MAGIC_NUMBER))
	{
		// Reset bootloader flag
		WRITE_REG(BOOTLOADER_FLAG_REGISTER, 0);
		JumpToBootloader();
	}

	// Initialize the HAL Library
	HAL_Init();
		// Configure the system clock to 80 MHz
	SystemClock_Config();
	// Enable Power Clock
	__HAL_RCC_PWR_CLK_ENABLE();

	//Init global command buffer
	global_command_USB[0] = '\0';
	global_command_ESP[1] = '\0';

	// Init hardware functions and set start values
	Init_RadIIS();

  //TODO Move Init USB to some Init()
	//Initialize Command buffer:
  for(int32_t i=0;i<5000;i++)
	{
		global_command_USB[i]=0;
		global_command_ESP[i]=0;
	}

	// enable USB power on Pwrctrl CR2 register
	HAL_PWREx_EnableVddUSB();
	// Init Device Library
	USBD_Init(&USBD_Device, &VCP_Desc, 0);
	// Add Supported Class
	USBD_RegisterClass(&USBD_Device, USBD_CDC_CLASS);
	// Add CDC Interface Class
	USBD_CDC_RegisterInterface(&USBD_Device, &USBD_CDC_fops);
	// Start Device Process
	USBD_Start(&USBD_Device);

	SET_BIT(RadIIS_HW.Flags,DEBUG_MODE);
	RadIIS_HW.Status = IDLE;
	//Default: Enable LED
	HAL_GPIO_WritePin(GPIOC,Pin_LEDStat,GPIO_PIN_SET);

	//ms counter for different periods:

	uint32_t P100_counter = HAL_GetTick();
	uint32_t P1000_counter = P100_counter;
	uint32_t P10000_counter = P100_counter;
	uint32_t PVar_counter = P100_counter;

	// Run Application (Interrupt mode)
	while (1) {

		//Check for ESP serial programming (firmware update) mode:
		if(READ_BIT(RadIIS_HW.Flags,ESP_TRANSCOM))
		{

			uint32_t prog_timer = HAL_GetTick();
			bool  start = true;

			while((HAL_GetTick()-prog_timer < ESP_TRANSPARENT_TIMEOUT) || start)
			{
				if(READ_BIT(RadIIS_HW.Flags,ESP_TRANSFER))
				{
					start = false;
					prog_timer = HAL_GetTick();
					CLEAR_BIT(RadIIS_HW.Flags,ESP_TRANSFER);
				}
			}

			if(READ_BIT(RadIIS_HW.Flags,ESP_PROGGING))
			{
				//Reset ESP
				HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_RESET);
				//Set ESP to normal operation
				HAL_Delay(100);
				HAL_GPIO_WritePin(GPIOE,Pin_ESPboot,GPIO_PIN_SET);
				HAL_Delay(100);
				//Start ESP
				HAL_GPIO_WritePin(GPIOE,Pin_ESPpdown,GPIO_PIN_SET);
			}

			//Go back to normal operation mode
			CLEAR_BIT(RadIIS_HW.Flags,ESP_PROGGING);
			CLEAR_BIT(RadIIS_HW.Flags,ESP_TRANSCOM);

			if(READ_BIT(RadIIS_HW.Flags,DEBUG_MODE))
			{
				char buffer[50];
				int16_t n = sprintf(buffer,"ESP transparent mode done!\n");
				Send_CharBuffer_USB((uint8_t *)buffer,n);
			}
		}

		//TODO Later move to ESP32
		if (RadIIS_HW.ButtonTs)
		{
			if (RadIIS_HW.Status != STANDBY)
			{
				Check_Sleep();
			}
			RadIIS_HW.ButtonTs = 0;
		}

		// If new Command is available, process:
		if(READ_BIT(RadIIS_HW.Flags,USB_RX_AVAIL))
		{
			//Append '\n'
			strcat(global_command_USB,"\n");
			//Flag command as USB command
			SET_BIT(RadIIS_HW.Flags,PROC_CMD_USB);
			//TODO: Debug ECHO
			Send_CharBuffer_USB((uint8_t *)global_command_USB,strlen(global_command_USB));
			//Pass command to SCPI lib
			scpi_handle_string(global_command_USB);
			//Clear command source flag
			CLEAR_BIT(RadIIS_HW.Flags,PROC_CMD_USB);
			//Clear command
			global_command_USB[0] = '\0';
			//Clear command avail. flag
			CLEAR_BIT(RadIIS_HW.Flags,USB_RX_AVAIL);
		}
		if(READ_BIT(RadIIS_HW.Flags,ESP_RX_AVAIL))
		{
			//Append '\n'
			strcat(global_command_ESP,"\n");
			//Flag command as ESP command
			SET_BIT(RadIIS_HW.Flags,PROC_CMD_ESP);
			//Pass command to SCPI lib
			scpi_handle_string(global_command_ESP);
			//Clear command source flag
			CLEAR_BIT(RadIIS_HW.Flags,PROC_CMD_ESP);
			//Clear command
			global_command_ESP[0] = '\0';
			//Clear command avail. flag
			CLEAR_BIT(RadIIS_HW.Flags,ESP_RX_AVAIL);
		}

		if(READ_BIT(RadIIS_HW.Flags, MEAS_RUNNING))
		{
			if((HAL_GetTick()-current_measurement.Tstart >= current_measurement.Tstop)||(current_measurement.Count>=current_measurement.MaxCount))
			{
				CLEAR_BIT(RadIIS_HW.Flags, MEAS_RUNNING);
				RadIIS_HW.Status = IDLE;
				current_measurement.Active = 0;
				current_measurement.Tstop = HAL_GetTick();
				//char buffer[50];
				//int16_t n = sprintf(buffer,"Measurement finished!\n");
				//Send_CharBuffer_USB((uint8_t *)buffer,n);
				//SET_BIT(RadIIS_HW.Flags, MEAS_FINISHED);
			}
		}

		if((HAL_GetTick()-P100_counter) >= 100)
		{
			//Process with 100ms period:
			P100_counter = HAL_GetTick();
			if(RadIIS_HW.Status == RUNNING)
			{
				HAL_GPIO_TogglePin(GPIOC,Pin_LEDStat);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOC, Pin_LEDStat, GPIO_PIN_SET);
			}
		}

		if((HAL_GetTick()-P1000_counter) >= 1000)
		{
			//Process with 1s period:
			P1000_counter = HAL_GetTick();
			RadIIS_HW.Temperature = getSiPMTemp();
			RadIIS_HW.BatLevel = getBatLevel();

			if(READ_BIT(RadIIS_HW.Flags, AUTOTCOMP))
			{
				SetBiasVoltTC(RadIIS_HW.BiasNom);
			}

		}

		if((HAL_GetTick()-P10000_counter) >= 10000)
		{
			//Process with 10s period:
			P10000_counter = HAL_GetTick();
		}

		if((HAL_GetTick()-PVar_counter) >= RadIIS_HW.GateTime)
		{
			//Variable counter period:



			RadIIS_HW.Rate = (RadIIS_HW.Count*1000)/RadIIS_HW.GateTime;
			RadIIS_HW.ExRate = (RadIIS_HW.ExCount*1000)/RadIIS_HW.GateTime;
			RadIIS_HW.RateBGC = 0;

			for(int i = 0; i < RESOLUTION ; i++)
			{
				// Calculate background corrected rate for all channels:
				RadIIS_HW.RateBGC += ((temp_measurement.current_spectrum[i]*1000)/RadIIS_HW.GateTime)-((current_background.spectrum[i]*1000)/current_background.Tlive);

				// Reset the temporal measurement:
				temp_measurement.current_spectrum[i] = 0;
			}


			RadIIS_HW.Count = 0;
			RadIIS_HW.ExCount = 0;
			PVar_counter = HAL_GetTick();

			// Reset temp_measurement?:
			temp_measurement.Tstart = PVar_counter;
			temp_measurement.Bias = RadIIS_HW.BiasNom;
			temp_measurement.ATC = READ_BIT(RadIIS_HW.Flags, AUTOTCOMP);
			temp_measurement.Thres = RadIIS_HW.Threshold;
			temp_measurement.Active = 1;
		}


	}

}

/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *
 *         If define USB_USE_LSE_MSI_CLOCK enabled:
 *            System Clock source            = PLL (MSI)
 *            SYSCLK(Hz)                     = 80000000
 *            HCLK(Hz)                       = 80000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 1
 *            APB2 Prescaler                 = 2
 *            MSI Frequency(Hz)              = 4800000
 *            LSE Frequency(Hz)              = 32768
 *            PLL_M                          = 6
 *            PLL_N                          = 40
 *            PLL_P                          = 7
 *            PLL_Q                          = 4
 *            PLL_R                          = 4
 *            Flash Latency(WS)              = 4
 *
 *         If define USB_USE_HSE_CLOCK enabled:
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 80000000
 *            HCLK(Hz)                       = 80000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 1
 *            APB2 Prescaler                 = 2
 *            HSE Frequency(Hz)              = 8000000
 *            PLL_M                          = 1
 *            PLL_N                          = 20
 *            PLL_P                          = 7
 *            PLL_Q                          = 4
 *            PLL_R                          = 2
 *            Flash Latency(WS)              = 4
 *
 * @param  None
 * @retval None
 */
static void SystemClock_Config(void) {
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

#if defined (USB_USE_LSE_MSI_CLOCK)

	// Enable the LSE Oscilator
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_OFF;
	HAL_RCC_OscConfig(&RCC_OscInitStruct);

	// Enable the CSS interrupt in case LSE signal is corrupted or not present
	HAL_RCCEx_DisableLSECSS();

	// Enable MSI Oscillator and activate PLL with MSI as source
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
	RCC_OscInitStruct.MSIState = RCC_MSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
	RCC_OscInitStruct.PLL.PLLM = 6;
	RCC_OscInitStruct.PLL.PLLN = 40;
	RCC_OscInitStruct.PLL.PLLP = 7;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	RCC_OscInitStruct.PLL.PLLR = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	// Enable MSI Auto-calibration through LSE
	HAL_RCCEx_EnableMSIPLLMode();

	// Select MSI output as USB clock source
	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USB;
	PeriphClkInitStruct.UsbClockSelection = RCC_USBCLKSOURCE_MSI;
	HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);

	// Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
	{
		Error_Handler();
	}

#elif defined (USB_USE_HSE_CLOCK)

	// Enable HSE Oscillator and activate PLL with HSE as source
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 20;
	RCC_OscInitStruct.PLL.PLLR = 2;
	RCC_OscInitStruct.PLL.PLLP = 7;
	RCC_OscInitStruct.PLL.PLLQ = 4;

	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler(DEFAULT_ERROR);
	}

	// Select PLLSAI output as USB clock source
	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USB;
	PeriphClkInitStruct.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
	PeriphClkInitStruct.PLLSAI1.PLLSAI1N = 24;
	PeriphClkInitStruct.PLLSAI1.PLLSAI1Q = 4;
	PeriphClkInitStruct.PLLSAI1.PLLSAI1P = 1;
	PeriphClkInitStruct.PLLSAI1.PLLSAI1M = 1;
	PeriphClkInitStruct.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
	PeriphClkInitStruct.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
		Error_Handler(DEFAULT_ERROR);
	}

	// Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
		Error_Handler(DEFAULT_ERROR);
	}

#endif // USB_USE_LSE_MSI_CLOCK
}

/**
 * @brief  This function is executed in case of error occurrence. Error codes defines in main.h.
 * @param  None
 * @retval None
 */
static void Error_Handler(uint8_t error_code) //TODO this assumes a succesful GPIO init...
{
	while (1)
	{
        int i;
		for (i = 0; i<error_code; i++)
		{
			HAL_GPIO_WritePin(GPIOC,Pin_LEDStat,GPIO_PIN_RESET);
			HAL_Delay(252);
			HAL_GPIO_WritePin(GPIOC,Pin_LEDStat,GPIO_PIN_SET);
			HAL_Delay(250);
		}
		HAL_GPIO_WritePin(GPIOC,Pin_LEDStat,GPIO_PIN_RESET);
		HAL_Delay(2000);
	}
}


#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	// Infinite loop
	while (1)
	{
	}
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

import numpy as np
import time
import radiis
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.signal import savgol_filter

import signal
import sys


my_radiis = RadIIS.RadIIS_Device()
time.sleep(0.1)
my_radiis.set_bias(26200)
time.sleep(0.1)
my_radiis.set_threshold(150)


def signal_handler(sig, frame):
    global my_radiis
    my_radiis.serdev.close()
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

"""
time.sleep(0.1)
my_radiis.serdev.write(b'*CLS\n')
time.sleep(0.1)
my_radiis.serdev.write(b'*CLS\n')
time.sleep(0.1)
my_radiis.serdev.write(b'*CLS\n')
time.sleep(0.1)
my_radiis.check_connection()
time.sleep(0.1)
my_radiis.set_bias(25700)
time.sleep(0.1)
my_radiis.set_threshold(85)
"""
my_radiis.set_debug("OFF")

my_radiis.start_measurement(99999999, 9999999999, 0)

global_rate = np.array([])
ys = []

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)
# This function is called periodically from FuncAnimation


def animate_spectrum(i):

    result = my_radiis.get_measurement()
    time.sleep(0.1)

    # Draw x and y lists
    ax1.clear()
    ax1.plot(np.arange(1, result.size), result[1:], "o")
    ax1.plot(np.arange(1, result.size), savgol_filter(result[1:], 51, 3), "-")

    # Format plot
    # plt.title('TMP102 Temperature over Time')
    # plt.ylabel('Temperature (deg C)')


def animate_rate(i):

    global global_rate
    global_rate = np.append(global_rate, my_radiis.get_rate())
    time.sleep(0.1)

    result = my_radiis.get_measurement()
    time.sleep(0.1)

    # Draw x and y lists
    ax1.clear()
    ax1.plot(np.arange(1, result.size), savgol_filter(result[1:], 51, 3), "-")

    mean = np.mean(global_rate)
    std = np.std(global_rate)

    # Draw x and y lists
    ax2.clear()
    ax2.plot(np.arange(0, global_rate.size), global_rate, ".-")

    ax2.axhline(y=mean)
    ax2.axhspan(mean - std, mean + std, facecolor="0.5", alpha=0.5)
    ax2.axhspan(
        mean - std / np.sqrt(global_rate.size),
        mean + std / np.sqrt(global_rate.size),
        facecolor="red",
        alpha=0.5,
    )

    # Format plot
    ax1.set_title("RadIIS live data!")
    ax1.set_ylabel("Events")
    ax2.set_ylabel("Eventrate [Hz]")
    ax1.set_xlabel("Energy [a.u.]")
    ax2.set_xlabel("Time [s]")


# Set up plot to call animate() function periodically
# ani1 = animation.FuncAnimation(fig, animate_spectrum, interval=10500)
ani2 = animation.FuncAnimation(fig, animate_rate, interval=1000)

plt.show()


"""
while(my_radiis.is_running):

    result = my_radiis.get_measurement()
    global_rate = np.append(global_rate,my_radiis.get_rate())

    plt.subplot(2, 1, 1)
    plt.plot(np.arange(1,result.size),result[1:], 'o-')
    plt.title('Live RadIIS data!')
    plt.ylabel('Events')

    plt.subplot(2, 1, 2)
    plt.plot(np.arange(0,global_rate.size),global_rate, '.-')
    plt.xlabel('time (s)')
    plt.ylabel('Events/s')
    plt.pause(1.000)

plt.show()
"""

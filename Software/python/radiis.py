import serial
import numpy as np
import time


class RadIIS_System:
    def __init__(self):
        self.ATC = 0
        self.threshold = 0
        self.bias = 0
        self.debug = 0


class RadIIS_Device:
    def __init__(
        self, dev="/dev/serial/by-id/usb-RadIIS_RadIIS_Gamma_spectrometer_ReV.1.1_FFFFFFFEFFFF-if00"
    ):
        self.serdev = serial.Serial(dev, 115200, timeout=1)
        self.serdev.write(b"*CLS\n")
        time.sleep(0.05)
        self.serdev.write(b"*CLS\n")
        time.sleep(0.05)
        self.read_serial()
        self.serdev.write(b"SYS:COMP:ST OFF\n")
        time.sleep(0.05)
        self.serdev.write(b"*IDN?\n")
        time.sleep(0.05)
        response = self.read_serial().split(",")
        print(response)

        if response[0] == "RadIIS":
            print("Connected to: " + " ".join(response))
            self.status = "connected"
            self.revision = response[3]
        else:
            self.status = "disconnected"
            self.revision = "disconnected"

        self.system = RadIIS_System()

        # ToDo proper error handling (try)
        self.set_bias(25700)
        self.set_ATC("ON")
        self.set_threshold(100)
        self.serdev.write(b"SYS:COMP:ST ON\n")
        time.sleep(0.05)

    def read_serial(self):
        response = self.serdev.read_all().decode("utf-8")[1:]
        return response

    def check_connection(self):
        if self.status == "disconnected":
            self.serdev.write(b"*IDN?\n")
            time.sleep(0.05)
            response = self.read_serial().split(",")
            print(response)

            if response[0] == "RadIIS":
                print("Connected to: " + " ".join(response))
                self.status = "connected"
                self.revision = response[3]
                return True
            else:
                self.status = "disconnected"
                self.revision = "disconnected"
                return False
        else:
            self.serdev.write(b"SYS:ACK?\n")
            time.sleep(0.05)
            response = self.read_serial()
            print(response)
            if resonse == "":
                self.status = "disconnected"
                return False
            if response == "ACK":
                return True
            # ToDo handle case with errorous message

    def set_bias(self, bias):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:BIAS {}\n".format(bias), "utf-8"))
            time.sleep(0.05)
            self.system.bias = self.get_bias()
            return self.system.bias
        else:
            return [0, 0]
        # ToDo diconnect/error handling

    def get_bias(self):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:BIAS?\n", "utf-8"))
            time.sleep(0.05)
            response = self.read_serial().split(",")
            print(response)
            self.system.bias = [float(i) for i in response]
            return self.system.bias
        else:
            return [0, 0]
        # ToDo diconnect/error handling

    def set_ATC(self, atc):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:ATC {}\n".format(atc), "utf-8"))
            time.sleep(0.05)
            self.system.ATC = self.get_ATC()
            return self.system.ATC
        else:
            return ""
        # ToDo diconnect/error handling / compare set/get values etc...

    def get_ATC(self):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:ATC?\n", "utf-8"))
            time.sleep(0.05)
            response = self.read_serial()
            print(response)
            self.system.ATC = response
            return self.system.ATC
        else:
            return ""
        # ToDo diconnect/error handling

    def set_debug(self, debug):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:DEBUG {}\n".format(debug), "utf-8"))
            time.sleep(0.05)
            self.system.debug = self.get_debug()
            return self.system.debug
        else:
            return ""
        # ToDo diconnect/error handling / compare set/get values etc...

    def get_debug(self):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:DEBUG?\n", "utf-8"))
            time.sleep(0.05)
            response = self.read_serial()
            print(response)
            self.system.debug = response
            return self.system.debug
        else:
            return ""
        # ToDo diconnect/error handling

    def set_threshold(self, thr):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:COMP:THR {}\n".format(thr), "utf-8"))
            time.sleep(0.05)
            self.system.threshold = self.get_threshold()
            return self.system.threshold
        else:
            return -1
        # ToDo diconnect/error handling

    def get_threshold(self):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:COMP:THR?\n", "utf-8"))
            time.sleep(0.05)
            response = self.read_serial()
            print(response)
            self.system.threshold = int(response)
            return self.system.threshold
        else:
            return -1
        # ToDo diconnect/error handling

    def get_rate(self):
        if self.status != "disconnected":
            self.serdev.write(bytes("SYS:RATE?\n", "utf-8"))
            time.sleep(0.05)
            response = self.read_serial()
            return int(response)
        else:
            return -1
        # ToDo diconnect/error handling

    def start_measurement(self, meastime, eventnum, channel):
        if self.status != "disconnected":
            self.serdev.write(
                bytes("MEAS:START {},{},{}\n".format(meastime, eventnum, channel), "utf-8")
            )
            time.sleep(0.05)
            return self.is_running()
        else:
            return -1
        # ToDo diconnect/error handling

    def stop_measurement(self):
        if self.status != "disconnected":
            self.serdev.write(bytes("MEAS:STOP\n", "utf-8"))
            time.sleep(0.05)
            return self.is_running()
        else:
            return -1
        # ToDo diconnect/error handling

    def is_running(self):
        if self.status != "disconnected":
            self.serdev.write(bytes("MEAS:ST?\n", "utf-8"))
            time.sleep(0.05)
            response = self.read_serial()
            print(response)
            if response == "ON":
                return True
            if response == "OFF":
                return False
            else:
                return -1
        else:
            return -1
        # ToDo diconnect/error handling

    def get_measurement(self):
        if self.status != "disconnected":
            self.serdev.write(bytes("MEAS:GET?\n", "utf-8"))
            time.sleep(0.1)
            response = self.read_serial()
            response = np.array([str.split(",") for str in response.split("\n")[3:]], dtype=float)[
                :, 1
            ]
            return response
        else:
            return np.zeros(512)
        # ToDo diconnect/error handling

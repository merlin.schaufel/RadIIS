import os
import time

import serial
import matplotlib.pyplot as plt
import numpy as np

import streamlit as st

inps = []


class Device:
    def __init__(self, dev="/dev/ttyX", baud=115200, timeout=0.1):
        self.serial = serial.Serial(dev, baudrate=baud, timeout=timeout)

    def write(self, inp=""):
        self.serial.write(b"%s\n" % inp.encode())

    def read(self):
        return self.serial.read(size=100000).decode("utf-8")[1:]


def get_measurement(device):
    device.write("MEAS:GET?")
    time.sleep(0.01)
    outp = device.read()
    meas = {
        int(key.split(",")[0]): int(key.split(",")[1])
        for key in outp.split("#")[-1].split("Counts")[1].split("\n")[1:]
    }
    bins = np.array(list(meas.keys()))
    values = np.array(list(meas.values()))
    return bins, values


def plot_measurement(x, y):
    fig = plt.figure()
    plt.plot(x, y)
    st.write(fig)


def handle_history(inp, filename="commands.txt"):

    with open("commands.txt", "a") as f:
        line = inp + "\n"
        lines = f.readlines()
        st.write(lines)
        if len(lines) == 0:
            f.write(line)
        else:
            if not lines[-1] == line:
                f.write(line)

    delete_history = st.button("Delete History")
    if delete_history:
        os.remove("commands.txt")

    st.write("History:")
    if os.path.isfile("commands.txt"):
        with open("commands.txt", "r") as f:
            st.write(f.readlines())
    else:
        st.write("-- empty --")


st.title("RadIIS")
device = Device(dev="/dev/tty.usbmodemFFFFFFFEFFFF1")

refresh = st.button("refresh")

inp = st.text_input("Serial Command:", value="")
handle_history(inp)

device.write(inp)
time.sleep(0.01)
outp = device.read()
st.write("In:", inp, "Out:", outp)

if len(outp) > 0:
    # read and plot measurement
    x, y = get_measurement(device)
    plot_measurement(x, y)

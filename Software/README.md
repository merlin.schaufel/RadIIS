# RADIIS Software

## Requirements

| Name | Version | Download
| ------ | ------ | ----- |
| Python | 2.X or 3.X | |
| Node | > 6.5.0 | https://nodejs.org/en/ |

## Setup
Two terminal windows are needed to first start the server and secondly start the app.

### Start server:
This opens up a simple python based http server, which serves the data on `http://localhost:8000`

In python 2.X:
```sh
$ cd $RADIIS_REPO/Software
$ python -m SimpleHTTPServer 8000
```
In python 3.X:
```sh
$ python3 -m http.server 8000 --directory $RADIIS_REPO/Software
```

### Start App
This starts the `node` app.
Usually it is served on `http://localhost:3000`.
```sh
$ npm run dev --prefix $RADIIS_REPO/Software/app
```
Now open the local address (usually `http://localhost:3000`) in your browser.

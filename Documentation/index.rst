Usage
==================================

documentation of the (advanced) usage of RadIIS:

SCPI commands:

.. function:: SYStem:BIAS <INT>
    
    Set the SiPM bias, given in [mV]. Higher bias means a higher signal amplification. If a very low bias is set it might be impossible to digitize the signals. It is not possible to choose a bias that damages the system if the enclosure is intact.
    
    *Maximum = 29950 mV*
    *Minimum = 24304 mV*
    *Default = ?*
    
    Example: SYS:BIAS 26000 (Sets the bias to 26.00V).
    
    
.. function:: SYStem:BIAS? 
    
    *Returns:* SiPM bias in mV [INT]
    

.. function:: SYStem:ATC <BOOL> 
    
    Activates or deactivates the **A**utomatic **T**emperature **C**ompensation. If enabled the bias of the SiPM is automatically adjusted to achieve a constant gain during temperature variations. It is very recommended to acctivate the ATC during measurements.    

    *Default = ?*
    
    Example: SYS:ATC ON (Activates the ATC).
    
    
.. function:: SYStem:ATC?
    
    *Returns:* State of the ATC [BOOL]
    

.. function:: SYStem:TEMPerature?
    
    *Returns:* SiPM temperature in mC [INT]
    
    
.. function:: SYStem:COMPerator:THReshold <INT>
    
    Set the comperator threshold for the external and internal channel. The number is given in DAC counts. Higher numbers mean a higher signal level that is needed to trigger a detection of a pulse. 
    
    *Maximum = 4095*
    *Minimum = 0*
    *Default = 0*
    
    Example: SYS:COMP:THR 100 
    
.. image:: gnu.gif


.. toctree::
   :maxdepth: 2
   :caption: Usage:

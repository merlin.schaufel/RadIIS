EESchema Schematic File Version 4
LIBS:IO_PCB-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L IO_PCB-rescue:Conn_01x12 J1
U 1 1 59E898F3
P 1900 2250
F 0 "J1" H 1900 2850 50  0000 C CNN
F 1 "Conn_01x12" H 1900 1550 50  0000 C CNN
F 2 "w_conn_misc:HR30-8R-12SD" H 1900 2250 50  0001 C CNN
F 3 "" H 1900 2250 50  0001 C CNN
	1    1900 2250
	-1   0    0    1   
$EndComp
$Comp
L IO_PCB-rescue:Conn_01x12 J2
U 1 1 59E89970
P 4500 2250
F 0 "J2" H 4500 2850 50  0000 C CNN
F 1 "Conn_01x12" H 4500 1550 50  0000 C CNN
F 2 "Connector_FFC-FPC:Hirose_FH12-12S-0.5SH_1x12-1MP_P0.50mm_Horizontal" H 4500 2250 50  0001 C CNN
F 3 "" H 4500 2250 50  0001 C CNN
	1    4500 2250
	1    0    0    1   
$EndComp
Text Label 3800 2350 2    60   ~ 0
Ext_3.3Va
Text Label 3800 2750 2    60   ~ 0
Ext_-2Va
Text Label 3800 2650 2    60   ~ 0
Ext_Ain
Text Label 3800 2250 2    60   ~ 0
Ext_Bout
Text Label 3800 2550 2    60   ~ 0
Ext_AGND
Text Label 3800 2150 2    60   ~ 0
Ext_Bat
Text Label 3800 1650 2    60   ~ 0
Ext_DGND
Text Label 3800 2050 2    60   ~ 0
Ext_TX
Text Label 3800 1850 2    60   ~ 0
Ext_GPIO0
Text Label 3800 1950 2    60   ~ 0
Ext_RX
Text Label 3800 1750 2    60   ~ 0
Ext_Vin
Wire Wire Line
	4300 1650 3800 1650
Wire Wire Line
	3800 1750 4300 1750
Wire Wire Line
	4300 1850 3800 1850
Wire Wire Line
	3800 1950 4300 1950
Wire Wire Line
	4300 2050 3800 2050
Wire Wire Line
	3800 2150 4300 2150
Wire Wire Line
	4300 2250 3800 2250
Wire Wire Line
	3800 2350 4300 2350
Wire Wire Line
	4300 2450 3800 2450
Wire Wire Line
	3800 2550 4300 2550
Wire Wire Line
	4300 2650 3800 2650
Wire Wire Line
	3800 2750 4300 2750
Text Label 2300 1850 0    60   ~ 0
Ext_3.3Va
Text Label 2300 2150 0    60   ~ 0
Ext_-2Va
Text Label 2300 2650 0    60   ~ 0
Ext_AGND
Text Label 2300 2550 0    60   ~ 0
Ext_Ain
Text Label 2300 1650 0    60   ~ 0
Ext_Bout
Text Label 2300 1750 0    60   ~ 0
Ext_Bat
Text Label 2300 2750 0    60   ~ 0
Ext_DGND
Text Label 2300 1950 0    60   ~ 0
Ext_TX
Text Label 2300 2350 0    60   ~ 0
Ext_GPIO0
Text Label 2300 2050 0    60   ~ 0
Ext_RX
Text Label 2300 2450 0    60   ~ 0
Ext_Vin
Wire Wire Line
	2300 1650 2100 1650
Wire Wire Line
	2100 1750 2300 1750
Wire Wire Line
	2300 1850 2100 1850
Wire Wire Line
	2100 1950 2300 1950
Wire Wire Line
	2300 2050 2100 2050
Wire Wire Line
	2100 2150 2300 2150
Wire Wire Line
	2100 2350 2300 2350
Wire Wire Line
	2300 2450 2100 2450
Wire Wire Line
	2100 2550 2300 2550
Wire Wire Line
	2300 2650 2100 2650
Wire Wire Line
	2100 2750 2300 2750
Text Label 3800 2450 2    50   ~ 0
Ext_Misc
Text Label 2100 2250 0    50   ~ 0
Ext_Misc
$EndSCHEMATC

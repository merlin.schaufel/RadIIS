EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L IO_PCB-rescue:Conn_01x12 J1
U 1 1 59E898F3
P 1900 2250
F 0 "J1" H 1900 2850 50  0000 C CNN
F 1 "Conn_01x12" H 1900 1550 50  0000 C CNN
F 2 "IO_PCB:860-012-113R004" H 1900 2250 50  0001 C CNN
F 3 "" H 1900 2250 50  0001 C CNN
	1    1900 2250
	-1   0    0    1   
$EndComp
$Comp
L IO_PCB-rescue:Conn_01x12 J2
U 1 1 59E89970
P 5750 2200
F 0 "J2" H 5750 2800 50  0000 C CNN
F 1 "Conn_01x12" H 5750 1500 50  0000 C CNN
F 2 "IO_PCB:Hirose_FH12-12S-0.5SH_1x12-1MP_P0.50mm_Horizontal" H 5750 2200 50  0001 C CNN
F 3 "" H 5750 2200 50  0001 C CNN
	1    5750 2200
	1    0    0    1   
$EndComp
Text Label 5050 2300 2    60   ~ 0
Ext_3.3Va
Text Label 5050 2600 2    60   ~ 0
Ext_-2Va
Text Label 5050 1900 2    60   ~ 0
Ext_Ain
Text Label 5050 2700 2    60   ~ 0
Ext_Bout
Text Label 5050 2500 2    60   ~ 0
Ext_AGND
Text Label 5050 2100 2    60   ~ 0
Ext_Bat
Text Label 5050 2000 2    60   ~ 0
Ext_DGND
Text Label 5050 1700 2    60   ~ 0
Ext_TX
Text Label 5050 2400 2    60   ~ 0
Ext_GPIO0
Text Label 5050 1800 2    60   ~ 0
Ext_RX
Text Label 5050 1600 2    60   ~ 0
Ext_Vin
Wire Wire Line
	5550 1600 5050 1600
Wire Wire Line
	5050 1700 5550 1700
Wire Wire Line
	5550 1800 5050 1800
Wire Wire Line
	5050 1900 5550 1900
Wire Wire Line
	5550 2000 5050 2000
Wire Wire Line
	5050 2100 5550 2100
Wire Wire Line
	5550 2200 5050 2200
Wire Wire Line
	5050 2300 5550 2300
Wire Wire Line
	5550 2400 5050 2400
Wire Wire Line
	5050 2500 5550 2500
Wire Wire Line
	5550 2600 5050 2600
Wire Wire Line
	5050 2700 5550 2700
Text Label 2300 2550 0    60   ~ 0
Ext_-2Va
Text Label 2300 2250 0    60   ~ 0
Ext_AGND
Text Label 2300 2350 0    60   ~ 0
Ext_Ain
Text Label 2300 2650 0    60   ~ 0
Ext_Bout
Text Label 2300 1650 0    60   ~ 0
Ext_DGND
Text Label 2300 2150 0    60   ~ 0
Ext_TX
Text Label 2300 1750 0    60   ~ 0
Ext_GPIO0
Text Label 2300 2050 0    60   ~ 0
Ext_RX
Wire Wire Line
	2100 2050 2300 2050
Wire Wire Line
	2300 2150 2100 2150
Wire Wire Line
	2300 2350 2100 2350
Wire Wire Line
	2100 2550 2300 2550
Wire Wire Line
	2100 2650 2300 2650
Wire Wire Line
	2300 2750 2100 2750
Wire Wire Line
	2100 1750 2300 1750
Wire Wire Line
	2300 1950 2100 1950
Wire Wire Line
	2100 1650 2300 1650
Wire Wire Line
	2100 2250 2300 2250
Text Label 2300 1950 0    60   ~ 0
Ext_Vin
Text Label 2300 2750 0    60   ~ 0
Ext_Bat
Text Label 5050 2200 2    60   ~ 0
Ext_DGND
Text Label 2300 1850 0    60   ~ 0
Ext_DGND
Wire Wire Line
	2100 1850 2300 1850
Wire Wire Line
	2100 2450 2300 2450
Text Label 2300 2450 0    60   ~ 0
Ext_3.3Va
$EndSCHEMATC

EESchema Schematic File Version 4
LIBS:BUTTON_PCB-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RadIIS_Schematic:SW_Push_LED SW1
U 1 1 5BBD16D6
P 4500 1550
F 0 "SW1" H 4525 1935 50  0000 C CNN
F 1 "SW_Push_LED" H 4525 1844 50  0000 C CNN
F 2 "RadIIS_footprints_NEW:MBP16_RGB_PCB" H 4500 1850 50  0001 C CNN
F 3 "" H 4500 1850 50  0001 C CNN
	1    4500 1550
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5BBD1B3C
P 4100 1450
F 0 "TP1" V 4100 1700 50  0000 C CNN
F 1 "Pad" V 4050 1700 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 4300 1450 50  0001 C CNN
F 3 "~" H 4300 1450 50  0001 C CNN
	1    4100 1450
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5BBD1B8E
P 4100 1550
F 0 "TP2" V 4100 1800 50  0000 C CNN
F 1 "Pad" V 4050 1800 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 4300 1550 50  0001 C CNN
F 3 "~" H 4300 1550 50  0001 C CNN
	1    4100 1550
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5BBD1BF6
P 4100 1700
F 0 "TP3" V 4100 1950 50  0000 C CNN
F 1 "Pad" V 4050 1950 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 4300 1700 50  0001 C CNN
F 3 "~" H 4300 1700 50  0001 C CNN
	1    4100 1700
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5BBD1C14
P 4100 1850
F 0 "TP4" V 4100 2100 50  0000 C CNN
F 1 "Pad" V 4050 2100 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 4300 1850 50  0001 C CNN
F 3 "~" H 4300 1850 50  0001 C CNN
	1    4100 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4900 1700 4800 1700
Wire Wire Line
	4300 1450 4100 1450
Wire Wire Line
	4100 1550 4350 1550
Wire Wire Line
	4350 1700 4100 1700
Wire Wire Line
	4100 1850 4350 1850
Wire Wire Line
	4700 1450 4800 1450
Wire Wire Line
	4800 1450 4800 1700
Connection ~ 4800 1700
Wire Wire Line
	4800 1700 4750 1700
$Comp
L Connector:TestPoint TP5
U 1 1 5BBF65E5
P 4900 1700
F 0 "TP5" V 4900 1887 50  0000 L CNN
F 1 "Pad" V 4945 1888 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 5100 1700 50  0001 C CNN
F 3 "~" H 5100 1700 50  0001 C CNN
	1    4900 1700
	0    1    1    0   
$EndComp
$EndSCHEMATC

EESchema Schematic File Version 4
LIBS:RadIIS_Mainboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5500 5050 0    60   ~ 0
VOUT+
$Comp
L Device:C C11
U 1 1 572C8B68
P 3750 5200
F 0 "C11" H 3800 5300 50  0000 L CNN
F 1 "1.0µF" H 3800 5100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3788 5050 50  0001 C CNN
F 3 "" H 3750 5200 50  0000 C CNN
F 4 "1276-2931-1-ND" H 3750 5200 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3750 5200 50  0001 C CNN "Digikey Price"
	1    3750 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 572C8B69
P 5500 5300
F 0 "C14" H 5500 5400 50  0000 L CNN
F 1 "1.0µF" H 5500 5200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5538 5150 50  0001 C CNN
F 3 "" H 5500 5300 50  0000 C CNN
F 4 "1276-2931-1-ND" H 5500 5300 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5500 5300 50  0001 C CNN "Digikey Price"
	1    5500 5300
	1    0    0    -1  
$EndComp
Text HLabel 6300 5550 2    60   Input ~ 0
GNDA_Out
Wire Wire Line
	4250 5200 4400 5200
Wire Wire Line
	3750 5050 4400 5050
Wire Wire Line
	3750 5350 3750 5550
Wire Wire Line
	3750 4900 3750 5050
Wire Wire Line
	4800 5550 4800 5400
Text HLabel 6300 5050 2    60   Input ~ 0
A3V3_Out
Text HLabel 3750 5700 3    60   Input ~ 0
GND_In
Text HLabel 3700 4900 0    60   Input ~ 0
VBAT_In
Wire Wire Line
	3750 4900 3700 4900
Connection ~ 3750 5550
Connection ~ 4800 5550
Text HLabel 4250 5200 0    60   Input ~ 0
SHDN
$Comp
L Connector:TestPoint W18
U 1 1 58B570B5
P 6100 5050
F 0 "W18" H 6100 5320 50  0000 C CNN
F 1 "TEST_1P" H 6100 5250 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 6300 5050 50  0001 C CNN
F 3 "" H 6300 5050 50  0000 C CNN
F 4 "NA" H 6100 5050 50  0001 C CNN "Digikey Nr"
	1    6100 5050
	-1   0    0    1   
$EndComp
Connection ~ 6100 5050
Wire Wire Line
	3750 5550 3750 5700
Wire Wire Line
	4800 5550 3750 5550
Wire Wire Line
	6100 5050 6300 5050
$Comp
L Regulator_SwitchedCapacitor:LM27761 U7
U 1 1 5B7D044F
P 4850 1800
F 0 "U7" H 4850 2367 50  0000 C CNN
F 1 "LM27761" H 4850 2276 50  0000 C CNN
F 2 "Package_SON:WSON-8-1EP_2x2mm_P0.5mm_EP0.9x1.6mm" H 5000 1300 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm27761.pdf" H 7350 1400 50  0001 C CNN
F 4 "296-44379-1-ND" H 4850 1800 50  0001 C CNN "Digikey Nr"
F 5 "1.45" H 4850 1800 50  0001 C CNN "Digikey Price"
	1    4850 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5B7D09D4
P 3400 1950
F 0 "C9" H 3450 2050 50  0000 L CNN
F 1 "4.7µF" H 3400 1850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3438 1800 50  0001 C CNN
F 3 "" H 3400 1950 50  0000 C CNN
F 4 "1276-1065-1-ND" H 3400 1950 50  0001 C CNN "Digikey Nr"
F 5 "0.15" H 3400 1950 50  0001 C CNN "Digikey Price"
	1    3400 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5B7D0BD6
P 4300 1950
F 0 "C10" H 4185 1996 50  0000 R CNN
F 1 "1.0µF" H 4185 1905 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4338 1800 50  0001 C CNN
F 3 "" H 4300 1950 50  0000 C CNN
F 4 "1276-2931-1-ND" H 4300 1950 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 4300 1950 50  0001 C CNN "Digikey Price"
	1    4300 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 1800 4450 1800
Wire Wire Line
	4450 2100 4300 2100
Text HLabel 4400 1600 0    60   Input ~ 0
SHDN
Wire Wire Line
	4450 1600 4400 1600
Wire Wire Line
	3400 2450 3400 2100
Wire Wire Line
	4850 2300 4850 2450
Wire Wire Line
	4850 2450 4950 2450
Wire Wire Line
	4950 2300 4950 2450
Text HLabel 3100 2450 0    60   Input ~ 0
GND_In
Text HLabel 2600 1500 0    60   Input ~ 0
VBAT_In
Wire Wire Line
	3400 2450 3100 2450
Connection ~ 3400 2450
Connection ~ 3750 5050
$Comp
L Device:Ferrite_Bead FB1
U 1 1 5CF26E7F
P 2950 1500
F 0 "FB1" V 2676 1500 50  0000 C CNN
F 1 "2MHz, 250mA" V 2767 1500 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 2880 1500 50  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/beads_commercial_power_mpz1608_en.pdf" H 2950 1500 50  0001 C CNN
F 4 "445-8694-1-ND" H 2950 1500 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 2950 1500 50  0001 C CNN "Digikey Price"
	1    2950 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 1500 2800 1500
$Comp
L RadIIS_Mainboard:NCP167 U9
U 1 1 5D3968C4
P 4800 5150
F 0 "U9" H 4800 5492 50  0000 C CNN
F 1 "NCP167" H 4800 5401 50  0000 C CNN
F 2 "RadIIS_Mainboard:XDFN4" H 4750 5500 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NCP167-D.PDF" H 4800 5150 50  0001 C CNN
F 4 "WLCSP4 Footprint has different pinout" H 4800 5600 60  0001 C CNN "Comment"
	1    4800 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1500 3400 1500
Wire Wire Line
	3400 1800 3400 1500
Connection ~ 3400 1500
Wire Wire Line
	3400 2450 4850 2450
Connection ~ 4850 2450
Wire Wire Line
	3400 1500 4450 1500
$Comp
L Device:C C13
U 1 1 5B7D0B8C
P 6550 1950
F 0 "C13" H 6300 2000 50  0000 L CNN
F 1 "4.7µF" H 6250 1900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6588 1800 50  0001 C CNN
F 3 "" H 6550 1950 50  0000 C CNN
F 4 "1276-1065-1-ND" H 6550 1950 50  0001 C CNN "Digikey Nr"
F 5 "0.15" H 6550 1950 50  0001 C CNN "Digikey Price"
	1    6550 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5B7D562C
P 6050 2200
F 0 "R14" H 6200 2200 50  0000 C CNN
F 1 "470k 1%" V 5950 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5980 2200 50  0001 C CNN
F 3 "" H 6050 2200 50  0001 C CNN
F 4 "311-470KHRCT-ND " H 6050 2200 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 6050 2200 50  0001 C CNN "Digikey Price"
	1    6050 2200
	-1   0    0    1   
$EndComp
$Comp
L Device:R R13
U 1 1 5B7D62A0
P 6050 1700
F 0 "R13" H 6200 1700 50  0000 C CNN
F 1 "300k 1%" V 5950 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5980 1700 50  0001 C CNN
F 3 "" H 6050 1700 50  0000 C CNN
F 4 "311-300KHRCT-ND" H 6050 1700 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 6050 1700 50  0001 C CNN "Digikey Price"
	1    6050 1700
	-1   0    0    1   
$EndComp
Text HLabel 6950 1500 2    60   Input ~ 0
A-2V0_Out
Text HLabel 6950 2450 2    60   Input ~ 0
GNDA_Out
Connection ~ 4950 2450
Wire Wire Line
	4950 2450 5500 2450
Wire Wire Line
	5500 2450 6050 2450
Connection ~ 5500 2450
Wire Wire Line
	5250 1900 5500 1900
Wire Wire Line
	5500 2050 5500 1900
Wire Wire Line
	5500 2350 5500 2450
$Comp
L Device:C C12
U 1 1 5B7D0AFC
P 5500 2200
F 0 "C12" H 5250 2250 50  0000 L CNN
F 1 "4.7µF" H 5200 2150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5538 2050 50  0001 C CNN
F 3 "" H 5500 2200 50  0000 C CNN
F 4 "1276-1065-1-ND" H 5500 2200 50  0001 C CNN "Digikey Nr"
F 5 "0.15" H 5500 2200 50  0001 C CNN "Digikey Price"
	1    5500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 1700 5700 1700
Wire Wire Line
	5700 1700 5700 1950
Wire Wire Line
	5700 1950 6050 1950
Wire Wire Line
	6050 1950 6050 1850
Wire Wire Line
	5250 1500 6050 1500
Wire Wire Line
	6050 1950 6050 2050
Connection ~ 6050 1950
Wire Wire Line
	6050 2350 6050 2450
Connection ~ 6050 2450
Wire Wire Line
	6050 1550 6050 1500
Connection ~ 6050 1500
Wire Wire Line
	6050 1500 6550 1500
Wire Wire Line
	6050 2450 6550 2450
Wire Wire Line
	6550 1800 6550 1500
Connection ~ 6550 1500
Wire Wire Line
	6550 2100 6550 2450
Connection ~ 6550 2450
Wire Wire Line
	6550 2450 6950 2450
Wire Wire Line
	6550 1500 6950 1500
Wire Wire Line
	4800 5550 5500 5550
Wire Wire Line
	5500 5050 5500 5150
Wire Wire Line
	5200 5050 5500 5050
Connection ~ 5500 5050
Wire Wire Line
	5500 5050 6100 5050
Wire Wire Line
	5500 5450 5500 5550
Connection ~ 5500 5550
Wire Wire Line
	5500 5550 6300 5550
$Comp
L Connector:TestPoint TP8
U 1 1 5D6699FC
P 6100 5050
F 0 "TP8" H 6158 5168 50  0000 L CNN
F 1 "TestPoint" H 6158 5077 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6300 5050 50  0001 C CNN
F 3 "~" H 6300 5050 50  0001 C CNN
	1    6100 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP9
U 1 1 5D66ACEF
P 6550 1500
F 0 "TP9" H 6608 1618 50  0000 L CNN
F 1 "TestPoint" H 6608 1527 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6750 1500 50  0001 C CNN
F 3 "~" H 6750 1500 50  0001 C CNN
	1    6550 1500
	1    0    0    -1  
$EndComp
$EndSCHEMATC

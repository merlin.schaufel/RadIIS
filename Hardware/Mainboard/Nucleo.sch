EESchema Schematic File Version 4
LIBS:RadIIS_Mainboard-cache
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 10 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 14900 2600 2    60   Input ~ 0
PH1_In(PA0)*a
Text HLabel 14900 2700 2    60   Input ~ 0
Temp_In(PA2)_a
Text HLabel 14900 3000 2    60   Input ~ 0
BatSense_In(PA5)_a
Text HLabel 14900 6000 2    60   Input ~ 0
Reset_PH1(PC1)*
Text HLabel 14900 5200 2    60   Input ~ 0
ESP_RX(PB11)*
Text HLabel 14900 5300 2    60   Input ~ 0
ESP_TX(PB10)*
Text HLabel 1400 5500 0    60   Input ~ 0
ESP_PD(PD4)
Text HLabel 1400 5600 0    60   Input ~ 0
ESP_BOOT(PD1)
Text HLabel 14900 6400 2    60   Input ~ 0
ESP_RD(PD3)
Text HLabel 14900 2900 2    60   Input ~ 0
Bias_Out(PA4)_a
Text HLabel 14900 5100 2    60   Input ~ 0
SHDN_Out(PB9)
Text HLabel 14900 2800 2    60   Input ~ 0
Bias_In(PA3)_a
Text HLabel 5200 9900 2    60   Input ~ 0
3.3VOut
$Comp
L Device:Crystal Y1
U 1 1 57BEB2A7
P 1450 3950
F 0 "Y1" H 1450 4100 50  0000 C CNN
F 1 "ABM3-8.000MHZ-D2Y-T" V 1200 4150 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_Abracon_ABM3-2Pin_5.0x3.2mm" H 1450 3950 50  0001 C CNN
F 3 "https://abracon.com/Resonators/abm3.pdf" H 1450 3950 50  0001 C CNN
F 4 "535-10630-1-ND" H 1450 3950 50  0001 C CNN "Digikey Nr"
F 5 "0.62" H 1450 3950 50  0001 C CNN "Digikey Price"
	1    1450 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C21
U 1 1 57BEB342
P 1100 3750
F 0 "C21" V 1150 3850 50  0000 L CNN
F 1 "20pF" V 1050 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1138 3600 50  0001 C CNN
F 3 "" H 1100 3750 50  0000 C CNN
F 4 "1276-1187-1-ND" H 1100 3750 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 1100 3750 50  0001 C CNN "Digikey Price"
	1    1100 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C C22
U 1 1 57BEB38D
P 1100 4150
F 0 "C22" V 1050 4250 50  0000 L CNN
F 1 "20pF" V 1050 3900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1138 4000 50  0001 C CNN
F 3 "" H 1100 4150 50  0000 C CNN
F 4 "1276-1187-1-ND" H 1100 4150 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 1100 4150 50  0001 C CNN "Digikey Price"
	1    1100 4150
	0    1    1    0   
$EndComp
Text HLabel 750  3750 0    60   Input ~ 0
GND
Text HLabel 750  4150 0    60   Input ~ 0
GND
$Comp
L Device:C C25
U 1 1 57BEB5E3
P 1450 10000
F 0 "C25" H 1475 10100 50  0000 L CNN
F 1 "100nF" H 1475 9900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1488 9850 50  0001 C CNN
F 3 "" H 1450 10000 50  0000 C CNN
F 4 "1276-1000-1-ND " H 1450 10000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 1450 10000 50  0001 C CNN "Digikey Price"
	1    1450 10000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C26
U 1 1 57BEB615
P 1700 10000
F 0 "C26" H 1725 10100 50  0000 L CNN
F 1 "100nF" H 1725 9900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1738 9850 50  0001 C CNN
F 3 "" H 1700 10000 50  0000 C CNN
F 4 "1276-1000-1-ND " H 1700 10000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 1700 10000 50  0001 C CNN "Digikey Price"
	1    1700 10000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C27
U 1 1 57BEB640
P 1950 10000
F 0 "C27" H 1975 10100 50  0000 L CNN
F 1 "100nF" H 1975 9900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1988 9850 50  0001 C CNN
F 3 "" H 1950 10000 50  0000 C CNN
F 4 "1276-1000-1-ND " H 1950 10000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 1950 10000 50  0001 C CNN "Digikey Price"
	1    1950 10000
	1    0    0    -1  
$EndComp
Text HLabel 3150 9900 0    60   Input ~ 0
Bat_In+
Text HLabel 3150 10050 0    60   Input ~ 0
Bat_In-
Text HLabel 8450 8100 3    60   Input ~ 0
AGND
Text HLabel 8150 8100 3    60   Input ~ 0
GND
Text HLabel 8250 1600 1    60   Input ~ 0
3.3VOut
Text HLabel 9000 10300 2    60   Input ~ 0
GND
Text Label 8800 10500 0    60   ~ 0
NRST
Text Label 8800 10600 0    60   ~ 0
SWO
Text Label 8800 10400 0    60   ~ 0
TMS
Text Label 8800 10200 0    60   ~ 0
TCK
Text Label 1850 2400 1    60   ~ 0
NRST
Text Label 14700 4500 0    60   ~ 0
SWO
$Comp
L Device:C C24
U 1 1 57BEED21
P 1600 2500
F 0 "C24" H 1625 2600 50  0000 L CNN
F 1 "100nF" H 1600 2400 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1638 2350 50  0001 C CNN
F 3 "" H 1600 2500 50  0000 C CNN
F 4 "1276-1000-1-ND " H 1600 2500 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 1600 2500 50  0001 C CNN "Digikey Price"
	1    1600 2500
	0    1    1    0   
$EndComp
Text HLabel 1350 2500 0    60   Input ~ 0
GND
$Comp
L Device:R R25
U 1 1 57BEEE58
P 700 3000
F 0 "R25" V 780 3000 50  0000 C CNN
F 1 "10k" V 700 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 630 3000 50  0001 C CNN
F 3 "" H 700 3000 50  0000 C CNN
F 4 "311-10.0KHRCT-ND" H 700 3000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 700 3000 50  0001 C CNN "Digikey Price"
	1    700  3000
	1    0    0    -1  
$EndComp
Text HLabel 700  3200 3    60   Input ~ 0
GND
Text Label 14700 3900 0    60   ~ 0
TCK
Text Label 14700 3800 0    60   ~ 0
TMS
Text HLabel 1350 9700 1    60   Input ~ 0
3.3VOut
Text HLabel 1350 10400 3    60   Input ~ 0
GND
Text HLabel 4000 10600 3    60   Input ~ 0
GND
$Comp
L Device:C C30
U 1 1 57C6DB33
P 3500 10300
F 0 "C30" H 3525 10400 50  0000 L CNN
F 1 "100nF" H 3525 10200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3538 10150 50  0001 C CNN
F 3 "" H 3500 10300 50  0000 C CNN
F 4 "1276-1000-1-ND " H 3500 10300 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3500 10300 50  0001 C CNN "Digikey Price"
	1    3500 10300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C29
U 1 1 57C6DBA7
P 3300 10300
F 0 "C29" H 3325 10400 50  0000 L CNN
F 1 "1.0µF" H 3325 10200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3338 10150 50  0001 C CNN
F 3 "" H 3300 10300 50  0000 C CNN
F 4 "1276-2931-1-ND" H 3300 10300 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3300 10300 50  0001 C CNN "Digikey Price"
	1    3300 10300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C32
U 1 1 57C6DBE8
P 4800 10300
F 0 "C32" H 4825 10400 50  0000 L CNN
F 1 "1.0µF" H 4825 10200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4838 10150 50  0001 C CNN
F 3 "" H 4800 10300 50  0000 C CNN
F 4 "1276-2931-1-ND" H 4800 10300 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 4800 10300 50  0001 C CNN "Digikey Price"
	1    4800 10300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C33
U 1 1 57C6E375
P 5000 10300
F 0 "C33" H 5025 10400 50  0000 L CNN
F 1 "100nF" H 5025 10200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5038 10150 50  0001 C CNN
F 3 "" H 5000 10300 50  0000 C CNN
F 4 "1276-1000-1-ND " H 5000 10300 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5000 10300 50  0001 C CNN "Digikey Price"
	1    5000 10300
	1    0    0    -1  
$EndComp
Text HLabel 3150 10150 3    60   Input ~ 0
GND
$Comp
L Device:C C28
U 1 1 57C6EF82
P 2350 10000
F 0 "C28" H 2375 10100 50  0000 L CNN
F 1 "100nF" H 2375 9900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2388 9850 50  0001 C CNN
F 3 "" H 2350 10000 50  0000 C CNN
F 4 "1276-1000-1-ND " H 2350 10000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 2350 10000 50  0001 C CNN "Digikey Price"
	1    2350 10000
	1    0    0    -1  
$EndComp
Text HLabel 8450 1600 1    60   Input ~ 0
A3V3_IN
Text HLabel 2350 10350 3    60   Input ~ 0
AGND
Text HLabel 2350 9700 1    60   Input ~ 0
A3V3_IN
Text HLabel 14900 3600 2    60   Input ~ 0
USB_DM(PA11)=
Text HLabel 14900 3700 2    60   Input ~ 0
USB_DP(PA12)=
Text HLabel 14900 3500 2    60   Input ~ 0
USB_ID(PA10)
Text HLabel 14900 3400 2    60   Input ~ 0
USB_VBUS(PA9)
$Comp
L Device:Crystal Y2
U 1 1 58B26DC7
P 14900 7800
F 0 "Y2" H 14900 7532 50  0000 C CNN
F 1 "FX135A-327" H 14900 7623 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_G8-2Pin_3.2x1.5mm" H 14900 7800 50  0001 C CNN
F 3 "https://www.foxonline.com/pdfs/K135.pdf" H 14900 7800 50  0001 C CNN
F 4 " 631-1002-1-ND" H 14900 7800 50  0001 C CNN "Digikey Nr"
F 5 "0.8" H 14900 7800 50  0001 C CNN "Digikey Price"
	1    14900 7800
	-1   0    0    1   
$EndComp
$Comp
L Device:C C48
U 1 1 58B26DCD
P 14600 8050
F 0 "C48" H 14486 8096 50  0000 R CNN
F 1 "4.7pF" H 14486 8005 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 14638 7900 50  0001 C CNN
F 3 "" H 14600 8050 50  0000 C CNN
F 4 "311-1740-1-ND " H 14600 8050 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 14600 8050 50  0001 C CNN "Digikey Price"
	1    14600 8050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C47
U 1 1 58B26DD3
P 15250 8050
F 0 "C47" H 15365 8096 50  0000 L CNN
F 1 "4.7pF" H 15365 8005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 15288 7900 50  0001 C CNN
F 3 "" H 15250 8050 50  0000 C CNN
F 4 "311-1740-1-ND " H 15250 8050 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 15250 8050 50  0001 C CNN "Digikey Price"
	1    15250 8050
	1    0    0    -1  
$EndComp
Text HLabel 14600 8300 3    60   Input ~ 0
GND
Text HLabel 1350 2900 0    60   Input ~ 0
3.3VOut
Text Notes 12100 9550 0    60   ~ 0
Legend:\nNo mark 	= DC level (IO), low speed IO, IO only used if mode is not acq.\n*			= High speed IO, used even if in aq. mode\n=			= Differential IO, high speed (USB)\n*a			= High speed analog signal\n_a			= DC analog signal
Text HLabel 14900 4400 2    60   Input ~ 0
Trigger1_In(PB2)*a
Text HLabel 14900 2500 2    60   Input ~ 0
PH2_In(PA1)*a
Text HLabel 14900 4600 2    60   Input ~ 0
Trigger2_In(PB4)*a
Text HLabel 14900 6100 2    60   Input ~ 0
Reset_PH2(PC2)*
Text HLabel 15150 7100 2    60   Input ~ 0
Ex_TX(PC12)*
Text HLabel 1400 6100 0    60   Input ~ 0
Ex_RX(PD2)*
Text HLabel 14900 3200 2    60   Input ~ 0
Ex_IO(PA7)?
Text HLabel 14900 5700 2    60   Input ~ 0
Charger_STAT(PB15)
Text HLabel 14900 5500 2    60   Input ~ 0
EN_ExDCDC(PB13)
Text HLabel 1400 6400 0    60   Input ~ 0
LR_Reset(PD5)
Text Notes 14600 7100 0    60   ~ 0
U5Tx
Text Notes 1550 6500 0    60   ~ 0
US2Tx\nUS2Rx
Text Notes 1600 6100 0    60   ~ 0
U5Rx
$Comp
L Device:C C45
U 1 1 58B50BC9
P 950 10000
F 0 "C45" H 975 10100 50  0000 L CNN
F 1 "100nF" H 975 9900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 988 9850 50  0001 C CNN
F 3 "" H 950 10000 50  0000 C CNN
F 4 "1276-1000-1-ND " H 950 10000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 950 10000 50  0001 C CNN "Digikey Price"
	1    950  10000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C44
U 1 1 58B50C38
P 700 10000
F 0 "C44" H 725 10100 50  0000 L CNN
F 1 "100nF" H 725 9900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 738 9850 50  0001 C CNN
F 3 "" H 700 10000 50  0000 C CNN
F 4 "1276-1000-1-ND " H 700 10000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 700 10000 50  0001 C CNN "Digikey Price"
	1    700  10000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 3750 1450 3750
Wire Wire Line
	1450 3800 1450 3750
Connection ~ 1450 3750
Wire Wire Line
	1450 4150 1650 4150
Wire Wire Line
	1450 4150 1450 4100
Connection ~ 1450 4150
Wire Wire Line
	9000 10300 8700 10300
Wire Wire Line
	8800 10500 8700 10500
Wire Wire Line
	8700 10600 8800 10600
Wire Wire Line
	8800 10200 8700 10200
Wire Wire Line
	8800 10400 8700 10400
Wire Wire Line
	1750 2500 1850 2500
Wire Wire Line
	1850 2400 1850 2500
Connection ~ 1850 2500
Wire Wire Line
	1450 2500 1350 2500
Wire Wire Line
	14700 3900 14550 3900
Wire Wire Line
	14700 3800 14550 3800
Wire Wire Line
	14700 4500 14550 4500
Wire Wire Line
	1950 10300 1950 10150
Wire Wire Line
	700  10300 950  10300
Wire Wire Line
	1700 10150 1700 10300
Connection ~ 1700 10300
Wire Wire Line
	1450 10150 1450 10300
Connection ~ 1450 10300
Wire Wire Line
	1950 9800 1950 9850
Wire Wire Line
	700  9800 950  9800
Wire Wire Line
	1450 9850 1450 9800
Connection ~ 1450 9800
Wire Wire Line
	1700 9850 1700 9800
Connection ~ 1700 9800
Wire Wire Line
	4800 10150 4800 9900
Wire Wire Line
	3600 10050 3500 10050
Wire Wire Line
	3500 9900 3500 10050
Wire Wire Line
	3150 9900 3300 9900
Connection ~ 3500 10050
Wire Wire Line
	3300 9900 3300 10150
Connection ~ 3500 9900
Connection ~ 3300 9900
Wire Wire Line
	3300 10450 3500 10450
Connection ~ 4000 10450
Connection ~ 3500 10450
Connection ~ 4800 9900
Connection ~ 4800 10450
Wire Wire Line
	5000 10150 5000 9900
Connection ~ 5000 9900
Wire Wire Line
	3150 10150 3150 10050
Wire Wire Line
	2350 9850 2350 9700
Wire Wire Line
	2350 10350 2350 10150
Wire Wire Line
	8550 1800 8550 1900
Wire Wire Line
	7950 1800 8050 1800
Wire Wire Line
	7950 1800 7950 1900
Wire Wire Line
	8050 1900 8050 1800
Connection ~ 8050 1800
Wire Wire Line
	8150 1900 8150 1800
Connection ~ 8150 1800
Wire Wire Line
	8250 1600 8250 1800
Connection ~ 8250 1800
Wire Wire Line
	8350 1900 8350 1800
Connection ~ 8350 1800
Wire Wire Line
	8450 1600 8450 1900
Wire Wire Line
	8350 8000 8350 7900
Wire Wire Line
	7950 8000 8050 8000
Wire Wire Line
	7950 8000 7950 7900
Wire Wire Line
	8050 7900 8050 8000
Connection ~ 8050 8000
Wire Wire Line
	8150 7900 8150 8000
Connection ~ 8150 8000
Wire Wire Line
	8250 7900 8250 8000
Connection ~ 8250 8000
Wire Wire Line
	8450 8100 8450 7900
Wire Wire Line
	1650 4000 1650 4150
Wire Wire Line
	1650 3900 1650 3750
Wire Wire Line
	14900 3400 14550 3400
Wire Wire Line
	14550 3500 14900 3500
Wire Wire Line
	14900 3600 14550 3600
Wire Wire Line
	14550 3700 14900 3700
Wire Wire Line
	15150 7100 14550 7100
Wire Wire Line
	14900 4400 14550 4400
Wire Wire Line
	14550 4600 14900 4600
Wire Wire Line
	14900 5100 14550 5100
Wire Wire Line
	14900 2500 14550 2500
Wire Wire Line
	14550 2600 14900 2600
Wire Wire Line
	14900 2700 14550 2700
Wire Wire Line
	14550 2800 14900 2800
Wire Wire Line
	14900 2900 14550 2900
Wire Wire Line
	14550 3000 14900 3000
Wire Wire Line
	14900 3200 14550 3200
Wire Wire Line
	700  9800 700  9850
Wire Wire Line
	950  9850 950  9800
Connection ~ 950  9800
Wire Wire Line
	950  10150 950  10300
Wire Wire Line
	700  10150 700  10300
Connection ~ 950  10300
Wire Wire Line
	1350 10400 1350 10300
Connection ~ 1350 10300
Wire Wire Line
	1350 9700 1350 9800
Connection ~ 1350 9800
Text HLabel 1350 4400 0    60   Input ~ 0
LED_Trigger(PA8)*
Wire Wire Line
	14900 3100 14550 3100
Text HLabel 1350 3100 0    60   Input ~ 0
VREF_Out
Text HLabel 1350 3000 0    60   Input ~ 0
AGND
NoConn ~ 8700 10100
Wire Wire Line
	14900 5200 14550 5200
Wire Wire Line
	14550 5300 14900 5300
Wire Wire Line
	14550 5500 14900 5500
Wire Wire Line
	14550 5700 14900 5700
Wire Wire Line
	14900 6000 14550 6000
Wire Wire Line
	14550 6100 14900 6100
Text HLabel 1400 6000 0    60   Input ~ 0
LR_SCK(PD1)*
Text HLabel 1400 5900 0    60   Input ~ 0
LR_NSS(PD0)
Text HLabel 1400 6200 0    60   Input ~ 0
LR_MISO(PD3)*
Text HLabel 1400 6300 0    60   Input ~ 0
LR_MOSI(PD4)*
Text HLabel 1400 6500 0    60   Input ~ 0
LR_PG0(PD6)
Text HLabel 14900 4300 2    60   Input ~ 0
BLR_DS1_In(PB1)*a
Wire Wire Line
	14900 4300 14550 4300
Text HLabel 14900 3100 2    60   Input ~ 0
BLR_DS2_In(PA6)*a
$Comp
L Device:C C55
U 1 1 5B1FC359
P 1350 8350
F 0 "C55" H 1375 8450 50  0000 L CNN
F 1 "100nF" H 1375 8250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1388 8200 50  0001 C CNN
F 3 "" H 1350 8350 50  0000 C CNN
F 4 "1276-1000-1-ND " H 1350 8350 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 1350 8350 50  0001 C CNN "Digikey Price"
	1    1350 8350
	1    0    0    -1  
$EndComp
Text HLabel 1350 8600 3    60   Input ~ 0
AGND
Text HLabel 1350 8100 1    60   Input ~ 0
VREF_Out
Wire Wire Line
	1350 8100 1350 8200
Wire Wire Line
	1350 8500 1350 8600
Text Label 6300 10600 2    60   ~ 0
NRST
$Comp
L Switch:SW_Push SW1
U 1 1 5B1FF4E4
P 6550 10600
F 0 "SW1" H 6600 10700 50  0000 L CNN
F 1 "SW_Push" H 6550 10540 50  0000 C CNN
F 2 "RadIIS_Mainboard:SW_SPST_PTS810" H 6550 10800 50  0001 C CNN
F 3 "https://www.ckswitches.com/media/1476/pts810.pdf" H 6550 10800 50  0001 C CNN
F 4 "CKN10503CT-ND " H 6550 10600 50  0001 C CNN "Digikey Nr"
F 5 "0.27" H 6550 10600 50  0001 C CNN "Digikey Price"
	1    6550 10600
	1    0    0    -1  
$EndComp
Text HLabel 6850 10600 2    60   Input ~ 0
GND
Wire Wire Line
	6350 10600 6300 10600
Wire Wire Line
	6850 10600 6750 10600
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 5B20103B
P 3350 8750
F 0 "J2" H 3350 8850 50  0000 C CNN
F 1 "Conn_01x01" H 3350 8650 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x01_P1.27mm_Vertical" H 3350 8750 50  0001 C CNN
F 3 "" H 3350 8750 50  0001 C CNN
F 4 "NA" H 3350 8750 50  0001 C CNN "Digikey Nr"
	1    3350 8750
	1    0    0    -1  
$EndComp
Text HLabel 3050 8750 0    60   Input ~ 0
GND
Wire Wire Line
	3150 8750 3050 8750
Wire Wire Line
	1450 3750 1650 3750
Wire Wire Line
	1700 10300 1950 10300
Wire Wire Line
	1450 10300 1700 10300
Wire Wire Line
	1450 9800 1700 9800
Wire Wire Line
	1700 9800 1950 9800
Wire Wire Line
	3500 10050 3500 10150
Wire Wire Line
	3500 9900 3600 9900
Wire Wire Line
	3300 9900 3500 9900
Wire Wire Line
	4000 10450 4000 10600
Wire Wire Line
	4800 9900 5000 9900
Wire Wire Line
	4800 10450 5000 10450
Wire Wire Line
	5000 9900 5200 9900
Wire Wire Line
	8050 1800 8150 1800
Wire Wire Line
	8150 1800 8250 1800
Wire Wire Line
	8250 1800 8250 1900
Wire Wire Line
	8250 1800 8350 1800
Wire Wire Line
	8350 1800 8550 1800
Wire Wire Line
	8050 8000 8150 8000
Wire Wire Line
	8150 8000 8150 8100
Wire Wire Line
	8150 8000 8250 8000
Wire Wire Line
	8250 8000 8350 8000
Wire Wire Line
	950  9800 1350 9800
Wire Wire Line
	950  10300 1350 10300
Wire Wire Line
	1350 10300 1450 10300
Wire Wire Line
	1350 9800 1450 9800
Wire Wire Line
	4400 9900 4800 9900
NoConn ~ 1950 4300
NoConn ~ 1950 4200
Wire Wire Line
	14550 7400 14600 7400
Wire Wire Line
	14600 7400 14600 7800
Wire Wire Line
	14750 7800 14600 7800
Connection ~ 14600 7800
Wire Wire Line
	14600 7800 14600 7900
Wire Wire Line
	15050 7800 15250 7800
Wire Wire Line
	15250 7800 15250 7900
Wire Wire Line
	15250 7800 15250 7300
Wire Wire Line
	15250 7300 14550 7300
Connection ~ 15250 7800
Wire Wire Line
	14600 8200 14600 8300
Text HLabel 15250 8300 3    60   Input ~ 0
GND
Wire Wire Line
	15250 8200 15250 8300
Wire Wire Line
	1250 4150 1450 4150
Wire Wire Line
	700  2700 700  2850
Wire Wire Line
	700  2700 700  2450
Connection ~ 700  2700
Text HLabel 700  2450 1    60   Input ~ 0
STM_BOOT
Wire Wire Line
	700  3200 700  3150
Wire Wire Line
	1950 6500 1400 6500
Wire Wire Line
	1400 6400 1950 6400
Wire Wire Line
	1950 6300 1400 6300
Wire Wire Line
	1400 6200 1950 6200
Wire Wire Line
	1950 6100 1400 6100
Wire Wire Line
	1400 6000 1950 6000
Wire Wire Line
	1400 5900 1950 5900
Wire Wire Line
	1950 5600 1400 5600
Wire Wire Line
	1400 5500 1950 5500
Wire Wire Line
	1950 4400 1350 4400
Wire Wire Line
	1350 3100 1950 3100
Wire Wire Line
	1950 3000 1350 3000
Wire Wire Line
	1950 2900 1350 2900
Wire Wire Line
	700  2700 1950 2700
Wire Wire Line
	1850 2500 1950 2500
NoConn ~ 1950 6600
$Comp
L RadIIS_Mainboard:STM32L476VGTx U17
U 1 1 58B2B459
P 8250 4900
F 0 "U17" H 2150 7725 50  0000 L BNN
F 1 "STM32L476VGTx" H 14350 7725 50  0000 R BNN
F 2 "Package_QFP:LQFP-100_14x14mm_P0.5mm" H 14350 7675 50  0001 R TNN
F 3 "https://www.st.com/resource/en/datasheet/stm32l476vg.pdf" H 8250 4900 50  0001 C CNN
F 4 " 497-15873-ND " H 8250 4900 50  0001 C CNN "Digikey Nr"
F 5 "9.4" H 8250 4900 50  0001 C CNN "Digikey Price"
	1    8250 4900
	1    0    0    -1  
$EndComp
NoConn ~ 14550 3300
NoConn ~ 14550 4000
NoConn ~ 14550 4200
NoConn ~ 14550 4700
NoConn ~ 14550 4800
NoConn ~ 14550 4900
NoConn ~ 14550 5000
NoConn ~ 14550 5400
NoConn ~ 14550 5900
NoConn ~ 14550 6200
NoConn ~ 14550 6300
NoConn ~ 14550 6500
NoConn ~ 14550 6600
NoConn ~ 14550 6700
NoConn ~ 14550 6900
NoConn ~ 14550 7000
NoConn ~ 14550 7200
NoConn ~ 1950 6700
NoConn ~ 1950 6800
NoConn ~ 1950 6900
NoConn ~ 1950 7000
NoConn ~ 1950 7100
NoConn ~ 1950 7200
NoConn ~ 1950 7300
NoConn ~ 1950 7400
NoConn ~ 1950 5400
NoConn ~ 1950 5300
NoConn ~ 1950 5200
NoConn ~ 1950 5100
NoConn ~ 1950 4500
NoConn ~ 1950 4600
NoConn ~ 1950 4700
NoConn ~ 1950 4800
NoConn ~ 1950 4900
NoConn ~ 1950 5000
Wire Wire Line
	4000 10450 4800 10450
Wire Wire Line
	3500 10450 4000 10450
Wire Wire Line
	4000 10250 4000 10450
$Comp
L RadIIS_Mainboard:NCP167 U10
U 1 1 5D397894
P 4000 10000
F 0 "U10" H 4000 10342 50  0000 C CNN
F 1 "NCP167" H 4000 10251 50  0000 C CNN
F 2 "RadIIS_Mainboard:XDFN4" H 3950 10350 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NCP167-D.PDF" H 4000 10000 50  0001 C CNN
F 4 "WLCSP4 Footprint has different pinout" H 4000 10450 60  0001 C CNN "Comment"
	1    4000 10000
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  4150 950  4150
Wire Wire Line
	750  3750 950  3750
Wire Wire Line
	1650 3900 1950 3900
Wire Wire Line
	1650 4000 1950 4000
Text Notes 8000 10600 0    60   ~ 0
T_SWO
Text Notes 8000 10500 0    60   ~ 0
T_NRST
Text Notes 8000 10400 0    60   ~ 0
T_JTMS
Text Notes 8000 10300 0    60   ~ 0
GND
Text Notes 8000 10200 0    60   ~ 0
T_JTCK
Text Notes 8000 10100 0    60   ~ 0
AIN
$Comp
L Connector:TestPoint W22
U 1 1 58B56B42
P 5000 9900
F 0 "W22" H 5000 10170 50  0000 C CNN
F 1 "TEST_1P" H 5000 10100 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 5200 9900 50  0001 C CNN
F 3 "" H 5200 9900 50  0000 C CNN
F 4 "NA" H 5000 9900 50  0001 C CNN "Digikey Nr"
	1    5000 9900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 P1
U 1 1 57BEDD1C
P 8500 10300
F 0 "P1" H 8500 10650 50  0000 C CNN
F 1 "CONN_01X06" V 8600 10300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch1.27mm" H 8500 10300 50  0001 C CNN
F 3 "" H 8500 10300 50  0000 C CNN
F 4 "NA" H 8500 10300 50  0001 C CNN "Digikey Nr"
	1    8500 10300
	-1   0    0    -1  
$EndComp
Text HLabel 14900 5600 2    60   Input ~ 0
PG_ExDCDC(PB14)
Wire Wire Line
	14900 5600 14550 5600
Text HLabel 6100 9900 0    60   Input ~ 0
3.3VOut
$Comp
L Connector:TestPoint TP19
U 1 1 5D639B7B
P 6100 9900
F 0 "TP19" V 6054 10088 50  0000 L CNN
F 1 "TestPoint" V 6145 10088 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6300 9900 50  0001 C CNN
F 3 "~" H 6300 9900 50  0001 C CNN
	1    6100 9900
	0    1    1    0   
$EndComp
Wire Wire Line
	14900 6400 14550 6400
NoConn ~ 1950 5700
$Comp
L Connector:TestPoint TP20
U 1 1 5D6F7AF1
P 14650 6800
F 0 "TP20" V 14604 6988 50  0000 L CNN
F 1 "TestPoint" V 14695 6988 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 14850 6800 50  0001 C CNN
F 3 "~" H 14850 6800 50  0001 C CNN
	1    14650 6800
	0    1    1    0   
$EndComp
Wire Wire Line
	14650 6800 14550 6800
$Comp
L Connector:TestPoint TP21
U 1 1 5D7044CD
P 6100 10100
F 0 "TP21" V 6054 10288 50  0000 L CNN
F 1 "TestPoint" V 6145 10288 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6300 10100 50  0001 C CNN
F 3 "~" H 6300 10100 50  0001 C CNN
	1    6100 10100
	0    1    1    0   
$EndComp
Text HLabel 6100 10100 0    60   Input ~ 0
GND
$Comp
L Connector:TestPoint TP22
U 1 1 5D707344
P 6100 10250
F 0 "TP22" V 6054 10438 50  0000 L CNN
F 1 "TestPoint" V 6145 10438 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6300 10250 50  0001 C CNN
F 3 "~" H 6300 10250 50  0001 C CNN
	1    6100 10250
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 10250 6100 10100
$EndSCHEMATC

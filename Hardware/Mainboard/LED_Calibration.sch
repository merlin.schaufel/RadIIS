EESchema Schematic File Version 4
LIBS:RadIIS_Mainboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1500 1450 0    60   Input ~ 0
~LED_Trigger*
Text HLabel 1500 1950 0    60   Input ~ 0
GND
$Comp
L Device:R R38
U 1 1 58B31822
P 2150 1450
F 0 "R38" V 2230 1450 50  0000 C CNN
F 1 "1k" V 2150 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2080 1450 50  0001 C CNN
F 3 "" H 2150 1450 50  0000 C CNN
F 4 "311-1.00KHRCT-ND" H 2150 1450 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 2150 1450 50  0001 C CNN "Digikey Price"
	1    2150 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 1450 2000 1450
Wire Wire Line
	2300 1450 2400 1450
Wire Wire Line
	2400 1450 2400 1500
Wire Wire Line
	2400 1800 2400 1950
Wire Wire Line
	1500 1950 2400 1950
$Comp
L Device:LED D5
U 1 1 58B43FDD
P 2400 1650
F 0 "D5" H 2550 1550 50  0000 C CNN
F 1 "LED" H 2400 1550 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 2400 1650 50  0001 C CNN
F 3 "" H 2400 1650 50  0000 C CNN
F 4 "492-1949-1-ND" H 2400 1650 50  0001 C CNN "Digikey Nr"
F 5 "0.98" H 2400 1650 50  0001 C CNN "Digikey Price"
	1    2400 1650
	0    -1   -1   0   
$EndComp
Connection ~ 2400 1450
$Comp
L Connector:TestPoint TP23
U 1 1 5D607BB1
P 2400 1450
F 0 "TP23" H 2458 1568 50  0000 L CNN
F 1 "TestPoint" H 2458 1477 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2600 1450 50  0001 C CNN
F 3 "~" H 2600 1450 50  0001 C CNN
	1    2400 1450
	1    0    0    -1  
$EndComp
$EndSCHEMATC

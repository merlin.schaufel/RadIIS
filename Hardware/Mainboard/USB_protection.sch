EESchema Schematic File Version 4
LIBS:RadIIS_Mainboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6400 2250 2    60   Input ~ 0
VBus_In
Text HLabel 6400 2350 2    60   Input ~ 0
Din-
Text HLabel 6400 2450 2    60   Input ~ 0
Din+
Text HLabel 6400 2550 2    60   Input ~ 0
ID_In
Text HLabel 3900 2250 0    60   Input ~ 0
VBus_Out
Text HLabel 3900 2350 0    60   Input ~ 0
Dout-
Text HLabel 3900 2450 0    60   Input ~ 0
Dout+
Text HLabel 3900 2550 0    60   Input ~ 0
ID_Out
$Comp
L Device:R R35
U 1 1 58B1AEE6
P 4350 2350
F 0 "R35" V 4300 2550 50  0000 C CNN
F 1 "22" V 4350 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4280 2350 50  0001 C CNN
F 3 "" H 4350 2350 50  0000 C CNN
F 4 "311-22.0HRCT-ND" H 4350 2350 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 4350 2350 50  0001 C CNN "Digikey Price"
	1    4350 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R R36
U 1 1 58B1AF3D
P 4350 2450
F 0 "R36" V 4300 2650 50  0000 C CNN
F 1 "22" V 4350 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4280 2450 50  0001 C CNN
F 3 "" H 4350 2450 50  0000 C CNN
F 4 "311-22.0HRCT-ND" H 4350 2450 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 4350 2450 50  0001 C CNN "Digikey Price"
	1    4350 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 2350 3900 2350
Wire Wire Line
	3900 2450 4200 2450
$Comp
L Connector:TestPoint W21
U 1 1 58B565E1
P 4000 2200
F 0 "W21" H 4000 2470 50  0000 C CNN
F 1 "TEST_1P" H 4000 2400 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4200 2200 50  0001 C CNN
F 3 "" H 4200 2200 50  0000 C CNN
F 4 "NA" H 4000 2200 50  0001 C CNN "Digikey Nr"
	1    4000 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2250 4000 2200
Connection ~ 4000 2250
Wire Wire Line
	4000 2250 3900 2250
Wire Wire Line
	5450 2250 6400 2250
Wire Wire Line
	4500 2350 4700 2350
Wire Wire Line
	4500 2450 4800 2450
Wire Wire Line
	4800 3450 4800 2450
Connection ~ 4800 2450
Wire Wire Line
	4800 2450 6400 2450
Wire Wire Line
	4700 3650 4700 2350
Connection ~ 4700 2350
Wire Wire Line
	4700 2350 6400 2350
Wire Wire Line
	3900 2550 6150 2550
Wire Wire Line
	6150 3450 6150 2550
Connection ~ 6150 2550
Wire Wire Line
	6150 2550 6400 2550
NoConn ~ 5950 3650
$Comp
L Device:Ferrite_Bead FB4
U 1 1 5CFC1ADE
P 5150 1500
F 0 "FB4" V 4876 1500 50  0000 C CNN
F 1 "Ferrite Bead" V 4967 1500 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 5080 1500 50  0001 C CNN
F 3 "https://katalog.we-online.de/pbs/datasheet/742792651.pdf" H 5150 1500 50  0001 C CNN
F 4 " 732-1593-1-ND" H 5150 1500 50  0001 C CNN "Digikey Nr"
F 5 "0.13" H 5150 1500 50  0001 C CNN "Digikey Price"
	1    5150 1500
	0    1    1    0   
$EndComp
$Comp
L Device:Ferrite_Bead FB3
U 1 1 5CFC224E
P 4500 1500
F 0 "FB3" V 4226 1500 50  0000 C CNN
F 1 "Ferrite Bead" V 4317 1500 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 4430 1500 50  0001 C CNN
F 3 "https://katalog.we-online.de/pbs/datasheet/742792651.pdf" H 4500 1500 50  0001 C CNN
F 4 " 732-1593-1-ND" H 4500 1500 50  0001 C CNN "Digikey Nr"
F 5 "0.13" H 4500 1500 50  0001 C CNN "Digikey Price"
	1    4500 1500
	0    1    1    0   
$EndComp
$Comp
L Device:C C49
U 1 1 5CEDB10E
P 4850 1750
F 0 "C49" V 4598 1750 50  0000 C CNN
F 1 "680nF" V 4689 1750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4888 1600 50  0001 C CNN
F 3 "~" H 4850 1750 50  0001 C CNN
F 4 "732-7970-1-ND" H 4850 1750 50  0001 C CNN "Digikey Nr"
F 5 "0.13" H 4850 1750 50  0001 C CNN "Digikey Price"
	1    4850 1750
	-1   0    0    1   
$EndComp
Text HLabel 4850 1950 3    60   Input ~ 0
GND
Wire Wire Line
	5450 1500 5300 1500
Wire Wire Line
	4850 1600 4850 1500
Wire Wire Line
	5000 1500 4850 1500
Wire Wire Line
	4850 1500 4650 1500
Connection ~ 4850 1500
Wire Wire Line
	4350 1500 4250 1500
Wire Wire Line
	4000 2250 4250 2250
Wire Wire Line
	4250 1500 4250 2250
Wire Wire Line
	5450 1500 5450 2250
Connection ~ 5450 2250
Wire Wire Line
	4850 1900 4850 1950
Wire Wire Line
	5450 4050 5450 4150
Text HLabel 5450 4150 3    60   Input ~ 0
GND
Wire Wire Line
	5450 2250 5450 3050
Wire Wire Line
	5950 3450 6150 3450
Wire Wire Line
	4950 3450 4800 3450
Wire Wire Line
	4950 3650 4700 3650
$Comp
L RadIIS_Mainboard:IP4220CZ6 U14
U 1 1 5CED2986
P 5450 3550
F 0 "U14" H 5200 3900 50  0000 C CNN
F 1 "IP4220CZ6" H 5750 3150 50  0000 C CNN
F 2 "RadIIS_Mainboard:SC-74" H 4500 3150 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/IP4220CZ6.pdf" H 5450 3550 50  0001 C CNN
F 4 "1727-3578-1-ND" H 5450 3550 50  0001 C CNN "Digikey Nr"
F 5 "0.37" H 5450 3550 50  0001 C CNN "Digikey Price"
	1    5450 3550
	1    0    0    -1  
$EndComp
$EndSCHEMATC

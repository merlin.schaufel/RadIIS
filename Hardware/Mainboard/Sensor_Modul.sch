EESchema Schematic File Version 4
LIBS:RadIIS_Mainboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5200 3450 0    61   Input ~ 0
Bias_In+
Text HLabel 4700 4250 0    61   Input ~ 0
Bias_In-
Text HLabel 5200 3850 0    61   Input ~ 0
Pulse_Out
Text HLabel 5200 3750 0    61   Input ~ 0
AGND
Text HLabel 5200 3950 0    61   Input ~ 0
A-2V0
Text HLabel 5200 3650 0    61   Input ~ 0
A3V3
Text HLabel 5200 3550 0    60   Input ~ 0
Temp_Out
Wire Wire Line
	5400 3450 5200 3450
Wire Wire Line
	5200 3550 5400 3550
Wire Wire Line
	5400 3650 5200 3650
Wire Wire Line
	5200 3750 5400 3750
Wire Wire Line
	5400 3850 5200 3850
Wire Wire Line
	5200 3950 5400 3950
Text HLabel 4800 4250 2    61   Input ~ 0
AGND
Wire Wire Line
	4700 4250 4800 4250
$Comp
L RadIIS_Mainboard:RadIISModule B1
U 1 1 5BE6B396
P 5900 3700
F 0 "B1" H 6200 3850 60  0000 C CNN
F 1 "RadIISModule" H 5900 3700 60  0000 C CNN
F 2 "RadIIS_Mainboard:RadIISModule" H 5900 3700 60  0001 C CNN
F 3 "https://radiis.de/" H 5900 3700 60  0001 C CNN
F 4 "NA" H 5900 3700 50  0001 C CNN "Digikey Nr"
	1    5900 3700
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 P2
U 1 1 57C7144A
P 5600 3650
F 0 "P2" H 5600 4000 50  0000 C CNN
F 1 "CONN_01X06" V 5700 3650 50  0000 C CNN
F 2 "RadIIS_Mainboard:6Pin_CardEdge_CON" H 5600 3650 50  0001 C CNN
F 3 "https://www.mouser.de/datasheet/2/40/VerticalTop_00-9159-952588.pdf" H 5600 3650 50  0001 C CNN
F 4 "Mouser Nr, not found at Digikey" H 5600 3650 50  0001 C CNN "Comment"
F 5 "581-009159006551906" H 5600 3650 50  0001 C CNN "Digikey Nr"
F 6 "1.06" H 5600 3650 50  0001 C CNN "Digikey Price"
	1    5600 3650
	1    0    0    -1  
$EndComp
$EndSCHEMATC

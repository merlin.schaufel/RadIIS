EESchema Schematic File Version 4
LIBS:RadIIS_Mainboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1600 1850 0    61   Input ~ 0
Pulse_In1
Text HLabel 8000 3550 0    61   Input ~ 0
Reset_In1
Text HLabel 2450 1500 3    61   Input ~ 0
AGND
Text HLabel 2250 1100 1    61   Input ~ 0
A3V3
Text HLabel 2250 2800 3    61   Input ~ 0
A-2V0
Text HLabel 6150 2050 2    61   Input ~ 0
PH_Out1
$Comp
L Device:C C5
U 1 1 572D13FF
P 2450 1300
F 0 "C5" H 2565 1346 50  0000 L CNN
F 1 "100nF" H 2565 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2488 1150 50  0001 C CNN
F 3 "" H 2450 1300 50  0000 C CNN
F 4 "1276-1000-1-ND " H 2450 1300 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 2450 1300 50  0001 C CNN "Digikey Price"
	1    2450 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 572D1431
P 5150 2500
F 0 "R9" V 4943 2500 50  0000 C CNN
F 1 "68k 1%" V 5034 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5080 2500 50  0001 C CNN
F 3 "" H 5150 2500 50  0000 C CNN
F 4 "311-68.0KHRCT-ND" H 5150 2500 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5150 2500 50  0001 C CNN "Digikey Price"
	1    5150 2500
	0    1    1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 572D15ED
P 2450 2500
F 0 "C7" H 2335 2454 50  0000 R CNN
F 1 "100nF" H 2335 2545 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2488 2350 50  0001 C CNN
F 3 "" H 2450 2500 50  0000 C CNN
F 4 "1276-1000-1-ND " H 2450 2500 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 2450 2500 50  0001 C CNN "Digikey Price"
	1    2450 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 1850 1850 1850
Wire Wire Line
	2250 1100 2250 1150
Wire Wire Line
	2450 1150 2250 1150
Connection ~ 2250 1150
Wire Wire Line
	2050 2050 2000 2050
Wire Wire Line
	2000 2050 2000 2300
Wire Wire Line
	2000 2300 3250 2300
$Comp
L Device:C C6
U 1 1 572D175F
P 3450 2500
F 0 "C6" H 3565 2546 50  0000 L CNN
F 1 "1nF (NP0!)" H 3565 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3488 2350 50  0001 C CNN
F 3 "" H 3450 2500 50  0000 C CNN
F 4 "1292-1475-1-ND" H 3450 2500 50  0001 C CNN "Digikey Nr"
F 5 "0.12" H 3450 2500 50  0001 C CNN "Digikey Price"
	1    3450 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1450 2450 1500
Wire Wire Line
	2700 1950 2650 1950
Wire Wire Line
	3250 2300 3250 2150
Text HLabel 2450 2800 3    61   Input ~ 0
AGND
Connection ~ 3450 1950
$Comp
L Device:R R10
U 1 1 572D1A63
P 4200 2750
F 0 "R10" H 4130 2704 50  0000 R CNN
F 1 "68k 1%" H 4130 2795 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4130 2750 50  0001 C CNN
F 3 "" H 4200 2750 50  0000 C CNN
F 4 "311-68.0KHRCT-ND" H 4200 2750 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 4200 2750 50  0001 C CNN "Digikey Price"
	1    4200 2750
	-1   0    0    1   
$EndComp
Text HLabel 4200 3450 3    61   Input ~ 0
AGND
Wire Wire Line
	4200 2150 4200 2500
Wire Wire Line
	4200 2500 5000 2500
Connection ~ 4200 2500
Wire Wire Line
	5300 2500 5350 2500
Wire Wire Line
	5350 2500 5350 2050
Wire Wire Line
	4800 2050 5350 2050
Connection ~ 5350 2050
Text HLabel 4400 1200 1    61   Input ~ 0
A3V3
Wire Wire Line
	4400 1200 4400 1750
Text HLabel 4700 2850 2    61   Input ~ 0
A-2V0
Wire Wire Line
	4200 2900 4200 3250
Connection ~ 4200 3250
Text Notes 2950 700  0    61   ~ 0
C6 Low Loss Dieelec. : NP0
Wire Wire Line
	4700 2850 4400 2850
Wire Wire Line
	4400 2850 4400 2350
Text HLabel 1650 5350 0    61   Input ~ 0
Pulse_In2
Text HLabel 8000 4200 0    61   Input ~ 0
Reset_In2
Text HLabel 2500 5000 3    61   Input ~ 0
AGND
Text HLabel 2300 4600 1    61   Input ~ 0
A3V3
Text HLabel 2300 6300 3    61   Input ~ 0
A-2V0
Text HLabel 6250 5550 2    61   Input ~ 0
PH_Out2
$Comp
L Device:C C4
U 1 1 58A853FE
P 2500 4800
F 0 "C4" H 2615 4846 50  0000 L CNN
F 1 "100nF" H 2615 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2538 4650 50  0001 C CNN
F 3 "" H 2500 4800 50  0000 C CNN
F 4 "1276-1000-1-ND " H 2500 4800 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 2500 4800 50  0001 C CNN "Digikey Price"
	1    2500 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R27
U 1 1 58A85404
P 5200 6000
F 0 "R27" V 4993 6000 50  0000 C CNN
F 1 "68k 1%" V 5084 6000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5130 6000 50  0001 C CNN
F 3 "" H 5200 6000 50  0000 C CNN
F 4 "311-68.0KHRCT-ND" H 5200 6000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5200 6000 50  0001 C CNN "Digikey Price"
	1    5200 6000
	0    1    1    0   
$EndComp
$Comp
L Device:C C8
U 1 1 58A85416
P 2500 6050
F 0 "C8" H 2615 6096 50  0000 L CNN
F 1 "100nF" H 2615 6005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2538 5900 50  0001 C CNN
F 3 "" H 2500 6050 50  0000 C CNN
F 4 "1276-1000-1-ND " H 2500 6050 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 2500 6050 50  0001 C CNN "Digikey Price"
	1    2500 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 5350 1800 5350
Wire Wire Line
	2300 4600 2300 4650
Wire Wire Line
	2500 4650 2300 4650
Connection ~ 2300 4650
Wire Wire Line
	2100 5550 2050 5550
Wire Wire Line
	2050 5550 2050 5800
Wire Wire Line
	2050 5800 3300 5800
$Comp
L Device:C C36
U 1 1 58A85426
P 3500 6000
F 0 "C36" H 3615 6046 50  0000 L CNN
F 1 "1nF (NP0!)" H 3615 5955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3538 5850 50  0001 C CNN
F 3 "" H 3500 6000 50  0000 C CNN
F 4 "1292-1475-1-ND" H 3500 6000 50  0001 C CNN "Digikey Nr"
F 5 "0.12" H 3500 6000 50  0001 C CNN "Digikey Price"
	1    3500 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4950 2500 5000
Wire Wire Line
	2750 5450 2700 5450
Wire Wire Line
	3300 5450 3500 5450
Wire Wire Line
	3300 5800 3300 5650
Text HLabel 3500 6300 3    61   Input ~ 0
AGND
Connection ~ 3500 5450
$Comp
L Device:R R26
U 1 1 58A85439
P 4250 6250
F 0 "R26" H 4180 6204 50  0000 R CNN
F 1 "68k 1%" H 4180 6295 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4180 6250 50  0001 C CNN
F 3 "" H 4250 6250 50  0000 C CNN
F 4 "311-68.0KHRCT-ND" H 4250 6250 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 4250 6250 50  0001 C CNN "Digikey Price"
	1    4250 6250
	-1   0    0    1   
$EndComp
Text HLabel 4250 6950 3    61   Input ~ 0
AGND
Wire Wire Line
	4250 5650 4250 6000
Wire Wire Line
	4250 6000 5050 6000
Connection ~ 4250 6000
Wire Wire Line
	5350 6000 5400 6000
Wire Wire Line
	5400 6000 5400 5550
Wire Wire Line
	4850 5550 5400 5550
Text HLabel 4450 4700 1    61   Input ~ 0
A3V3
Wire Wire Line
	4450 4700 4450 5250
Text HLabel 4750 6350 2    61   Input ~ 0
A-2V0
Wire Wire Line
	4250 6400 4250 6750
Connection ~ 4250 6750
Text Notes 3050 4100 0    61   ~ 0
C36 Low Loss Dieelec. : NP0
Wire Wire Line
	4750 6350 4450 6350
Wire Wire Line
	4450 6350 4450 5850
$Comp
L Connector:TestPoint W16
U 1 1 58B587C5
P 5900 1800
F 0 "W16" H 5900 2070 50  0000 C CNN
F 1 "TEST_1P" H 5900 2000 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 6100 1800 50  0001 C CNN
F 3 "" H 6100 1800 50  0000 C CNN
F 4 "NA" H 5900 1800 50  0001 C CNN "Digikey Nr"
	1    5900 1800
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint W15
U 1 1 58B58824
P 1850 1850
F 0 "W15" H 1850 2120 50  0000 C CNN
F 1 "TEST_1P" H 1850 2050 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 2050 1850 50  0001 C CNN
F 3 "" H 2050 1850 50  0000 C CNN
F 4 "NA" H 1850 1850 50  0001 C CNN "Digikey Nr"
	1    1850 1850
	1    0    0    -1  
$EndComp
Connection ~ 1850 1850
Wire Wire Line
	3000 2150 3250 2150
Connection ~ 3250 2150
Wire Wire Line
	3250 1950 3450 1950
Wire Wire Line
	3050 5650 3300 5650
Connection ~ 3300 5650
Text HLabel 9750 2300 3    61   Input ~ 0
AGND
Text HLabel 9750 3700 2    61   Input ~ 0
AGND
Wire Wire Line
	9750 3700 9650 3700
Wire Wire Line
	9750 4400 9650 4400
Wire Wire Line
	8850 4900 8850 4800
Wire Wire Line
	8050 4200 8000 4200
Wire Wire Line
	8050 3550 8000 3550
Text HLabel 8850 2850 1    60   Input ~ 0
3V3D
$Comp
L Device:C C54
U 1 1 58B5A5A4
P 9750 2050
F 0 "C54" H 9865 2096 50  0000 L CNN
F 1 "100nF" H 9865 2005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9788 1900 50  0001 C CNN
F 3 "" H 9750 2050 50  0000 C CNN
F 4 "1276-1000-1-ND " H 9750 2050 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 9750 2050 50  0001 C CNN "Digikey Price"
	1    9750 2050
	1    0    0    -1  
$EndComp
Text HLabel 9750 1800 1    60   Input ~ 0
3V3D
Wire Wire Line
	9750 1800 9750 1900
Wire Wire Line
	9750 2300 9750 2200
Wire Wire Line
	8850 2850 8850 2950
Text Label 3450 1800 0    60   ~ 0
RESET1
Wire Wire Line
	3450 1800 3450 1950
Wire Wire Line
	3500 5450 3500 5300
Text Label 3500 5300 0    60   ~ 0
RESET2
Text Label 9750 4050 0    60   ~ 0
RESET2
Text Label 9750 3350 0    60   ~ 0
RESET1
Wire Wire Line
	9750 3350 9650 3350
Wire Wire Line
	9750 4050 9650 4050
Text HLabel 8850 4900 3    61   Input ~ 0
AGND
Text HLabel 9750 4400 2    61   Input ~ 0
AGND
Wire Wire Line
	2250 1150 2250 1650
Wire Wire Line
	3450 1950 4200 1950
Wire Wire Line
	4200 2500 4200 2600
Wire Wire Line
	4200 3250 4200 3450
Wire Wire Line
	2300 4650 2300 5150
Wire Wire Line
	3500 5450 4250 5450
Wire Wire Line
	4250 6000 4250 6100
Wire Wire Line
	4250 6750 4250 6950
Wire Wire Line
	1850 1850 2050 1850
Wire Wire Line
	3250 2150 3250 1950
Wire Wire Line
	3300 5650 3300 5450
$Comp
L Device:D_Schottky_x2_ACom_AKK D1
U 1 1 5B8F76ED
P 3000 1850
F 0 "D1" V 3000 1900 50  0000 L CNN
F 1 "D_Schottky_x2_ACom_AKK" V 3400 1150 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3000 1850 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/MBD301-D.PDF" H 3000 1850 50  0001 C CNN
F 4 "MMBD301LT1GOSCT-ND" H 3000 1850 50  0001 C CNN "Digikey Nr"
F 5 "0.23" H 3000 1850 50  0001 C CNN "Digikey Price"
	1    3000 1850
	0    1    1    0   
$EndComp
NoConn ~ 3000 1550
Wire Wire Line
	2700 1950 2700 1850
Wire Wire Line
	2700 1850 2800 1850
$Comp
L Device:D_Schottky_x2_ACom_AKK D3
U 1 1 5B8FBCBD
P 3050 5350
F 0 "D3" V 3050 5400 50  0000 L CNN
F 1 "D_Schottky_x2_ACom_AKK" V 3450 4650 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3050 5350 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/MBD301-D.PDF" H 3050 5350 50  0001 C CNN
F 4 "MMBD301LT1GOSCT-ND" H 3050 5350 50  0001 C CNN "Digikey Nr"
F 5 "0.23" H 3050 5350 50  0001 C CNN "Digikey Price"
	1    3050 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 5450 2750 5350
Wire Wire Line
	2750 5350 2850 5350
NoConn ~ 3050 5050
$Comp
L Device:R R15
U 1 1 5B900075
P 3450 2150
F 0 "R15" H 3520 2196 50  0000 L CNN
F 1 "59" H 3520 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3380 2150 50  0001 C CNN
F 3 "~" H 3450 2150 50  0001 C CNN
F 4 " 311-59.0HRCT-ND " H 3450 2150 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3450 2150 50  0001 C CNN "Digikey Price"
	1    3450 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1950 3450 2000
Wire Wire Line
	3450 2300 3450 2350
Wire Wire Line
	2250 2250 2250 2350
Wire Wire Line
	2450 2350 2250 2350
Connection ~ 2250 2350
Wire Wire Line
	2250 2350 2250 2800
Text HLabel 3450 2800 3    61   Input ~ 0
AGND
Text HLabel 2500 6300 3    61   Input ~ 0
AGND
Wire Wire Line
	2300 5750 2300 5900
Wire Wire Line
	2500 5900 2300 5900
Connection ~ 2300 5900
Wire Wire Line
	2300 5900 2300 6300
Wire Wire Line
	2500 6200 2500 6300
$Comp
L Device:R R16
U 1 1 5B914FAF
P 3500 5650
F 0 "R16" H 3570 5696 50  0000 L CNN
F 1 "59" H 3570 5605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3430 5650 50  0001 C CNN
F 3 "~" H 3500 5650 50  0001 C CNN
F 4 " 311-59.0HRCT-ND " H 3500 5650 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3500 5650 50  0001 C CNN "Digikey Price"
	1    3500 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6300 3500 6150
Wire Wire Line
	3500 5850 3500 5800
Wire Wire Line
	3500 5500 3500 5450
Wire Wire Line
	3450 2650 3450 2800
Wire Wire Line
	2450 2650 2450 2800
$Comp
L Device:R R23
U 1 1 5B924DF8
P 5650 5550
F 0 "R23" V 5443 5550 50  0000 C CNN
F 1 "120" V 5534 5550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5580 5550 50  0001 C CNN
F 3 "~" H 5650 5550 50  0001 C CNN
F 4 " 311-120HRCT-ND " H 5650 5550 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5650 5550 50  0001 C CNN "Digikey Price"
	1    5650 5550
	0    1    1    0   
$EndComp
$Comp
L Device:C C38
U 1 1 5B924F4D
P 5900 6200
F 0 "C38" H 6015 6246 50  0000 L CNN
F 1 "100pF" H 6015 6155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5938 6050 50  0001 C CNN
F 3 "~" H 5900 6200 50  0001 C CNN
F 4 "311-1069-1-ND " H 5900 6200 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5900 6200 50  0001 C CNN "Digikey Price"
	1    5900 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 5550 5900 5550
Wire Wire Line
	5900 6050 5900 5550
Connection ~ 5900 5550
Wire Wire Line
	5900 5550 5800 5550
Wire Wire Line
	5500 5550 5400 5550
Connection ~ 5400 5550
$Comp
L Device:R R22
U 1 1 5B936977
P 5650 2050
F 0 "R22" V 5443 2050 50  0000 C CNN
F 1 "120" V 5534 2050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5580 2050 50  0001 C CNN
F 3 "~" H 5650 2050 50  0001 C CNN
F 4 " 311-120HRCT-ND " H 5650 2050 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5650 2050 50  0001 C CNN "Digikey Price"
	1    5650 2050
	0    1    1    0   
$EndComp
$Comp
L Device:C C31
U 1 1 5B936A21
P 5900 2650
F 0 "C31" H 6015 2696 50  0000 L CNN
F 1 "100pF" H 6015 2605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5938 2500 50  0001 C CNN
F 3 "~" H 5900 2650 50  0001 C CNN
F 4 "311-1069-1-ND " H 5900 2650 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5900 2650 50  0001 C CNN "Digikey Price"
	1    5900 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2050 5900 2050
Wire Wire Line
	5900 2500 5900 2050
Connection ~ 5900 2050
Wire Wire Line
	5900 2050 5800 2050
Wire Wire Line
	5500 2050 5350 2050
$Comp
L RadIIS_Mainboard:MAX4541 U5
U 1 1 58B59F54
P 8850 3900
F 0 "U5" H 8300 4700 60  0000 C CNN
F 1 "MAX4541" H 8450 3250 60  0000 C CNN
F 2 "RadIIS_Mainboard:uMAX8" H 8850 3900 60  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX4541-MAX4544.pdf" H 8850 3900 60  0001 C CNN
F 4 "MAX4541EUA+-ND" H 8850 3900 50  0001 C CNN "Digikey Nr"
F 5 "1.13" H 8850 3900 50  0001 C CNN "Digikey Price"
	1    8850 3900
	1    0    0    -1  
$EndComp
$Comp
L RadIIS_Mainboard:OPA2354xxDGK U4
U 1 1 5D34BC23
P 2350 1950
F 0 "U4" H 2550 1900 50  0000 L CNN
F 1 "OPA2354xxDGK" H 2350 1800 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_3.0x3.0mm_P0.65mm" H 1500 1650 50  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fopa354" H 2350 1950 50  0001 C CNN
F 4 "296-12642-1-ND" H 1500 1550 60  0001 L CNN "Digikey Nr"
F 5 "2.57" H 1500 1450 60  0001 L CNN "Digikey Price"
	1    2350 1950
	1    0    0    -1  
$EndComp
$Comp
L RadIIS_Mainboard:OPA2354xxDGK U4
U 2 1 5D34C5AA
P 4500 2050
F 0 "U4" H 4700 2000 50  0000 L CNN
F 1 "OPA2354xxDGK" H 4500 1900 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_3.0x3.0mm_P0.65mm" H 3650 1750 50  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fopa354" H 4500 2050 50  0001 C CNN
F 4 "296-12642-1-ND" H 3650 1650 60  0001 L CNN "Digikey Nr"
F 5 "2.57" H 3650 1550 60  0001 L CNN "Digikey Price"
	2    4500 2050
	1    0    0    -1  
$EndComp
$Comp
L RadIIS_Mainboard:OPA2354xxDGK U11
U 1 1 5D34E60D
P 2400 5450
F 0 "U11" H 2600 5400 50  0000 L CNN
F 1 "OPA2354xxDGK" H 2400 5300 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_3.0x3.0mm_P0.65mm" H 1550 5150 50  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fopa354" H 2400 5450 50  0001 C CNN
F 4 "296-12642-1-ND" H 1550 5050 60  0001 L CNN "Digikey Nr"
F 5 "2.57" H 1550 4950 60  0001 L CNN "Digikey Price"
	1    2400 5450
	1    0    0    -1  
$EndComp
$Comp
L RadIIS_Mainboard:OPA2354xxDGK U11
U 2 1 5D34EC22
P 4550 5550
F 0 "U11" H 4750 5500 50  0000 L CNN
F 1 "OPA2354xxDGK" H 4550 5400 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_3.0x3.0mm_P0.65mm" H 3700 5250 50  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fopa354" H 4550 5550 50  0001 C CNN
F 4 "296-12642-1-ND" H 3700 5150 60  0001 L CNN "Digikey Nr"
F 5 "2.57" H 3700 5050 60  0001 L CNN "Digikey Price"
	2    4550 5550
	1    0    0    -1  
$EndComp
Text Notes 6300 800  0    60   ~ 0
Using OPA2354 in VSSOP8 package to make it interchangebale with OPA2356.
Wire Wire Line
	4200 3250 5900 3250
Wire Wire Line
	5900 2800 5900 3250
Wire Wire Line
	4250 6750 5900 6750
Wire Wire Line
	5900 6350 5900 6750
Wire Notes Line
	6250 750  5200 750 
Wire Notes Line
	5200 750  5200 1800
Wire Notes Line
	5200 1800 4550 1800
Wire Notes Line
	4550 1800 4550 1950
Wire Notes Line
	4000 750  4000 2450
Wire Notes Line
	4000 2450 3800 2450
Wire Notes Line
	3900 5950 4150 5950
Wire Notes Line
	4150 5950 4150 4150
Wire Wire Line
	5900 1800 5900 2050
$Comp
L Connector:TestPoint TP3
U 1 1 5D67B5F8
P 5900 1800
F 0 "TP3" V 5854 1988 50  0000 L CNN
F 1 "TestPoint" V 5945 1988 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6100 1800 50  0001 C CNN
F 3 "~" H 6100 1800 50  0001 C CNN
	1    5900 1800
	0    1    1    0   
$EndComp
Connection ~ 5900 1800
$Comp
L Connector:TestPoint TP2
U 1 1 5D67E644
P 1850 1850
F 0 "TP2" H 1792 1876 50  0000 R CNN
F 1 "TestPoint" H 1792 1967 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2050 1850 50  0001 C CNN
F 3 "~" H 2050 1850 50  0001 C CNN
	1    1850 1850
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5D68369F
P 1800 5350
F 0 "TP1" H 1742 5376 50  0000 R CNN
F 1 "TestPoint" H 1742 5467 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2000 5350 50  0001 C CNN
F 3 "~" H 2000 5350 50  0001 C CNN
	1    1800 5350
	-1   0    0    1   
$EndComp
Connection ~ 1800 5350
Wire Wire Line
	1800 5350 2100 5350
$Comp
L Connector:TestPoint TP4
U 1 1 5D6842AB
P 5900 5550
F 0 "TP4" H 5958 5668 50  0000 L CNN
F 1 "TestPoint" H 5958 5577 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6100 5550 50  0001 C CNN
F 3 "~" H 6100 5550 50  0001 C CNN
	1    5900 5550
	1    0    0    -1  
$EndComp
$EndSCHEMATC

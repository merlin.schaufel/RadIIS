EESchema Schematic File Version 4
LIBS:RadIIS_Mainboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C40
U 1 1 58B07946
P 4400 3000
F 0 "C40" H 4425 3100 50  0000 L CNN
F 1 "10uF 50V" H 4425 2900 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4438 2850 50  0001 C CNN
F 3 "" H 4400 3000 50  0000 C CNN
F 4 "1276-6736-1-ND" H 4400 3000 50  0001 C CNN "Digikey Nr"
F 5 "0.27" H 4400 3000 50  0001 C CNN "Digikey Price"
	1    4400 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:L L2
U 1 1 58B079EF
P 6500 2750
F 0 "L2" V 6450 2750 50  0000 C CNN
F 1 "4.7uH" V 6575 2750 50  0000 C CNN
F 2 "Inductor_SMD:L_Taiyo-Yuden_NR-50xx" H 6500 2750 50  0001 C CNN
F 3 "https://www.yuden.co.jp/productdata/catalog/wound04_e.pdf" H 6500 2750 50  0001 C CNN
F 4 " NRS5020T4R7MMGJ" V 6500 2750 60  0001 C CNN "Type"
F 5 " 587-2407-1-ND " H 6500 2750 50  0001 C CNN "Digikey Nr"
F 6 "0,30" H 6500 2750 50  0001 C CNN "Digikey Price"
	1    6500 2750
	0    -1   -1   0   
$EndComp
Text HLabel 2450 2750 0    60   Input ~ 0
Ext_Vin
Text HLabel 4400 3300 3    60   Input ~ 0
GND
Text HLabel 4800 3000 3    60   Input ~ 0
EN_DCDCin
Text HLabel 5350 3450 3    60   Input ~ 0
GND
$Comp
L Device:C C42
U 1 1 58B07C21
P 7050 2900
F 0 "C42" H 7075 3000 50  0000 L CNN
F 1 "22uF" H 7075 2800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7088 2750 50  0001 C CNN
F 3 "" H 7050 2900 50  0000 C CNN
F 4 "C2012X5R1A226K125AB" H 7050 2900 60  0001 C CNN "Type"
F 5 "1276-6780-1-ND" H 7050 2900 50  0001 C CNN "Digikey Nr"
F 6 "0.27" H 7050 2900 50  0001 C CNN "Digikey Price"
	1    7050 2900
	-1   0    0    1   
$EndComp
Text HLabel 7050 3150 3    60   Input ~ 0
GND
$Comp
L Device:R R31
U 1 1 58B07E98
P 5300 2350
F 0 "R31" V 5380 2350 50  0000 C CNN
F 1 "NA/0" V 5300 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5230 2350 50  0001 C CNN
F 3 "" H 5300 2350 50  0000 C CNN
F 4 "311-0.0GRCT-ND" H 5300 2350 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5300 2350 50  0001 C CNN "Digikey Price"
	1    5300 2350
	0    1    1    0   
$EndComp
$Comp
L Device:C C39
U 1 1 58B07FA9
P 3950 3000
F 0 "C39" H 3800 2900 50  0000 L CNN
F 1 "100nF 50V" H 3700 3100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3988 2850 50  0001 C CNN
F 3 "" H 3950 3000 50  0000 C CNN
F 4 " 1292-1392-1-ND" H 3950 3000 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3950 3000 50  0001 C CNN "Digikey Price"
	1    3950 3000
	-1   0    0    1   
$EndComp
Text HLabel 7400 3150 3    60   Input ~ 0
GND
Text HLabel 7550 2750 2    60   Input ~ 0
Reg_Vout
Text Notes 5050 2250 0    60   ~ 0
If TS300x1 not assembled,\nR = 0 Ohm
$Comp
L Device:R R33
U 1 1 58B12612
P 6500 3050
F 0 "R33" V 6400 3050 50  0000 C CNN
F 1 "47k" V 6500 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6430 3050 50  0001 C CNN
F 3 "" H 6500 3050 50  0000 C CNN
F 4 "311-47.0KHRCT-ND" H 6500 3050 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 6500 3050 50  0001 C CNN "Digikey Price"
	1    6500 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R32
U 1 1 58B12671
P 6150 3350
F 0 "R32" V 6230 3350 50  0000 C CNN
F 1 "10k" V 6150 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 3350 50  0001 C CNN
F 3 "" H 6150 3350 50  0000 C CNN
F 4 "311-10.0KHRCT-ND" H 6150 3350 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 6150 3350 50  0001 C CNN "Digikey Price"
	1    6150 3350
	-1   0    0    1   
$EndComp
$Comp
L Device:C C43
U 1 1 58B12BA5
P 7400 2900
F 0 "C43" H 7425 3000 50  0000 L CNN
F 1 "22uF" H 7425 2800 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7438 2750 50  0001 C CNN
F 3 "" H 7400 2900 50  0000 C CNN
F 4 "C2012X5R1A226K125AB" H 7400 2900 60  0001 C CNN "Type"
F 5 "1276-6780-1-ND" H 7400 2900 50  0001 C CNN "Digikey Nr"
F 6 "0.27" H 7400 2900 50  0001 C CNN "Digikey Price"
	1    7400 2900
	-1   0    0    1   
$EndComp
Text HLabel 3950 3300 3    60   Input ~ 0
GND
$Comp
L Connector:TestPoint W20
U 1 1 58B560DB
P 7400 2750
F 0 "W20" H 7400 3020 50  0000 C CNN
F 1 "TEST_1P" H 7400 2950 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7600 2750 50  0001 C CNN
F 3 "" H 7600 2750 50  0000 C CNN
F 4 "NA" H 7400 2750 50  0001 C CNN "Digikey Nr"
	1    7400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2750 6750 2750
Connection ~ 6750 2750
Wire Wire Line
	7050 3150 7050 3050
Wire Wire Line
	7400 3150 7400 3050
Connection ~ 7050 2750
Connection ~ 7400 2750
Wire Wire Line
	6150 3500 6150 3550
$Comp
L RadIIS_Mainboard:TS30041_11 U13
U 1 1 58B6F4C6
P 5300 2950
F 0 "U13" H 5500 2650 60  0000 C CNN
F 1 "TS30041/11" H 5300 3250 60  0000 C CNN
F 2 "Housings_DFN_QFN:QFN-16-1EP_3x3mm_Pitch0.5mm" H 5250 2950 60  0001 C CNN
F 3 "https://www.semtech.com/uploads/documents/ts3004x.pdf" H 5250 2950 60  0001 C CNN
F 4 "Maybe switch to fixed?" H 5300 2950 50  0001 C CNN "Comment"
F 5 "TS30041-M000QFNRCT-ND" H 5300 2950 50  0001 C CNN "Digikey Nr"
F 6 "1.38" H 5300 2950 50  0001 C CNN "Digikey Price"
	1    5300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 2750 7550 2750
$Comp
L Device:D_Schottky_ALT D7
U 1 1 5CBAFC79
P 5750 3950
F 0 "D7" V 5704 4029 50  0000 L CNN
F 1 "D_Schottky_ALT" V 5795 4029 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123F" H 5750 3950 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/MBR230LSFT1-D.PDF" H 5750 3950 50  0001 C CNN
F 4 "MBR230LSFT1GOSCT-ND" H 5750 3950 50  0001 C CNN "Digikey Nr"
F 5 "0.39" H 5750 3950 50  0001 C CNN "Digikey Price"
	1    5750 3950
	0    1    1    0   
$EndComp
Text HLabel 5750 4200 3    60   Input ~ 0
GND
Wire Wire Line
	5650 2900 5750 2900
Wire Wire Line
	5650 3050 6150 3050
Wire Wire Line
	3950 3300 3950 3150
Wire Wire Line
	3950 2850 3950 2750
Connection ~ 3950 2750
Wire Wire Line
	3950 2750 4400 2750
Wire Wire Line
	4400 2850 4400 2750
Connection ~ 4400 2750
Wire Wire Line
	4400 3150 4400 3300
Wire Wire Line
	4800 3000 4800 2900
Wire Wire Line
	4800 2900 4950 2900
Wire Wire Line
	4400 2750 4950 2750
Wire Wire Line
	5150 2350 3950 2350
Wire Wire Line
	3950 2350 3950 2750
Wire Wire Line
	5450 2350 6750 2350
$Comp
L Device:C C41
U 1 1 58B07987
P 5950 2750
F 0 "C41" H 5975 2850 50  0000 L CNN
F 1 "22nF" H 5975 2650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5988 2600 50  0001 C CNN
F 3 "" H 5950 2750 50  0000 C CNN
F 4 "399-1280-1-ND " H 5950 2750 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5950 2750 50  0001 C CNN "Digikey Price"
	1    5950 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 2750 5800 2750
Wire Wire Line
	6100 2750 6250 2750
Wire Wire Line
	5750 3800 5750 2900
Connection ~ 5750 2900
Wire Wire Line
	5750 4200 5750 4100
Wire Wire Line
	6150 3200 6150 3050
Connection ~ 6150 3050
Wire Wire Line
	6150 3050 6350 3050
Wire Wire Line
	5750 2900 6250 2900
Wire Wire Line
	6250 2750 6250 2900
Wire Wire Line
	6750 2350 6750 2750
Wire Wire Line
	6650 3050 6750 3050
Wire Wire Line
	6750 2750 6750 3050
Wire Wire Line
	6350 2750 6250 2750
Connection ~ 6250 2750
Wire Notes Line
	6300 2850 6300 2500
Wire Notes Line
	6300 2500 6700 2500
Wire Notes Line
	6700 2500 6700 2850
Wire Notes Line
	6700 2850 6300 2850
Text Notes 6300 2600 0    60   ~ 0
shielded
Wire Wire Line
	6750 2750 7050 2750
Wire Wire Line
	7050 2750 7400 2750
Text Label 6250 2900 2    60   ~ 0
VSW
$Comp
L Device:Ferrite_Bead FB2
U 1 1 5CF47AD2
P 3450 2750
F 0 "FB2" V 3176 2750 50  0000 C CNN
F 1 "1MHZ, 500mA" V 3267 2750 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" V 3380 2750 50  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/beads_commercial_power_mpz1608_en.pdf" H 3450 2750 50  0001 C CNN
F 4 " 445-2205-1-ND" H 3450 2750 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3450 2750 50  0001 C CNN "Digikey Price"
	1    3450 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	3600 2750 3950 2750
Wire Wire Line
	3300 2750 2450 2750
Wire Wire Line
	5350 3300 5350 3450
Wire Wire Line
	5150 3300 5150 3450
Text HLabel 6150 3550 3    60   Input ~ 0
GND
Text HLabel 5150 3450 3    60   Input ~ 0
GND
Text Notes 2550 2750 0    60   ~ 0
max 40V
Text HLabel 5650 3250 3    60   Input ~ 0
PG_DCDCin
Wire Wire Line
	5650 3250 5650 3150
$EndSCHEMATC

EESchema Schematic File Version 4
LIBS:RadIIS_Mainboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8000 3200 2    60   Input ~ 0
Bat_Out
Text HLabel 3650 3650 3    60   Input ~ 0
GND
Text HLabel 3100 3500 0    60   Input ~ 0
USB_VCC_IN
$Comp
L Device:C C17
U 1 1 57AD4115
P 3650 3400
F 0 "C17" H 3675 3500 50  0000 L CNN
F 1 "4.7µF" H 3675 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3688 3250 50  0001 C CNN
F 3 "" H 3650 3400 50  0000 C CNN
F 4 "1276-1065-1-ND" H 3650 3400 50  0001 C CNN "Digikey Nr"
F 5 "0.15" H 3650 3400 50  0001 C CNN "Digikey Price"
	1    3650 3400
	1    0    0    -1  
$EndComp
Text HLabel 3450 4500 0    60   Input ~ 0
Charger_STAT
Text HLabel 5400 4150 3    60   Input ~ 0
GND
$Comp
L Device:R R12
U 1 1 57AD46B8
P 5400 3850
F 0 "R12" V 5480 3850 50  0000 C CNN
F 1 "2k" V 5400 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5330 3850 50  0001 C CNN
F 3 "" H 5400 3850 50  0000 C CNN
F 4 "311-2.00KHRCT-ND" H 5400 3850 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 5400 3850 50  0001 C CNN "Digikey Price"
	1    5400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3700 5400 3650
Wire Wire Line
	5400 3650 5250 3650
Wire Wire Line
	5400 4000 5400 4100
Wire Wire Line
	5400 4100 5250 4100
Connection ~ 5400 4100
$Comp
L Device:C C18
U 1 1 57AD4766
P 5700 3450
F 0 "C18" H 5725 3550 50  0000 L CNN
F 1 "4.7µF" H 5725 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5738 3300 50  0001 C CNN
F 3 "" H 5700 3450 50  0000 C CNN
F 4 "1276-1065-1-ND" H 5700 3450 50  0001 C CNN "Digikey Nr"
F 5 "0.15" H 5700 3450 50  0001 C CNN "Digikey Price"
	1    5700 3450
	1    0    0    -1  
$EndComp
Text HLabel 5700 3700 3    60   Input ~ 0
GND
Wire Wire Line
	5700 3600 5700 3700
Text HLabel 3100 2900 0    60   Input ~ 0
REG_VCC_IN
$Comp
L Device:D_Schottky_x2_KCom_AAK D4
U 1 1 58B16395
P 3300 3200
F 0 "D4" H 3350 3100 50  0000 C CNN
F 1 "PMEG2005CT" H 3300 3350 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3300 3200 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/PMEG2005CT.pdf" H 3300 3200 50  0001 C CNN
F 4 "1727-5188-1-ND" H 3300 3200 50  0001 C CNN "Digikey Nr"
F 5 "0.41" H 3300 3200 50  0001 C CNN "Digikey Price"
	1    3300 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3300 3500 3100 3500
Wire Wire Line
	3100 2900 3300 2900
$Comp
L Device:Battery_Cell BT1
U 1 1 58B1A0C5
P 6150 3950
F 0 "BT1" H 6268 4046 50  0000 L CNN
F 1 "Battery_Cell" H 6268 3955 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 6150 4010 50  0001 C CNN
F 3 "" V 6150 4010 50  0000 C CNN
F 4 "NA" H 6150 3950 50  0001 C CNN "Digikey Nr"
	1    6150 3950
	1    0    0    -1  
$EndComp
Text HLabel 6450 4250 3    60   Input ~ 0
GND
Text Notes 7450 3050 0    60   ~ 0
Resistor for battery monitoring
$Comp
L Device:R R29
U 1 1 58B54E21
P 7750 3400
F 0 "R29" V 7830 3400 50  0000 C CNN
F 1 "1M 1%" V 7650 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7680 3400 50  0001 C CNN
F 3 "" H 7750 3400 50  0000 C CNN
F 4 "311-1.00MHRCT-ND" H 7750 3400 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 7750 3400 50  0001 C CNN "Digikey Price"
	1    7750 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R30
U 1 1 58B54EAC
P 7750 3800
F 0 "R30" V 7830 3800 50  0000 C CNN
F 1 "2M2 1%" V 7650 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7680 3800 50  0001 C CNN
F 3 "" H 7750 3800 50  0000 C CNN
F 4 "YAG3329CT-ND" H 7750 3800 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 7750 3800 50  0001 C CNN "Digikey Price"
	1    7750 3800
	1    0    0    -1  
$EndComp
Text HLabel 7750 4050 3    60   Input ~ 0
GND
$Comp
L Device:C C37
U 1 1 58B54F00
P 8000 3800
F 0 "C37" H 8025 3900 50  0000 L CNN
F 1 "100nF" H 8025 3700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8038 3650 50  0001 C CNN
F 3 "" H 8000 3800 50  0000 C CNN
F 4 "1276-1000-1-ND " H 8000 3800 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 8000 3800 50  0001 C CNN "Digikey Price"
	1    8000 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3250 7750 3200
Text HLabel 8200 3600 2    60   Input ~ 0
Bat_SensOut
$Comp
L RadIIS_Mainboard:MCP73831 U6
U 1 1 58B9C10E
P 4550 3650
F 0 "U6" H 4700 4250 50  0000 C CNN
F 1 "MCP73831" H 4550 3050 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4550 3650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 4550 3650 50  0001 C CNN
F 4 "MCP73831T-2ACI/OTCT-ND" H 4550 3650 50  0001 C CNN "Digikey Nr"
F 5 "0.52" H 4550 3650 50  0001 C CNN "Digikey Price"
	1    4550 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:Polyfuse F1
U 1 1 5B20FE45
P 6150 3500
F 0 "F1" V 6050 3500 50  0000 C CNN
F 1 "Polyfuse" V 6250 3500 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric" H 6200 3300 50  0001 L CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/mfnsml.pdf" H 6150 3500 50  0001 C CNN
F 4 "MF-NSML175-2CT-ND" H 6150 3500 50  0001 C CNN "Digikey Nr"
F 5 "0.85" H 6150 3500 50  0001 C CNN "Digikey Price"
F 6 "3A5" V 6350 3500 50  0000 C CNN "Trip"
	1    6150 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3750 6150 3650
Wire Wire Line
	6150 3350 6150 3200
Wire Wire Line
	5400 4100 5400 4150
Wire Wire Line
	7750 3200 8000 3200
Wire Wire Line
	7750 4000 7750 4050
Wire Wire Line
	7750 3950 7750 4000
Connection ~ 7750 4000
Wire Wire Line
	8000 4000 7750 4000
Wire Wire Line
	8000 3950 8000 4000
Wire Wire Line
	7750 3550 7750 3600
Wire Wire Line
	7750 3600 8000 3600
Connection ~ 7750 3600
Wire Wire Line
	7750 3600 7750 3650
Wire Wire Line
	8000 3650 8000 3600
Connection ~ 8000 3600
Wire Wire Line
	8000 3600 8200 3600
Wire Wire Line
	6800 3500 6800 3200
Connection ~ 7750 3200
Wire Notes Line
	7500 3100 7500 4350
Wire Notes Line
	7500 4350 8900 4350
Wire Notes Line
	8900 4350 8900 3100
Wire Notes Line
	8900 3100 7500 3100
Wire Wire Line
	3500 3200 3650 3200
Wire Wire Line
	3650 3650 3650 3550
Wire Wire Line
	3650 3250 3650 3200
Connection ~ 3650 3200
Wire Wire Line
	3650 3200 3850 3200
Wire Wire Line
	5250 3200 5500 3200
Wire Wire Line
	5700 3300 5700 3200
Connection ~ 5700 3200
Wire Wire Line
	6150 4050 6150 4100
Wire Wire Line
	6800 3800 6800 4100
Connection ~ 6150 3200
Wire Wire Line
	6150 3200 6800 3200
Wire Wire Line
	5700 3200 6150 3200
Wire Wire Line
	6150 4100 6450 4100
Connection ~ 6450 4100
Wire Wire Line
	6450 4100 6800 4100
Wire Wire Line
	6450 4100 6450 4250
$Comp
L Device:D_Schottky_ALT D?
U 1 1 5D2C3AE6
P 6800 3650
AR Path="/58B072DB/5D2C3AE6" Ref="D?"  Part="1" 
AR Path="/5728B04B/5D2C3AE6" Ref="D8"  Part="1" 
F 0 "D8" V 6754 3729 50  0000 L CNN
F 1 "D_Schottky_ALT" V 6845 3729 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123F" H 6800 3650 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/MBR230LSFT1-D.PDF" H 6800 3650 50  0001 C CNN
F 4 "MBR230LSFT1GOSCT-ND" H 6800 3650 50  0001 C CNN "Digikey Nr"
F 5 "0.39" H 6800 3650 50  0001 C CNN "Digikey Price"
	1    6800 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 3200 7750 3200
Connection ~ 6800 3200
Wire Wire Line
	5500 3300 5500 3200
Connection ~ 5500 3200
Wire Wire Line
	5500 3200 5700 3200
Wire Wire Line
	5500 3600 5700 3600
Connection ~ 5700 3600
$Comp
L Device:C C50
U 1 1 5D5B0114
P 5500 3450
F 0 "C50" H 5350 3550 50  0000 L CNN
F 1 "4.7µF" H 5250 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5538 3300 50  0001 C CNN
F 3 "" H 5500 3450 50  0000 C CNN
F 4 "1276-1065-1-ND" H 5500 3450 50  0001 C CNN "Digikey Nr"
F 5 "0.15" H 5500 3450 50  0001 C CNN "Digikey Price"
	1    5500 3450
	1    0    0    -1  
$EndComp
Text HLabel 3600 4900 3    60   Input ~ 0
GND
Wire Wire Line
	3850 4100 3600 4100
Wire Wire Line
	3600 4100 3600 4150
Wire Wire Line
	3600 4450 3600 4500
Wire Wire Line
	3600 4850 3600 4900
Wire Wire Line
	3600 4500 3450 4500
Connection ~ 3600 4500
Wire Wire Line
	3600 4500 3600 4550
$Comp
L Device:R R?
U 1 1 5D5FD897
P 3600 4700
AR Path="/5728A85A/5D5FD897" Ref="R?"  Part="1" 
AR Path="/5728B04B/5D5FD897" Ref="R24"  Part="1" 
F 0 "R24" V 3700 4700 50  0000 C CNN
F 1 "115k 1%" V 3500 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3530 4700 50  0001 C CNN
F 3 "" H 3600 4700 50  0000 C CNN
F 4 "311-115KHRCT-ND" H 3600 4700 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3600 4700 50  0001 C CNN "Digikey Price"
	1    3600 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D6009E3
P 3600 4300
AR Path="/572C63BB/5D6009E3" Ref="R?"  Part="1" 
AR Path="/5728B04B/5D6009E3" Ref="R7"  Part="1" 
F 0 "R7" V 3500 4300 50  0000 C CNN
F 1 "47k" V 3700 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3530 4300 50  0001 C CNN
F 3 "" H 3600 4300 50  0000 C CNN
F 4 "311-47.0KHRCT-ND" H 3600 4300 50  0001 C CNN "Digikey Nr"
F 5 "0.09" H 3600 4300 50  0001 C CNN "Digikey Price"
	1    3600 4300
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5D66E1A4
P 6800 3200
F 0 "TP7" H 6858 3318 50  0000 L CNN
F 1 "TestPoint" H 6858 3227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7000 3200 50  0001 C CNN
F 3 "~" H 7000 3200 50  0001 C CNN
	1    6800 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP5
U 1 1 5D66F586
P 3300 2900
F 0 "TP5" H 3358 3018 50  0000 L CNN
F 1 "TestPoint" H 3358 2927 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 3500 2900 50  0001 C CNN
F 3 "~" H 3500 2900 50  0001 C CNN
	1    3300 2900
	1    0    0    -1  
$EndComp
Connection ~ 3300 2900
$Comp
L Connector:TestPoint TP6
U 1 1 5D66FD5A
P 3300 3500
F 0 "TP6" H 3242 3526 50  0000 R CNN
F 1 "TestPoint" H 3242 3617 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 3500 3500 50  0001 C CNN
F 3 "~" H 3500 3500 50  0001 C CNN
	1    3300 3500
	-1   0    0    1   
$EndComp
Connection ~ 3300 3500
$EndSCHEMATC
